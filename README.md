<!-- ## $`\textbf{ANGORA - }\textbf{AN}\text{ncestral reconstruction by local }\textbf{G}\text{en}\textbf{O}\text{me }\textbf{R}\text{earr}\textbf{A}\text{ngement analysis}`$ -->
## ANGORA - ANncestral reconstruction by local GenOme Rearrangement Analysis

This is a workflow for reconstructing ancestral genomes based on the one described by J. Salse (*Ancestors of modern plant crops*, 2016, Curr. Opin. Plant. Biol.) for ancestral reconstruction of plant genomes.

If you use this workflow, please cite:

&nbsp;&nbsp;&nbsp;   Diego P. Rubert, Fábio V. Martinez, Jens Stoye and Daniel Doerr  
&nbsp;&nbsp;&nbsp;   [*Analysis of local genome rearrangement improves resolution of ancestral genomic maps in plants*](https://doi.org/10.1186/s12864-020-6609-x)

Instead of using gene annotations for inferring the genome content of the ancestral sequence, the workflow identifies genomic markers through a process called genome segmentation. This enables it to reconstruct the ancestral genome from hundreds of thousands of markers rather than the tens of thousands of annotated genes. Using the concept of local genome rearrangement, it refines gene families and as consequence syntenic blocks before they are used in the reconstruction of contiguous ancestral regions.

### Licence

Copyright (C) 2019 Diego P. Rubert and Daniel Dörr
    
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
   
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
   
You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.


### Downloading this repository

To get stable releases of this project please look into [tagged releases](https://gitlab.ub.uni-bielefeld.de/gi/angora/-/releases). Please note that downloading this repository will not include in the downloaded file (e.g. tar.gz, .zip) the sources of tools external to this project, namely, Gecko3-DCJ, ANGeS/ANGeSpy3, Geese (atomizer) and LASTZ-hotfix, under the `tools/src` folder. If you need the sources of these projects, you need to download them individually or clone this repository (ANGORA) including its submodules as described bellow in [Cloning this repository](#cloning-this-repository) section.

### Cloning this repository

Some tools in this repository have their source codes as [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) under the `tools/src`. By default, git does not download submodules when cloning a project. In other words, git will create only a empty folder for each submodule in the repository. In order to clone this repository downloading also the submodules (that is, automatically initializing and updating each submodule in the repository), use:

```
git clone --recurse-submodules https://gitlab.ub.uni-bielefeld.de/gi/angora.git
```

or

```
git clone --recurse-submodules git@gitlab.ub.uni-bielefeld.de:gi/angora.git
```

If you want only to download a specific submodule in the repository, clone **without** the `--recurse-submodules` option, go to the empty directory corresponding to the submodule and then:

```
git submodule init 
git submodule update
```

For repositories with submodules, <tt>git pull</tt> command updates only the main repository. For updating also submodules:

```
git pull
git submodule update --recursive --remote
```


Methods
-------

The workflow can be seen as a pipeline with 3 major steps:

1. Identification of genomic markers and families
2. Discovery of syntenic blocks
3. Ancestral genome reconstruction

These steps are descibed in detail at the [original publication](https://doi.org/???).

### Identification of genomic markers and families

Genomic markers and families are obtained by solving the genome segmentation problem (M. Višnovská, T. Vinar and B. Brejová, [DNA sequence segmentation based on local similarity, ITAT 2013 Proceedings, 36-43](http://ceur-ws.org/Vol-1003/36.pdf "here"))). First, the all vs. all chromosomes aligments are obtained by the LASTZ tool ([here](https://github.com/lastz/lastz) or [here](http://www.bx.psu.edu/miller_lab/dist/README.lastz-1.02.00/README.lastz-1.02.00a.html)). Informally, the objective of genome segmentation is the decomposition of a DNA sequence into families of non-overlapping fragments, called *atoms*. To this end, genome segmentation takes as input pairwise alignments of the DNA sequence onto itself and requires that no alignment boundary lies within any of the created atoms. Note that the genome segmentation problem for multiple DNA sequences is simply defined as the segmentation problem of the concatenated DNA sequences.

A trivial segmentation would establish every single character of the input sequence into an atom of its own, thus satisfying the stated criteria. To avoid such meaningless segmentation, a minimal length requirement is imposed on the constructed atoms. Any nucleotide that is not covered by an atom resides in a *waste region*. An example of a segmentation of two DNA sequences can be seen in the figure bellow. The objective of the *genome segmentation problem* (GSP) is the construction of a segmentation that minimizes the total number of nucleotides located in waste regions. In 2013, Višnovská and colleagues have proven its intractibility and devised a heuristic called IMP for its solution. The pipeline uses an  [efficient implementation in C++ of the IMP algorithm that runs in parallel](https://gitlab.ub.uni-bielefeld.de/gi/geese).

<div align="center">
![Example of a segmentation of two DNA sequences](data/images/example_seg.png "Example of a segmentation of two DNA sequences")<br>  
<i>Example of a segmentation of two DNA sequences</i>
</div><br>

### Discovery of syntenic blocks

One method for discovering syntenic blocks that is particularly fast (speed is another important concern of this step in the ancestral reconstruction workflow) is [Gecko3-DCJ](https://gitlab.ub.uni-bielefeld.de/gi/gecko-dcj) (an extension of [Gecko3](https://bio.informatik.uni-jena.de/software/gecko3/)), which identifies syntenic blocks by discovering approximate common intervals in marker sequences. These are sets of intervals with associated genome content $`\mathcal G`$ such that the symmetric difference between each interval and $`\mathcal G`$ is bounded by $`\delta^\text{sum}`$ and, more specifically, the number of excessive (i.e., *inserted*) markers is bounded by $`\delta^\text{add}`$, and the number of missing markers by $`\delta^\text{loss}`$. Gecko3-DCJ identifies the genome content of a set of intervals by a referenced-based approach. In doing so, the genome content of the reference interval defines the genome content $`\mathcal G`$ of an interval set. Gecko3-DCJ can find approximate common intervals with multiple occurrences within a single sequence and also provides a *quorum* parameter $`q`$ by which reference-based approximate common intervals in $`q`$ or more of the provided genomes can be computed. Such syntenic blocks are scored based on their structural local similarities by means of the [local DCJ similarity](https://doi.org/???). Such score plays an importante role in later steps, namely in family refinement and ancestral reconstruction. For details about the local DCJ similarity, see [The local DCJ similarity](#the-local-dcj-similarity) section bellow or the [original publication](https://doi.org/???).


##### The local DCJ similarity

Analog to local sequence alignment, local genome rearrangement aims to identify pairs of substrings of two given marker sequences highly conserved. Since the workflow must to be able to deal with highly diverged genomes, a procedure able to tolerate all kinds of differences caused by insertion, deletion, or duplication of one or several genomic markers was designed. To this end, first referenced-based approximate common intervals are discovered in the studied genomes. Each discovered set of intervals gives rise to a set of pairs of substrings between the reference and the remaining genomes for which local rearrangement scores are calculated.

Let $`S`$ and $`T`$ be two substrings associated with one of these pairs of approximate common intervals. The local DCJ similarity computation proceeds in steps that are illustrated by an example in the figure bellow. First, pairs of sequences $`S', T'`$ are identified such that (i) $`S'`$ is a subsequence of $`S`$, and $`T'`$ of $`T`$, (ii) $`S'`$ and $`T'`$ are balanced, and (iii) for each marker $`g`$ in $`\mathcal G(S')`$ holds true that $`m_{S'}(g) = \min(m_S(g), m_T(g))`$. The last constraint ensures maximality of the balanced subsequences.

<div align="center">
![Illustration of the procedure for computing local DCJ similarity scores from a pair of syntenic marker sequences](data/images/ag.png "Illustration of the procedure for computing local DCJ similarity scores from a pair of syntenic marker sequences")<br>  
<i>Illustration of the procedure for computing local DCJ similarity scores from a pair of syntenic marker sequences</i>
</div><br>

Sequences $`S'`$ and $`T'`$ are then subjected to a second procedure that finds one-to-one assignments between all markers of the two sequences, thus further refining them to non-duplicated balanced sequences $`S''`$ and $`T''`$. Eventually, those pairs of balanced sequences $`S''`$ and $`T''`$ are identified that maximize the following formula:

```math
s_\text{\tiny DCJ}(S'', T'') = \sum_{C \in \mathcal{C}}{f(|C|)} + \frac{1}{2} \left (\sum_{O \in \mathcal{O}}{f(|O|+1)} + \sum_{E \in \mathcal{E}}{f(|E|+2)}\right) - d \cdot p\:,
```

where $`\mathcal{C}`$, $`\mathcal{O}`$ and $`\mathcal{E}`$ are the sets of cycles, odd paths, and even paths in the constructed adjacency graph of $`S''`$ and $`T''`$, $`d := |S|+|T|-(|S''|+|T''|)`$ is the number of deleted markers and $`p`$ is the deletion penalty. Function $`f : 2\mathbb{N} \rightarrow \mathbb{R}`$ scores each cycle and path proportional to its length. Because short cycles and paths are indicators of similarity, whereas long cycles and paths suggest the opposite, Gecko3-DCJ uses a simple realization of $`f`$ that works well in general:

```math
f(l) = \frac{2 - l}{b - 2} + 1\:.
```

The function $`f`$ used in Gecko3-DCJ makes use of constant $`b`$, a length threshold that demarcates short from long cycles and paths, called in the software "borderline cycle length".


### Ancestral genome reconstruction

The ancestral genome reconstruction is made by [ANGeSpy3](https://gitlab.ub.uni-bielefeld.de/gi/angespy3), a port to Python 3 of [ANGES](https://github.com/cchauve/ANGeS) (B.R. Jones, A, Rajaraman, E. Tannier and C. Chauve, [ANGES: Reconstructing ANcestral GEnomeS maps](https://doi.org/10.1093/bioinformatics/bts457), Bioinformatics 28(18), 2012), a suite of Python programs that allows reconstructing ancestral genome maps from the comparison of the organization of extant-related genomes.

In reconstructing ancestral genomes, non-duplicated balanced sequences $`S''`$ and $`T''`$ identified by the described in the [The local DCJ similarity](#the-local-dcj-similarity) section are used to refine the genomic marker families across the entire genomic dataset. This enables substantial improvement in determining the ancestral genome content. To this end, a procedure takes unambiguous one-to-one assignments across overlapping syntenic blocks to decompose their marker families into disjoint subsets. Further, if non-overlapping sets of syntenic blocks share markers from the same family, the families are also decomposed into smaller subsets.


How to use
-------

ANGORA workflow runs on Snakemake. The Snakemake workflow management system is a tool to create reproducible and scalable data analyses. Workflows are described via a human readable, Python based language. Snakemake also supports the submission of jobs to compute grid engines such as Oracle Grid Engine or Univa Grid Engine. This is specially useful when large data must be processed, such as when working with large genomes. Please refer to the official [Snakemake installation page](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html) to get it installed in your system.

Additional dependencies are:

* Python 3
* Biopython

After having Snakemake installed and configured the workflow (see [Workflow configuration](#workflow-configuration)), just run:
```
snakemake
```
or
```
snakemake --cores NN
```
if you want to run tasks in parallel (if possible) by replacing `NN` by the number of processor cores. The final PQ-tree file will be located in some subfolder reflecting the parameters configured, for instance:

```
atoms
  ↳ lastz_notransition_step10_gapped_hspthresh5000_nochain_gfextend_filteridentity:50
    ↳ ag100_ai30_al13_m100
      ↳ rSE002_q3_dcj_dcjBorderline8_dcjPenality0.25_dcjUseHeuristics_d6
        ↳ refoverlap_refsim_simulated_sim_heur_refined
          ↳ CARS
            ↳ simulated_PQTREE_HEUR
```

There are 2 small sample datasets included in this repository, read the [Sample datasets](#sample-datasets) section for how to run the pipeline for them. The next sections detail some aspects of the workflow. 

This workflow also implements an *alternative pipeline* (using annotated genes) and *[Salse 2016](https://doi.org/10.1016/j.pbi.2016.02.005) pipeline* (using CloseUp instead of Gecko3-DCJ). For running the alternative pipeline append <tt>angesG</tt> (or <tt>angesGREF</tt> if using family refinement) to the snakemake command above. For running Salse's pipeline, append <tt>angesC</tt> instead. In both cases, the output will be under the <tt>genes</tt> folder instead of the <tt>atoms</tt> folder.


### Files and folders

These are a few important files/folders in the workflow:

* `Snakefile`: defines the Snakemake&apos;s rules. There is no need to modify this file unless you want to customize the pipeline.
* `config.yaml`: defines parameters for all tools and locations of important files. See [Workflow configuration](#workflow-configuration) section.
* `dataset.cfg`: defines the species names used in the workflow and where their [GenBank](https://www.ncbi.nlm.nih.gov/genbank/) (<tt>.gbk</tt>) files are located. This file will be modified by the workflow to save some computed values. Its location is defined in <tt>config.yaml</tt>.
* `tree.nwk`: defines the evolutionary tree for the input species in the [Newick format](https://en.wikipedia.org/wiki/Newick_format). ANGES requires a special labeled node in this tree where the ancestor is located, marked by an `@` (see an example in the simulated sample tree at `data/simulated/tree.nwk`). Its location is defined in <tt>config.yaml</tt>.
* `scripts/`: folder containing auxiliary scripts written for this workflow. Its location is defined in <tt>config.yaml</tt>.
* `tools/bin/`: the binaries of all tools used in the pipeline are located under this folder, however the location of each tool is defined individually in <tt>config.yaml</tt>.

**The workflow requires that each species has a unique name and also that each chromosome has a unique name** (that is, there is no chromosome pair with the same name among all chromosomes of the whole dataset). In addition, each species must have a GenBank file **without spaces in the name** containing whole chromosome sequences. If you also want to run the alternative pipeline or Salse's pipeline, genes information must be in GenBank files, more specifically, the <tt>CDS</tt> related to genes, where each such entry must have a <tt>locus_tag</tt>, <tt>gene</tt> or a <tt>name</tt> field that is used as a unique identifier for the genes.


### Workflow configuration

The workflow configuration depends basically on three files (described in [Files and folders](#files-and-folders) section), namely (i) `config.yaml`, (ii) `dataset.cfg`, and (iii) `tree.nwk`. Examples of all these files can be found under the `data/simulated/` folder. More specifically:

* `config.yaml`: defines parameters and locations of files.
* `dataset.cfg`: defines species names (**do not use spaces**), their GenBank files, and saves some computed data used along the pipeline.
* `tree.nwk`: defines the evolutionary tree with all species and has a special node marked with an `@` defining where the ancestor is located in the tree.

As for the `config.yaml` file, there are comments describing each definition in the example file (`data/simulated/config.yaml`). This is a summary of its contents that you will probably want to change for your dataset:

* `dataset`: location of <tt>dataset.cfg</tt> file.
* `ancestor`: name of the ancestor to be reconstructed (can be any word).
* `evol_tree`: location of the evolutionary tree file.
* `reference`: reference species, is used by gecko (clusters are found with respect to some reference).
* parameters of tools used in this workflow.

The parameters of each tool are described in their manuals (e.g. in the tool&apos;s webpage listed in [Third party tools](#third-party-tools) section or by running the tool binary without any parameter) and refined running the pipeline with different settings. **However, there is one important step for which parameters must be choosen very carefully: the sequence alignments by LASTZ**. Fortunately, ANGORA provides some useful tools which might help choosing *good* parameters for LASTZ. Details in [Finding good LASTZ parameters](#finding-good-lastz-parameters) section below.


### Finding good LASTZ parameters

As noted before, the workflow step where LASTZ computes aligments is highly time consuming for large data and has deep impact on subsequent steps. If LASTZ parameters are too *loose* no alignment will be discarded and the output will be noisy. On the other hand, if LASTZ parameters are too *tight* most of alignments will be discarded and the output will consist of an almost empty set of alignments. See bellow an example. It is important to stress here that LASTZ has a bug that, depending on the parameters choice, outputs inconsistent data. The authors of LASTZ are aware and have already fixed this bug, but there is no public release containing the fix yet. Therefore, our workflow contains a custom [LASTZ hotfix version](https://gitlab.ub.uni-bielefeld.de/gi/lastz-hotfix).

<div align="center">
![Examples of LASTZ alignments plots](data/images/lastz-alignments.png "Examples of LASTZ alignments plots")<br>  
<i>Examples of LASTZ alignments plots</i>
</div><br>

The full list of LASTZ parameters with examples can be found in its [README page](https://lastz.github.io/lastz/). For instance, the ones used for the simulated sample dataset are:

```
--notransition --step=10 --gapped --hspthresh=5000 --nochain --gfextend --ambiguous=iupac --masking=5 --filter=identity:50
```

While the ones used in [*Analysis of local genome rearrangement improves resolution of ancestral genomic maps in plants*](https://doi.org/10.1186/s12864-020-6609-x) are:

```
--notransition --step=10 --gapped --hspthresh=6000 --nochain --gfextend --ambiguous=iupac --masking=5 --filter=identity:70
```

After selecting the paremeter set for your dataset (by modifying the `lastz_params` definition in `config.yaml`), the outcome can be tested by running:

```
snakemake alldotplots 
```

The rule `alldotplots` processes the data through the workflow until the alignments are calculated by LASTZ, then it produces a dotplot as in the exemple above for each pair of chromosomes. This can be very helpful for testing different combinations and finding a good parameter set. Notice, however, that for large datasets the alignments computation step can be highly time consuming. In this case, you can pick a pair of chromosomes (that you know beforehand that they have some syntenic region), remove all the others from your dataset and run the Sankemake command above for generating a dotplot. The dotplots files are located in some subfolder reflecting the parameters chosen, for instance (considering species <tt>SE001</tt>, chromosome <tt>SE001chr1</tt> and species <tt>SE002</tt>, chromosome <tt>SE002chr1</tt>):

```
atoms
  ↳ lastz_notransition_step10_gapped_hspthresh5000_nochain_gfextend_filteridentity:50
    ↳ SE001:SE002
      ↳ SE001chr1:SE002chr1.pdf
```

### Sample datasets

This workflow includes two small sample datasets, one with simulated and another with real genomes. The simulated dataset has 3 simulated unichromosomal genomes and is located under the `data/simulated` folder. The real dataset has 3 multichromosomal genomes of real species (*Ostreococcus* green algae), more specifically *O. tauri strain RCC4221*, *O. lucimarinus strain CCE9901* and *O. RCC809*, and is located under the `data/ostreococcus` folder.

Each one has:

* a small `README.txt` file with some details and URLs.
* a `config.yaml` with pre-configured parameters for running the workflow with for the dataset.
* a `dataset.cfg` defining the dataset.
* a `tree.nwk` file defining an arbitrary evolutionary tree.
* the GenBank (`.gbk`) files for each species to be used in the pipeline.
* additional fasta and gff files used for generating the GenBank files for simulated genomes and some real species (some internet databases do not provide <tt>.gbk</tt> files for genomes).

For running the pipeline for one of them, simply copy or symlink the corresponding <tt>config.yaml</tt> to the workflow root folder (there the <tt>Snakefile</tt> is located) and then run the pipeline as specified in the beginning of the [How to use](#how-to-use) section. Please notice that the <tt>.gbk</tt> files for the real species are compressed and must be decompressed with `gzip -d` before used .

Regarding the alternative pipeline (using annotated genes), only the dataset containing real genomes has any information about annotated genes in <tt>.gbk</tt> files, therefore the alternative pipeline cannot be used with the simulated sample dataset.

**Please note** that the pre-configured parameters are not result of any study on the sample datasets. Therefore, they may not be the good parameters for achieving the best results that either the main or the alternative pipelines can produce.


### Flowchart of the pipeline

This is the flowchart of the main pipeline written in ANGORA. It specifies each rule and the input and output files for each rule.

<div align="center">
![Flowchart of the main pipeline](data/images/workflow-atoms.png "Flowchart of the main pipeline")<br>  
<i>Flowchart of the main pipeline</i>
</div><br>

There is also an *alternative* pipeline, which uses gene annotations instead of genomic markers found by sequence alignment/atomization process.

<div align="center">
![Flowchart of the alternative pipeline](data/images/workflow-annot.png "Flowchart of the alternative pipeline")<br>  
<i>Flowchart of the alternative pipeline</i>
</div><br>

In addition, ANGORA provides a basic implementantion of the Salse's pipeline.

<div align="center">
![Flowchart of the Salse's pipeline](data/images/workflow-closeup.png "Flowchart of the Salse's pipeline")<br>  
<i>Flowchart of the Salse's pipeline</i>
</div><br>


Third party tools
-------

* [Gecko3-DCJ](https://gitlab.ub.uni-bielefeld.de/gi/gecko-dcj)
* [GEESE](https://gitlab.ub.uni-bielefeld.de/gi/geese)
* [ANGeSpy3](https://gitlab.ub.uni-bielefeld.de/gi/angespy3) (originally [ANGeS](https://github.com/cchauve/ANGeS))
* [LASTZ-hotfix](https://gitlab.ub.uni-bielefeld.de/gi/lastz-hotfix) (originally [LASTZ](https://github.com/lastz/lastz))
* [CloseUp](http://closeup.ics.uci.edu/) (with few modifications, check the <tt>CHANGELOG</tt> file in the <tt>tools/src/CloseUp</tt> folder for details)
