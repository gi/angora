#!/bin/bash

###############################################################################
# Copyright 2019 Diego Rubert                                                 #
#                                                                             #
# This file is part of Gecko3-DCJ.                                            #
#                                                                             #
# Gecko3-DCJ is free software: you can redistribute it and/or modify          #
# it under the terms of the GNU Lesser General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# Gecko3-DCJ is distributed in the hope that it will be useful,               #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with Gecko3-DCJ.  If not, see <http://www.gnu.org/licenses/>.         #
###############################################################################

# The arguments to the script are the .gbk files and the .cog file (output).
# This script gets transcripts from the gbk files (one file by species), computes
# gene families (orthologs) and builds a .cog file to be used as input for Gecko.
#
# This script depends on the following software (and their own dependencies):
#   * Proteinortho (https://www.bioinf.uni-leipzig.de/Software/proteinortho/)
#   * FFGC (https://bibiserv2.cebitec.uni-bielefeld.de/ffgc)


##################################
# Paths, variables and constants #
##################################
script_name="$(basename "$0")"
script_prefix="${script_name%.*}"

cur_path="$(pwd)"
log_file="$cur_path/$script_prefix.log"

PORTHO_EVALUE="1e-05"
PORTHO_CPUS=10

# Script of FFGC that extracts annotated sequences from GBK files
extract_seq_gbk=~/"vol/genomes/ffgc/ffgc_v0.3.1.src/extract_annotated_sequences.py"

# Proteinortho
portho=~/"vol/genomes/proteinortho_v5.16b/proteinortho5.pl"


#############
# Functions #
#############
# Exits if anything goes wrong
set -e
err_report() {
    echo "ERROR ON LINE $1"
}
trap 'err_report $LINENO' ERR

# Prints usage and exits with error code
function usage() {
  echo "The arguments to the script are the .gbk files and the .cog file (output)."
  echo "This script gets transcripts from the gbk files (one file by species), computes"
  echo "gene families (orthologs) and builds a .cog file to be used as input for Gecko."
  echo
  echo "This script depends on the following software (and their own dependencies):"
  echo "   * Proteinortho (https://www.bioinf.uni-leipzig.de/Software/proteinortho/)"
  echo "   * FFGC (https://bibiserv2.cebitec.uni-bielefeld.de/ffgc)"
  echo
  echo "Usage:"
  echo "  $script_prefix <.gbk files> <.cog file>"
  echo "Where the number of GBK files must be at least two."
  exit 1
}


####################
# Check parameters #
####################
if [[ $# -lt 3 ]]
then
  usage
fi
gbk_files=("$@")
unset "gbk_files[${#gbk_files[@]}-1]" # the last parameter is the .cog file
cog_file="${@: -1}"

echo "The input files are:" > "$log_file" # empty the log file, if it exists
for f in "${gbk_files[@]}"
do
  echo "$f" >> "$log_file"
  if [[ "$f" != *".gbk" ]]
  then
    echo "ERROR: \"$f\" is not a .gbk file!" >&2
    exit 1
  fi
done



#######################################
# Generate the faa from the gbk files #
#######################################
echo -n "Extracting annotated sequences from GBK files... "
echo -e "\nExtracting annotated sequences from GBK files:" >> "$log_file"
for f in "${gbk_files[@]}"
do
  echo "${f%gbk}faa" >> "$log_file"
  "$extract_seq_gbk" --translate_to_aa --only_longest "$f" > "${f%gbk}"faa
done
echo "Done."
echo "Converted GBK into FAA files." >> "$log_file"
rm -f "extract_annotated_sequences.log"


####################
# Run proteinortho #
####################
faa_files=("${gbk_files[@]}")
for i in "${!faa_files[@]}"
do
  faa_files[$i]="${faa_files[i]%gbk}faa"
done
echo "Running proteinortho to define gene families... "
echo -e "\nRunning proteinortho." >> "$log_file"
"$portho" -keep -clean -project="$script_prefix" -p=blastp+ -cpus=$PORTHO_CPUS -e=$PORTHO_EVALUE "${faa_files[@]}"
echo "Protheinortho tasks done."
echo "Families computed in file \"$script_prefix.proteinortho\"." >> "$log_file"


#######################
# Create fastab files #
#######################
# Contains the list of genes sorted by locus for each chromosome
echo -n "Creating fastab files... "
echo -e "\nCreating fastab files:" >> "$log_file"
for f in "${faa_files[@]}"
do
  echo "${f%faa}fastab" >> "$log_file"
  while read line
  do
    # These separators and column numbers may need to be changed depending on the metadata format of fasta (faa) files
    IFS='|' tokens=( $line )
    protein=${tokens[1]}
    gene=${tokens[3]}
    location=${tokens[4]}
    IFS=':' location_array=( $location )
    location_a=${location_array[0]}
    location_b=${location_array[1]}
    chromo=${tokens[6]}
    strand=${tokens[8]}
    echo -e "$protein\t$gene\tNaN\t$chromo\t$location_a\t$location_b\t$strand"
  done <<< "$(grep "^>" "$f")" | sort -V --key=4,5 > "${f%faa}fastab"
done
echo "Done."
echo "All fastab created." >> "$log_file"


########################################
# Create one COG file for each species #
########################################
# Each file contains all chromosomes of the species
fastab_files=("${gbk_files[@]}")
for i in "${!fastab_files[@]}"
do
  fastab_files[$i]="${fastab_files[i]%gbk}fastab"
done

echo -n "Generating intermediate cog files... "
echo -e "\nGenerating cog files" >> "$log_file"
python - "$script_prefix.proteinortho" "${fastab_files[@]}" <<END
from sys import argv
portho_fname = argv[1]
fastab_fnames = argv[2:]

families = dict()
f = open(portho_fname, "r")
f.readline()
family = 1
for line in f:
  fields = line.split()
  for species in fields[3:]: # for each key "protein_id|gene_id" we set the family
    for entry in species.split(","):
      families["|".join(entry.split("|")[1:4:2])] = family
  family += 1
f.close()

for fname in fastab_fnames:
  f = open(fname, "r")
  genome_name = ".".join(f.name.replace("_"," ").split(".")[:-1]) # filename replacing _ by space and removing extension
  chromosome = { "name" : "" }
  chromosomes = []
  for line in f:
    fields = line.split()
    protein_id = fields[0]
    gene_id = fields[1]
    chr_name = fields[3]
    strand = fields[6]
    key = protein_id + "|" + gene_id
    if key in families:
      family = families[key]
    else:
      family = 0
  
    if chr_name != chromosome["name"]:
      chromosome = dict()
      chromosome["name"] = chr_name
      chromosome["data"] = []
      chromosomes.append(chromosome)
      last_chr = chr_name
  
    chromosome["data"].append( [family, strand, protein_id, gene_id] )
  f.close()

  f = open(fname[:-6] + "cog", "w") # we know fname ends with "fastab"
  for c in chromosomes:
    f.write("%s, chromosome %s\n" % (genome_name, c["name"]))
    f.write("%d proteins\n" % len(c["data"]))
    for p in c["data"]: # for each protein (line)
      f.write("%d\t%s\t?\t%s\tunknown\t%s\tunknown\n" % (p[0], p[1], p[2], p[3]))
    f.write("\n")
  f.close()

exit(0)
END
echo "Done."


#############################################
# Create a .cog file containing all species #
#############################################
cog_files=("${gbk_files[@]}")
for i in "${!cog_files[@]}"
do
  cog_files[$i]="${cog_files[i]%gbk}cog"
done
echo -n "Generating main cog file... "
echo -e "Concatenating cog files:" >> "$log_file"
echo -n > "$cog_file"
for f in "${cog_files[@]}"
do
  echo "$f" >> "$log_file"
  cat "$f" >> "$cog_file"
done
echo "Done, output: $cog_file"
echo -e "COG files concatenated into \"$cog_file\"\n" >> "$log_file"



exit 0
