#!/usr/bin/perl -w

$cprog='closeup.c';
$comp='closeup';

`gcc -O3 $cprog -o $comp -std=c90 -Wno-unused-result -lm`;

$oldperl="/usr/bin/perl";
$oldperl =~ s/\//\\\//g;
$perl = `which perl`;
$perl =~ s/\//\\\//g;
chomp $perl;

@files =('closeup.pl', 'closeup2txt.pl', 'closeup2mysql.pl', 'closeup_input.pl');

foreach $f (@files){
    `sed -n 's/$oldperl/$perl/' $f`;
}
