#!/usr/bin/perl -w

if ($ARGV[0] ne ''){
    $file=$ARGV[0];
}
else{
    print "Which file contains the data you want to parse?\n";
    $file=<STDIN>;
    chomp $file;
}
if ($ARGV[1] ne ''){
    $data=$ARGV[1];
}
else{
    print "How do you want to call the dataset?\n";
    $data=<STDIN>;
    chomp $data;
}

open (IN, "$file") || die "cannot open $file\n";

$out=$file."_overview.txt";

open (OUT, ">$out") || die "cannot open $out\n";

$out=$file."_detail.txt";

open (DOUT, ">$out") || die "cannot open $out\n";


$block_counter=0;
$cycle=0;

 I:while ($line=<IN>){
     if ($line =~ /using (\d+) different marker locations/){
	 $X=$1;
	 $x=0;
	 until ($x == $X){
	     $x++;
	     $line=<IN>;
	 }
     }
     elsif ($line =~ /^CHROM 1 /){
	 $x=0;
	 %marker_ref=();
	 %chr_ref=();
	 $chr=1;
	 until ($x == $X){
	     $line=<IN>;
	     if ($line=~ /^CHROM (\d+)/){
		 $chr=$1;
	     }
	     else{
		 $x++;
		 $_=$line;
		 @tmp=split;
		 $marker_ref{$tmp[1]}=$tmp[2]; #links ref number to marker
		 $chr_ref{$chr}{$tmp[0]}=$tmp[1]; #links chr rank to marker ref
	     }
	 }
     }
     elsif ($line =~ /^\s*(\d+)\:\s+c1\=/ && $cycle==0){
	 
	 $block_counter=$1;
	 @marker=();
	 
	 if ($line =~ /^\s*\d+\:\s+c1\=\s+(\d+)\s+c2\=\s+(\d+)\s+match\=\s+(\d+)\/\s*(\d+).*? ss\=\s+(\d+)/){
	     $c1{$block_counter}=$1;
	     $c2{$block_counter}=$2;
	     $match=$3;
	     $len{$block_counter}=$4;
	     $ss{$block_counter}=$5;
	 }
	 else{
	     die "Problem\n$line\n";
	 }
	 
	 $line=<IN>;
	 $_=$line;
	 @tmp=split; #splits line on spaces, ignoring leading spaces
	 
	 unless ($#tmp+1 ==$match){
	     die "Something wrong with $c1{$block_counter} vs $c2{$block_counter} of length $match\n$line\n";
	 }
	 
	 foreach $t (@tmp){
	     @tmp2 = split /\//, $t;
	     push (@marker, $tmp2[0]);
	 }
	 
	 $line =<IN>;
	 $_=$line;
	 @tmp=split;
	 @order1 = sort {$a <=> $b} @tmp;

	 $line =<IN>;
	 $_=$line;
	 @tmp2=split;
	 
	 @order2 = sort {$a <=> $b} @tmp2;

	 unless ($#tmp+1 ==$match && $#tmp2+1 == $match){
	     die "Something wrong with $c1 vs $c2 of length $len\n@marker\n$line\n";
	 }
	 $c1s{$block_counter}=$order1[0];
	 $c1e{$block_counter}=$order1[$#order1];
	 $c2s{$block_counter}=$order2[0];
	 $c2e{$block_counter}=$order2[$#order1];
	 #$sig{$block_counter}=100;

	 @{$c1m{$block_counter}}=@tmp;
	 @{$c2m{$block_counter}}=@tmp2;
	 @{$marker{$block_counter}}=@marker;
	 #for ($i=0; $i < $len{$block_counter}; $i++){
	     #print OUT "INSERT INTO $table_b VALUES('$c1','$c2','$marker_ref{$chr_ref{$c1}{$marker[$i]}}','$tmp[$i]','$tmp2[$i]','$block_counter','NULL');\n";
	 #}
     }
     elsif ($line =~ /^Cycle/){ #fudge
	 $cycle=1;
     }
     elsif ($line =~ /^what percent cutoff/){ 
#	 $halting=1;
	 until ($line =~ /^\d+\s+runs\,.*?significant/){
	     if ($line =~ /\s*(\d+)\:\s+c\=\s+\d+\,\s+\d+\s+m\=\s+\d+.*?p\=\s+(\d+\.?\d+)/){
		 $block_id=$1;
		 $sig=$2;
		 $sig{$block_id}=$sig;
	     }
	     $line=<IN>;
	 }
	 last I;
     }
 }


@labels=('c1', 'c2','#markers','ss', 'c1s', 'c1e', 'c2s', 'c2e', 'sig', 'id');
printf  OUT "%4s %4s %6s %9s %6s %6s %6s %6s %5s %8s\n", @labels;

@id = keys %sig;
@id = sort {$sig{$a} <=> $sig{$b}} @id;

$divider="=";
$divider x= 10;

foreach $id (@id){
    printf  OUT "%4s %4s %6d %9d %6.1f %6.1f %6.1f %6.1f %5.1f %8d\n", $c1{$id},$c2{$id},$len{$id},$ss{$id},$c1s{$id},$c1e{$id},$c2s{$id},$c2e{$id},$sig{$id},$id;

    @tmp = @{$c1m{$id}};
    @tmp2 = @{$c2m{$id}};
    $c1=$c1{$id};
    $c2=$c2{$id};

    print DOUT "$divider id: $id BETWEEN Chr $c1 and Chr $c2\n";
 

    for ($i=0; $i < $len{$id}; $i++){
	printf DOUT "%25s %8.1f %8.1f\n", $marker_ref{$chr_ref{$c1}{$marker{$id}[$i]}},$tmp[$i],$tmp2[$i];
    }
}




close IN;
close OUT;
