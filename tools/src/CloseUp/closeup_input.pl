#!/usr/bin/perl -w


print "Enter mysql username: ";
$user=<STDIN>;
chomp $user;

print "Enter mysql password: ";
$passwd=<STDIN>;
chomp $passwd;

print "MySQL database name: ";
$db=<STDIN>;
chomp $db;

$dbh = &start_MySQL($db);

print "MySQL table containing the map data: ";
$table=<STDIN>;
chomp $table;

print "Name for outfile: [$table]";
$out=<STDIN>;
chomp $out;
if ($out eq ''){
    $out = $table;
}
open (OUT, ">$out") || die "cannot open $out\n";

$st = "SELECT marker,chr,cM  FROM $table";
$sth = $dbh->prepare($st)
    or warn "Can't prepare statement '$st': $DBI::errstr\n";
$rv = $sth->execute
    || die "Can't execute the statement '$st': $DBI::errstr\n";
if ($rv >= 1){
    while ( ($marker,$chr,$cm) =  $sth->fetchrow_array){
	$m{$marker}{$chr}{$cm}=1;
    }
}

foreach $m (keys %m){
    print OUT "$m (";
    @ch = keys %{$m{$m}};
    for ($i=0; $i<=$#ch; $i++){
	$ch=$ch[$i];
	@cm = keys %{$m{$m}{$ch}};
	for ($j=0; $j<=$#cm; $j++){
	    $cm=$cm[$j];
	    print OUT "$cm $ch";
	    unless ($i == $#ch && $j == $#cm){
		print OUT ", ";
	    }
	    else{
		print OUT ")\n";
	    }
	}
    }
}


sub start_MySQL{
    $database = $_[0];
    use DBI;
    $DBD = 'mysql';
    $host = 'localhost';

    $dbh = DBI->connect("DBI:$DBD:$database:$host",
                        "$user","$passwd",
                        { RaiseError => 1, AutoCommit => 1 });
    return $dbh;
}
