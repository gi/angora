#!/usr/bin/perl -w

print "Would you like to perform and inter- or intra-genomic comparison? [inter/intra]\n";
$type=<STDIN>;
while ($type !~ /[inter,intra]/i){
    print "Type must be either inter or intra\n";
    $type=<STDIN>;
}


if ($type =~ /intra/i){
    print "Please enter the name of the file containing the map data: ";
    $file=<STDIN>;
    chomp $file;
    open (TMP,"$file") || die "cannot open $file: $!\n";
    if ($file =~ /(.*?)\.\w+$/){
	$base=$1;
    }
    else{
	$base=$file;
    }
}
else{
    print "Please enter the names of the two files containing the map data. \nWrite the file names separated by a space.\n";
    $mtmp=<STDIN>;
    @files = split /\s+/, $mtmp;
    $base='';
    foreach $f (@files){
	open (TMP, "$f") || die "cannot open $f: $!\n";
	if ($f =~ /(.*?)\.\w+$/){
	    $base.=$1;
	}
	else{
	    $base .= $f;
	}
    }
    $file="tmpfile";
    $middle = "\ngenome (2 2)\n";
    `cat $files[0] >$file`;
    `echo "$middle" >> $file`;
    `cat $files[1] >> $file`;
}

print "Output in plain text tables, or in MySQL database format? [text/mysql] ";$format=<STDIN>;
while ($format !~ /[text,mysql]/i){
    print "please enter text or mysql: ";
    $format = <STDIN>;
}

print "Do FullPermutation (p), FastRuns (f), or Cluster (c) search: ";
$meth=<STDIN>;
chomp $meth;

print "Minimum number of matches in a run [3]: ";
$min_run = <STDIN>;
chomp $min_run;
unless ($min_run =~ /\d+/){$min_run = 3;}

if ($meth eq 'c'){
print "Minimum Cluster density ratio [2]: ";
$clust_dens=<STDIN>;
chomp $clust_dens;
unless ($clust_dens =~ /\d+/){$clust_dens = 2;}
print "Maximum Cluster length difference [20]: ";
$clust_diff=<STDIN>;
chomp $clust_diff;
unless ($clust_diff =~ /\d+/){$clust_diff = 20;}
$clust_sd = 'n';
$clust_bi = 'n';
} else
{
print "maximum Linup reordering [2]: ";
$line_d=<STDIN>;
chomp $line_d;
unless ($line_d =~ /\d+/){$line_d = 2;}
}

#print "Type of map randomization for statistical assessment.\n 1)uniform \n 2)preserving chromosome features  \n 3)shuffle matched markers only \nEnter 1,2, or 3: ";
#$rand=<STDIN>;
$rand = 2;
chomp $rand;

print "How many cycles of randomization would you like to perform? ";
$cycles=<STDIN>;
chomp $cycles;

print "Save runs with what level of significance [1] (enter 100 to save all runs): ";
$sig=<STDIN>;
chomp $sig;
unless ($sig =~ /\d+/){$sig = 1;}

print "Maximum number of map elements in a run [10]: ";
$max_runs=<STDIN>;
chomp $max_runs;
unless ($max_runs =~ /\d+/){$max_runs=10;}

$end=-1;

$tmp="delete.me";

open (OUT, ">$tmp") || die "cannot open $tmp\n";

if ($meth ne 'c'){
print OUT <<EOP;
$meth
$min_run
$line_d
$rand
$cycles
$file
$sig
$max_runs
$end
EOP
}  else
{
print OUT <<EOP;
$meth
$min_run
$clust_sd
$clust_dens
$clust_diff
$clust_bi
$rand
$cycles
$file
$sig
$max_runs
$end
EOP
}
    close OUT;

$nice{'f'}='Fast';
$nice{'p'}='Perm';
$nice{'c'}='Clus';

if ($meth ne 'c'){$default= $base."_".$nice{$meth}.$line_d;}
   else {$default= $base."_".$nice{$meth}.$clust_dens."_".$clust_diff;}
print "Basename for data (will be used for output files) [$default]: ";
$dataname=<STDIN>;
chomp $dataname;
if ($dataname eq '' || $dataname =~ /^\s+$/){
    $dataname=$default;
}

$default = $dataname.".out";
print "primary results file (not parsed) [$default] : ";
$resultsfile=<STDIN>;
if ($resultsfile eq '' || $resultsfile =~ /^\s+$/){
    $resultsfile=$default;
}
chomp $resultsfile;

print "\n\n Calculating runs ... may be slow.\n\n Enter <ctrl> z to suspend this job, and then enter bg to start it running in the background. This frees up your terminal for other uses\n\n";

print "Running: cat $tmp | closeup > $resultsfile\n";
`cat $tmp | closeup > $resultsfile`;
#system "mv roc $dataname.roc";
if ($format =~ /text/){
    `closeup2txt.pl $resultsfile $dataname`;
}
else{
    `closeup2mysql.pl $resultsfile $dataname`;
}


