
# include <stdio.h>
# include <string.h>
# include <math.h>
# include <stdlib.h>
# define MAX_RUN_LEN 10       /* maximum length of a run */
# define MAX_RUNS 100000      /* maximum runs for a pair of chromosomes */
# define MAX_ALL_RUNS 20000000  /* maximum runs over all chromosomes */
# define MAX_MARKS 100000     /* maximum markers over all chromosomes */
# define MAX_CHROM 80         /* maximum number of chromosomes */
# define HASH_SZ 1000000      /* size of hash table */
# define MAX_MRK_CHRM 10000    /* maximum number of markers per chromosome */
int max_runs,run_cnt,longest_run,b,t,max_chrom,max_mark,verbose,save_subruns;
int make_data,d_num,c_num,r_num,dr_num,rm_num,rf_num,rc_num,m_num,mch_num;
int min_len,max_cycles,randseed,rand_type,can_skip,can_tol,flip_all;
int toss_frac,save_set,biggest_hash_try,tried_runs,hack,two_sets;
int gml,gdm,len_cutoff,max_set_1,use_clust,use_fast,use_adhore,use_run;
int art_gene_num,old_style,toss_single,use_scaled_fdg,do_left,use_sd;
float art_sim_prob,art_run_frac,art_run_cent,run_fstart,run_fstop;
float fdg,prob_cutoff,clust_density,rs_num,run_keep_frac;
float ave_dist,total_len,clust_diff_max,fixed_fdg;
int mark[MAX_MARKS],xrun_marks[2*MAX_RUN_LEN+4],com_cnt[MAX_CHROM][MAX_CHROM];
int rand_match_cnt[MAX_RUN_LEN+2],index2[MAX_RUN_LEN+2];
int mkch[MAX_MARKS][MAX_CHROM];
int hash_tbl[HASH_SZ],mrk_cnt[100],index2[MAX_RUN_LEN+2];
float xlocs1[2*MAX_RUN_LEN+4],xlocs2[2*MAX_RUN_LEN+4];
float ch_len[MAX_CHROM],min_loc[MAX_CHROM],chrom_len[MAX_CHROM];
float cm[MAX_CHROM][500][2*MAX_CHROM];
int clust_info[5000][4];
int stat_cnts[MAX_CHROM][MAX_CHROM][MAX_RUN_LEN+2];
int stat_cnts1[MAX_CHROM][MAX_CHROM][MAX_RUN_LEN+2];
int seen_known[100],seen_matched[100];
int c1c2start[MAX_CHROM][MAX_CHROM][MAX_RUN_LEN+1];
char out_file_name[50],mrk_use[MAX_MARKS];
float *locs1 = &xlocs1[MAX_RUN_LEN];  /* middle of array */
float *locs2 = &xlocs2[MAX_RUN_LEN];  
int   *run_marks = &xrun_marks[MAX_RUN_LEN];
struct marker_info{char name[30]; 
  int mrk_num,chrom,pos,name_cnt,name_top,chrom_cnt; float loc;};
struct marker_info mrk[MAX_MARKS];
struct run_info{int c1,c2,len,cnt,cnt1,tmp,tmp1,bgnum,cnt_tot,len_nd,link; 
                float c1m1,c1m2,c2m1,c2m2,spc,prob,lin1,lin2;
                double ss;};
struct run_info all_runs[MAX_ALL_RUNS];
struct cluster_info{int mark,used; float loc1,loc2;};
struct cluster_info cl[MAX_RUN_LEN+2];
struct chrm_marks{int mrk_num,name_cnt,name_top,index; float loc;};
struct chrm_marks chmk[MAX_CHROM][MAX_MRK_CHRM+1];
float known[100][7] = {{1, 205.2, 241.3, 5, 5.1, 39.4, 13},
                       {1, 164.6, 202.1, 5, 37.3, 66.1, 12},
                       {1, 33.1, 60.8, 9, 72.8, 122.7, 8},
                       {1, 178.8, 189.4, 3, 104.3,151.8, 6},
                       {1, 73.2, 96.1, 9, 78, 97, 5},
                       {1, 15.9, 20.8, 3, 84.5, 112.2, 4},
                       {1, 105.9, 247.1, 4, 9.5, 175.5, 7},
                       {1, 52.9, 70.9, 4, 138.4, 175.8, 3},
                       {1, 59.5, 114.8, 8, 0, 141.1, 6},
                       {2, 81.1, 119.1, 8, 49.3, 103.3, 5},
                       {2, 11.4, 61.3, 10, 94.0, 127.3, 5},
                       {2, 150.7, 162.9, 7, 65.1, 128.4, 10},
                       {2, 192.0, 202.1, 7,22.8, 41.8, 4},
                       {3, 61.3, 99.2, 8, 0, 139.9, 16},
                       {3, 62.7, 112.2, 9, 67.7, 83.1, 6},
                       {3, 139.9, 166.7, 1, 99.1, 189.4, 7},
                       {3, 51.9, 99.2, 10, 10.3, 114.4, 7},
                       {3, 112.3, 143.8, 8, 87.9, 113.7, 5},
                       {4, 49.3, 70.9, 10, 61, 67.6, 5},
                       {4, 9.5, 138.4, 1, 70.9, 247.1, 7},
                       {4, 74.3, 107, 9, 43.2, 68.7, 4},
                       {4, 78.4, 96.8, 5, 85.5, 131, 4},
                       {4, 153.7, 175.8, 5, 86.7, 86.7, 4},
                       {4, 135.9, 171.8, 7, 50.1, 52.4, 4},
                       {4, 122.2, 148.3, 10, 0, 73.8, 6},
                       {4, 74.3, 126.3, 8, .9, 113.7, 6},
                       {4, 71.9, 78.4, 5, 85.5, 121, 6},
                       {5, 86.7, 121, 4, 71.9, 175.8, 13},
                       {5, 5.1, 37.3, 1, 202.1, 241.3, 14},
                       {5, 121, 153.3, 4, 71.9, 114, 7},
                       {5, 59.4, 65.8, 2, 148.1, 153.3, 3},
                       {5, 39.4, 56.0, 1, 112.9, 205.2, 10},
                       {6, 34.7, 80.7, 9, 0, 94.2, 7},
                       {6, 81.7, 86.5, 3, 77.9, 114.2, 3},
                       {7, 50.1, 103.6, 10, 58.4, 73.8, 4},
                       {7, 119.6, 128.4, 2, 156.8, 162.9, 5},
                       {8, 66.9, 69.6, 6, 97.2, 101.2, 4},
                       {8, 121, 166.5, 3, 78.9, 108.3, 12},
                       {8, 135.7, 141.1, 9, 67.7, 72.8, 3},
                       {8, 76.2, 87.9, 8, 99, 141.1, 5},
                       {8, 75.2, 113.7, 4, 74.3, 148.3, 5},
                       {8, 0, 37.9, 3, 49.8, 61.3, 6},
                       {8, 76.2, 119.1, 1, 59.5, 238.8, 6},
                       {8, 43, 93.7, 3, 47.6, 150.5, 8},
                       {9, 83.1, 132.8, 1, 19.9, 245.2, 10},
                       {9, 27.7, 138.2, 5, 39.4, 109.1, 5},
                       {9, 0, 47.7, 6, 79.8, 83.6, 5},
                       {9, 43.2, 68.7, 4, 74.3, 107, 4},
                       {9, 70.6, 101.1, 6, 15.6, 62.4, 5},
                       {9, 50.2, 72.8, 8, 49.3, 141.1, 6},
                       {10, 7.2, 58.3, 3, 70.1, 99.2, 7},
                       {10, 63.0, 110.2, 8, 49.3, 76.8, 4},
                       {10, 110.2, 123.1, 2, 11.4, 119.1, 5} };

int rand_int(lb,ub)
int lb,ub;
{
double tmp;
long random();
tmp = random() % (01 << 28);
tmp /= (01 << 28);
tmp *= 1 + ub - lb;
tmp = lb + tmp;
return((int) tmp);
}

float ans_real(x)
char x[];
{
float n;
printf(x);
printf(" ");
scanf("%f",&n);
getchar();
return(n);
}

int ans_int(x)
char x[];
{
int n;
printf(x);
printf(" ");
scanf("%d",&n);
getchar();
return(n);
}

get_char()
{
char c;
c = getchar();
if (c != '\n') getchar();
return(c);
}

ans_char(x)
char x[];
{
printf(x);
printf(" ");
return(get_char());
}

ans_string(x,ans)
char x[], ans[];
{
printf(x);
printf(" ");
/* setvbuf(stdin,NULL,_IOLBF,1000); */
scanf("%s",ans); /* seems to be buffer size of 200, only returns last block */
getchar();
}


disp_cnt_histo(cnt_array,array_len,histo_len,max_stars)
int cnt_array[];
int array_len,histo_len,max_stars;
{
int i,j,k,n,len_max,histo_step,max_count,total_matches,max,min;
int histo[1001];
float fstep,flen,fn;
if (histo_len > array_len) histo_len = array_len;
flen = array_len;
for (i = 0; i <= histo_len; i++) histo[i] = 0;
max = -999999; min = 9999999;
for (i = 0; i < array_len; i++)  /*  find max and min counts */
  {
  n = cnt_array[i];
  if (n > max) max = n;
  if (n < min) min = n;
  }
printf("Raw min counts = %d  max counts = %d\n",min,max);
/* flen = max-min; */
flen = max-0;
for (i = 0; i < array_len; i++)   /* bin the counts */
  {
  histo[(int)(histo_len*(float)i/array_len)] += cnt_array[i];
/*  fn = cnt_array[i];
  histo[(int)(.49+histo_len*fn/flen)]++; */
  }

max_count = -999;
total_matches = 0;
for (i = 0; i <= histo_len; i++) 
  {
  if (histo[i] > max_count) max_count = histo[i];
  total_matches += histo[i];
  }
if (max_count <= max_stars) max_stars = max_count;
fstep = array_len/histo_len;  
printf("\nSteps of %6.2f  ",fstep);
printf("Max height = %5d Total matches = %5d\n",max_count,total_matches);
for (i = max_stars-1; i >= 0; i--) 
  {
  for (j = 0; j <= histo_len; j++)
    if (histo[j] > max_count*((float)i/max_stars)) printf("X");
                                              else printf(" ");
  printf("|\n");
  }
for (j = histo_len+1; j >= 0; j--)
  if ((int)(j*fstep)%100 < fstep) printf("+"); else printf(".");
printf("\n");
}


set_options()  
{  
char x;  
use_scaled_fdg = 0;
/*
make_data = ('y' == ans_char("make artificial data? (y/n)"));
*/
make_data = 0;
if (make_data)
  {
  old_style = 'y' == ans_char("old style? (y/n)");
  c_num = 2; r_num = 1;
  if (old_style)
    {
/*  c_num = ans_int("how many chromosomes?"); */
/*  r_num = ans_int("how many hidden runs? (0,1)");  */
  m_num = ans_int("how many random marker sets?");
  if (m_num > 0)
    mch_num = ans_int("how many matches per marker set?");
    rm_num = ans_int("how many markers per run?");
    rs_num = ans_real("start run where?");
    rc_num = ans_int("how many centimorgans per run? ( < 100 )");
/*    dr_num = ans_int("duplicate run on first chrom? (0,1)"); */
    rf_num = ans_int("how many pair flips on second chrom? (0)");
    art_run_frac = (float)rc_num/100.0;
    art_run_cent = (rs_num + (float)rc_num/2.0)/100.0;
    run_fstart = rs_num/100.0;
    run_fstop = run_fstart+(float)rc_num/100.0;
    }
    else
    {
    art_gene_num = ans_int("how many genes on a chromosome? (5500)");
    art_sim_prob = ans_real("prob that a background gene is duplicated? (.2)");
    run_keep_frac = ans_real("retain what fraction of the run genes? (.3)");
    rf_num = ans_int("how many pair flips on second chrom? (0)");
    flip_all = 'y' == ans_char("do flips over whole chromosome? (y/n)");
    }
  d_num = ans_int("how many data sets?");
  randseed = ans_int("what is random seed?");
  ans_string("what is the name of the output data file?",out_file_name);
  if (d_num > 1) prob_cutoff = ans_real("what percent cutoff? (1)");
  make_artif_data(1);
  }
else d_num = 1;
if (d_num > 1) verbose = 0; else verbose = 1;
x = ans_char("Run, Cluster, Fast-run? (r,c,f)");
use_adhore = (x == 'a');
use_clust = (x == 'c');
use_fast  = (x == 'f');
use_run  = (x == 'r');
if (use_fast || use_run) toss_single = 1;
/*
if (use_clust) toss_single = 'y' == ans_char("toss singeltons? (y/n)"); 
*/
toss_single = 1;
if (use_adhore) return;

min_len = ans_int("what is minimum length run? (3)");  
len_cutoff = 99999;

if (use_run && 0)
{
hack = ans_int("what is maximum swap distance? (999)");
can_skip = ans_int("can skip how many markers? (0)");
can_tol = ans_int("can tolerate how many out-of-order markers? (0)");
}
if (use_clust && 0)
  can_skip = ans_int("can skip how many markers? (0)");

/* save_subruns = ('y' == ans_char("save subset runs? (y/n)")); */
save_subruns = 1;
if (!use_clust) 
  {
 /*  use_scaled_fdg = 'y' == ans_char("scaled reordering factor? (y/n)");*/
    use_scaled_fdg = 1;  
  fixed_fdg = fdg = ans_real("what is reordering fudge factore? (2)");
  }
if (use_clust)
  {
  use_sd = ('s' == ans_char("use Fold or SD for density cutoff? (f/s)"));
  clust_density = ans_real("what is the density cutoff? (2)");
  clust_diff_max = ans_real("what is maximum cluster size diff? (20)");
  do_left = ('y' == ans_char("do left extend? (y/n)"));
  }
while (rand_type < 1 || rand_type > 3)
  rand_type= ans_int("what randomization type? 1) uniform 2) chrom 3) matched?");
if (rand_type != 2 && use_clust)
  {printf("sorry, only type 2 is available for clustering\n"); exit(0);}
max_cycles = ans_int("how many cycles of randomization (100)");

/*
toss_frac = ans_int("use 1/N of data (1)");
if (toss_frac > 1) save_set = ans_int("save which set? (0-(N-1))");
              else save_set = 0;
*/
toss_frac = 1; save_set = 0;
}

int test_cnt,found_cnt;
double avx,avy,avx2,avy2,avxy,sdx,sdy;
double max_y,min_y,max_x,min_x;
calc_ave_sd(xi,yi,stage)
double xi,yi;
int stage;
{
if (stage == 1)    /* initialization of variables */
  {
  found_cnt = test_cnt = avx = avy = avxy = avx2 = avy2 = 0;
  max_x = max_y = -9999999;
  min_x = min_y =  9999999;
  }
if (stage == 2)   /* accumulating data */
  {
  test_cnt++;
  if (xi > max_x) max_x = xi;  if (xi < min_x) min_x = xi;
  if (yi > max_y) max_y = yi;  if (yi < min_y) min_y = yi;
  avx += xi; avy += yi; avxy += xi * yi;
  avx2 += xi * xi;   avy2 += yi * yi;
  if (xi > 0) found_cnt++;
  }
if (stage >= 3)  /* print out the results */
  {
  avx /= (float)test_cnt; avy /= (float)test_cnt; avxy /= (float)test_cnt;
  avx2 /= (float)test_cnt; avy2 /= (float)test_cnt;
  sdx = pow(avx2 - avx*avx, .5); sdy = pow(avy2 - avy*avy, .5);
  printf("min x= %5.2f max x= %5.2f ave x= %5.2f  sd x= %5.2f\n",
          min_x, max_x, avx, sdx);
  printf("min y= %5.2f max y= %5.2f ave y= %5.2f  sd y= %5.2f found= %5.2f\n", 
          min_y, max_y, avy, sdy, (float)found_cnt/test_cnt);
  }
}


sort_run(inpts,ptr,top)
int inpts[],ptr[],top;
{
int i,j,j_top,last_swap,temp;
j_top = -999;
last_swap = top;
for (i = 1; i <= top; i++)
  {
  if (j_top == last_swap) break; else  j_top = last_swap;
  for (j = 1; j < j_top; j++)
  if (inpts[ptr[j]] > inpts[ptr[j+1]])
    {
    temp = ptr[j];
    ptr[j] = ptr[j+1];
    ptr[j+1] = temp;
    last_swap = j;
    }
  }
/*
for (i = 1; i <= top; i++) printf("%4d %4d  %d \n",i,ptr[i],inpts[ptr[i]]);
*/
}


make_artif_data(set)
int set;
{
int i,j,k,c,ch1,ch2,n0,n1,j0,j1;
int art_chrom_len,art_run_start,art_run_stop;
int run_gene_start,run_gene_stop,runnum;
char or1,or2;
float loc,last_loc,d1;
int name[3][20000],cloc[3][20000],strand[3][20000],runmem[20000];
int cnt_array[1001];
float locs[20000],locsx[20000];
char orient;
FILE *fopen(),*fp_out,*fp_out1,*fp_out2,*fp_out3,*fp_out4;
srandom(randseed+set);
fp_out = fopen(out_file_name,"w");
fp_out1 = fopen("tmp1","w");
fp_out2 = fopen("tmp2","w");
fp_out3 = fopen("tmp3","w");

for (i = 0; i <= 1000; i++) cnt_array[i] = 0;
if (old_style) goto do_old;
art_chrom_len = 23000000;  /* 23MB is average length for Arab. */
/* art_gene_num  = 5500;       average number of genes on a chrom in Arab */
art_run_frac = .2;        /* run is 20% of chrom */
art_run_cent = .5;        /* run is at the center */
/* art_sim_prob = .2;         chance that a gene on both chrom 1 and 2 */
run_fstart = art_run_cent - art_run_frac/2.0;
run_fstop = art_run_cent + art_run_frac/2.0;
art_run_start = (art_run_cent - art_run_frac/2.0)  * (float) art_chrom_len;
art_run_stop  = (art_run_cent + art_run_frac/2.0)  * (float) art_chrom_len;
run_gene_start = (art_run_cent - art_run_frac/2.0)  * (float) art_gene_num;
run_gene_stop  = (art_run_cent + art_run_frac/2.0)  * (float) art_gene_num;

rm_num = art_gene_num * art_run_frac;
rs_num = art_run_start;

for (c = 1; c <= 2; c++)
for (i = 1; i <= art_gene_num; i++)  /* assign random strands, or not */
  if (1) strand[c][i] = rand_int(1,2)*2-3; else  strand[c][i] = 1;
  
for (c = 1; c <= 2; c++)             /* all different genes */ 
for (i = 1; i <= art_gene_num; i++)  
  name[c][i] = c * 1000000 + i;

for (c = 1; c <= 2; c++)            /* make some the same */
for (i = 1; i <= art_gene_num; i++)  
if (rand_int(1,1000) < art_sim_prob * 1000.0)
  name[c][i] = name[rand_int(1,2)][rand_int(1,art_gene_num)];

/*
for (i = 1; i <= art_gene_num; i++) 
if (rand_int(1,1000) < art_sim_prob * 1000.0)
  {
  name[1][i] = name[1][rand_int(1,art_gene_num)];
  name[2][i] = name[2][rand_int(1,art_gene_num)];
  }
for (i = 1; i <= art_gene_num; i++)
if (rand_int(1,1000) < art_sim_prob * 1000.0)
  name[2][i] = name[1][rand_int(1,art_gene_num)];
*/

for (i = 1; i <= art_gene_num; i++)  mrk_use[i] = 0;
runnum = 0;
for (c = 1; c <= 2; c++)
for (i = 1; i <= art_gene_num; i++)
  {
  if (i < run_gene_start)   cloc[c][i] = rand_int(1,art_run_start);
  if (i > run_gene_stop)  cloc[c][i] = rand_int(art_run_stop,art_chrom_len);
  if (i >= run_gene_start && i <= run_gene_stop) 
    {
    cloc[c][i] = rand_int(art_run_start,art_run_stop);
    if (c == 2) runmem[++runnum] = i;
    if (c == 2 && rand_int(1,1000) < run_keep_frac * 1000.0) 
      {
      name[2][i] = name[1][i];
      cloc[2][i] = cloc[1][i];
      strand[2][i] = strand[1][i];
      mrk_use[i] = 1;
      }
    }
  }

if (flip_all) 
  {
  for (i = 1; i <= art_gene_num; i++) runmem[i] = i;
  runnum = art_gene_num;
  }
sort_run(cloc[2],runmem,runnum);

for (i = 1; i <= rf_num; i++) /* random neighbor flips */
  {
  j = rand_int(1,runnum-1);
  j0 = runmem[j]; j1 = runmem[j+1]; 
  k = name[2][j0]; name[2][j0] = name[2][j1]; name[2][j1] = k;
  k = strand[2][j0]; strand[2][j0] = -1*strand[2][j1]; strand[2][j1] = -1*k;
  k = mrk_use[j0]; mrk_use[j0] = mrk_use[j1]; mrk_use[j1] = k;
  }

for (i = 1; i <= art_gene_num; i++)
  if (mrk_use[i] == 1) 
    cnt_array[(int)(1000*(float)cloc[2][i]/art_chrom_len)]++;  
disp_cnt_histo(cnt_array,1000,70,10);

for (c = 1; c <= 2; c++)
for (i = 1; i <= art_gene_num; i++)
  {
  fprintf(fp_out,"A%06d %d %d\n",name[c][i],c,cloc[c][i]);
  if (strand[c][i] > 0) orient = '+'; else orient = '-';
  if (c == 1)   fprintf(fp_out1,"1%08d%c\n",cloc[c][i],orient);
  if (c == 2)   fprintf(fp_out2,"2%08d%c\n",cloc[c][i],orient);
  }

for (i = 1; i <= art_gene_num; i++)
for (j = 1; j <= art_gene_num; j++)
if (name[1][i] == name[2][j])
  {
  if (strand[1][i] > 0) or1 = '+'; else or1 = '-';
  if (strand[2][j] > 0) or2 = '+'; else or2 = '-';
  fprintf(fp_out3,"1%08d%c	2%08d%c\n", cloc[1][i],or1,cloc[2][j],or2);
  }
/*
for (i = 1; i <= art_gene_num; i++)
  if (name[2][i] < 2000000) 
    fprintf(fp_out3,"1%08d%c	2%08d%c\n",cloc[1][i],orient,cloc[2][i],orient); */

fclose(fp_out);
fclose(fp_out1);
fclose(fp_out2);
fclose(fp_out3);
system("sort tmp1 > tmp0");
system("cp tmp0 tmp1");
system("sort tmp2 > tmp0");
system("cp tmp0 tmp2");
printf("written to tmp1, tmp2, tmp3 && %s\n",out_file_name);
return;

do_old:
for (i = 1; i <= r_num; i++)  /* for each hidden run */
  {
  d1 = (float)rc_num/(float)rm_num;
  ch1 = 1; ch2 = 2;
  for (j = 1; j <= rm_num; j++) locsx[j] = locs[j] = rs_num + j*d1;
/*  for (j = 1; j <= rm_num; j++)
    locsx[j] = locs[j] = rand_int(art_run_start,art_run_stop); */
  for (j = 1; j <= rm_num; j++) /* for each marker in the run */
    {
    fprintf(fp_out,"ARTIFX%d %d %5.2f\n",j,ch1,locs[j]);
    if ((j%2) == 0) orient = '+'; else orient = '-';
    orient = '+';
    fprintf(fp_out1,"1%08d%c\n",(int)(10000.0*locs[j]),orient);
    if (0 && dr_num > 0)       /* duplicate run on first chrom */
      fprintf(fp_out,"ARTIFX%d %d %5.2f\n",j,ch1,loc+rm_num*d1+10);
    }
  for (j = 1; j <= rf_num; j++) /* random flips in the run */
    {
    k = rand_int(1,rm_num-1);
    loc = locs[k]; locs[k] = locs[k+1]; locs[k+1] = loc;
    }
  for (j = 1; j <= rm_num; j++) /* write them out */
    {
    fprintf(fp_out,"ARTIFX%d %d %5.2f\n",j,ch2,locs[j]);
    n0 = (int)(10000.0*locsx[j]);
    n1 = (int)(10000.0*locs[j]);
    if ((j%2) == 0) orient = '+'; else orient = '-';
    orient = '+';
    fprintf(fp_out2,"2%08d%c\n",n1,orient);
      fprintf(fp_out3,"1%08d%c	2%08d%c\n",n0,orient,n1,orient);
    }
  }
for (i = 1; i <= m_num; i++)     /* for each random markers set */
  {
  for (j = 1; j <= mch_num; j++) /* for each marker in the set */
    {
    k = ch1;
    ch1 = rand_int(1,c_num);
    last_loc = loc;
    loc = ((float)rand_int(0,999999))/10000.0;
    if ((i%2) == 0) orient = '+'; else orient = '-';
    orient = '+';
    if (ch1 == 1) fprintf(fp_out1,"1%08d%c\n",(int)(10000.0*loc),orient);
    if (ch1 == 2) fprintf(fp_out2,"2%08d%c\n",(int)(10000.0*loc),orient);
    if (j > 1 && ch1 != k) 
      if (ch1 == 2)
      fprintf(fp_out3,"1%08d%c	2%08d%c\n",
        (int)(10000.0*last_loc),orient,(int)(10000.0*loc),orient); else
      fprintf(fp_out3,"1%08d%c	2%08d%c\n",
        (int)(10000.0*loc),orient,(int)(10000.0*last_loc),orient);

    if (i < 10)  fprintf(fp_out,"ARTIF0%d %d %5.2f\n",i,ch1,loc);
            else fprintf(fp_out,"ARTIF%d %d %5.2f\n",i,ch1,loc);
    }
/*
  ch2 = rand_int(1,c_num);
  while (ch1 == ch2) ch2 = rand_int(1,c_num);    key difference here
  loc = (float)rand_int(1,1000)/10.0;
  fprintf(fp_out,"ARTIF%d %d %5.2f\n",i,ch2,loc);
*/
  }
fclose(fp_out);
fclose(fp_out1);
fclose(fp_out2);
fclose(fp_out3);
system("sort tmp1 > tmp0");
system("cp tmp0 tmp1");
system("sort tmp2 > tmp0");
system("cp tmp0 tmp2");
printf("written to tmp1, tmp2, tmp3 && %s\n",out_file_name);
}

sort_loc_block(top)       /* 3rd order sort on location */
int top;
{
int i,j,cnt,last_swap,j_top;
struct marker_info temp;
i = cnt = 1;
while (strcmp(mrk[top].name,mrk[top+i].name) == 0 &&
       (mrk[top].chrom == mrk[top+i].chrom)) {cnt++; i++;}
last_swap = top+cnt-2;
for (i = 1; i < cnt; i++)
  {
  j_top = last_swap;
  for (j = top; j <= j_top; j++)
    if (mrk[j].loc > mrk[j+1].loc)
      {
      temp = mrk[j];
      mrk[j] = mrk[j+1];
      mrk[j+1] = temp;
      last_swap = j;
      }
  }
}

sort_marker_info3(kept)
int kept;
{
int i;
for (i = 1; i <= kept; i++)
  if (strcmp(mrk[i].name,mrk[i-1].name) != 0 ||
     (mrk[i].chrom != mrk[i-1].chrom)) sort_loc_block(i);
}


sort_name_block(name_top)  /* 2nd order sort on chrom number */
int name_top;
{
int i,j,cnt,last_swap,j_top;
struct marker_info temp;
i = cnt = 1;
while (strcmp(mrk[name_top].name,mrk[name_top+i].name) == 0) {cnt++; i++;}
last_swap = name_top+cnt-2;
for (i = 1; i < cnt; i++)
  {
  j_top = last_swap;
  for (j = name_top; j <= j_top; j++)
    if (mrk[j].chrom > mrk[j+1].chrom)
      {
      temp = mrk[j];
      mrk[j] = mrk[j+1];
      mrk[j+1] = temp;
      last_swap = j;
      }
  }
}

sort_marker_info2(kept)
int kept;
{
int i;
for (i = 1; i <= kept; i++)
  if (strcmp(mrk[i].name,mrk[i-1].name) != 0) sort_name_block(i);
}


sort_marker_info1(top)
int top;
{
int i,j,j_top,last_swap;
struct marker_info temp;
last_swap = top-1;
for (i = 1; i <= top; i++)
  {
  j_top = last_swap;
  for (j = 1; j <= j_top; j++)
  if (strcmp(mrk[j].name,mrk[j+1].name) > 0)
    {
    temp = mrk[j];
    mrk[j] = mrk[j+1];
    mrk[j+1] = temp;
    last_swap = j;
    }
  }
}


sort_chrom(chmk)
struct chrm_marks chmk[];
{
int i,j,j_top,last_swap,top;
struct chrm_marks temp;
top = chmk[0].index;   /* how many marks on this chromosome */
last_swap = top-1;
for (i = 1; i <= top; i++)
  {
  j_top = last_swap;
  for (j = 1; j <= j_top; j++)
  if (mrk[chmk[j].index].loc > mrk[chmk[j+1].index].loc)
    {
    temp = chmk[j];
    chmk[j] = chmk[j+1];
    chmk[j+1] = temp;
    last_swap = j;
    }
  }
}


read_data()
{
FILE *fopen(),*data_fp;
char data_file_name[100],str[10000],t_str1[100],t_str2[100],*sp,*tmp_sp;
int i,j,k,chrom,read,kept,name_top,chrom_top,n,cnt,c2,name_bot,format;
float loc;
int cnt_array[1001];
max_set_1 = name_top = max_chrom = kept = read = 0;
data_fp = NULL;
if (make_data) data_fp = fopen(out_file_name,"r");
while (!data_fp)
  {
  ans_string("what is the name of the data?",data_file_name);
  data_fp = fopen(data_file_name,"r");
  if (!data_fp) printf("sorry, can't find that file\n");
  }

fgets(str,1000,data_fp);
rewind(data_fp);
format = 0;
if (sscanf(str,"%s (%d %f",t_str1,&chrom,&loc) == 3)  format = 1;
if (sscanf(str,"%s %d %f",t_str1,&chrom,&loc) == 3)  format = 2;

if (format == 1) printf("looks like many-on-one-line format\n"); else
if (format == 2) printf("looks like one-per-line format\n"); else
                 printf("looks like old format\n");

i = 1;
if (format == 1 && 0) /*  (chrom, loc) */
while (fscanf(data_fp,"%s (%d %f",t_str1,&mrk[i].chrom,&mrk[i].loc) == 3)
  {
  read++;
  if (i >= MAX_MARKS) {printf("too many markers\n"); exit(0);}
  strcpy(mrk[i++].name,t_str1);
  while (fscanf(data_fp,", %d %f",&mrk[i].chrom, &mrk[i].loc) == 2)
    strcpy(mrk[i++].name,t_str1);
  fscanf(data_fp,"%s",str);
  if (!strstr(str,")")) {printf("error on input line %d\n",read); exit(0);}
  }

if (format == 1 && 1)     /* (loc, chrom) */
while (fscanf(data_fp,"%s (%f %d",t_str1,&mrk[i].loc,&mrk[i].chrom) == 3)
  {
  if (!two_sets && mrk[i].chrom > max_set_1) max_set_1 = mrk[i].chrom;
  if (strcmp(t_str1,"genome") == 0)
    {two_sets = 1; fscanf(data_fp,"%s",str); continue;}
  read++;
  if (i >= MAX_MARKS) {printf("too many markers\n"); exit(0);}
  if (two_sets) mrk[i].chrom += max_set_1; 
  strcpy(mrk[i++].name,t_str1);
  while (fscanf(data_fp,", %f %d",&mrk[i].loc, &mrk[i].chrom) == 2)
    {
    if (!two_sets && mrk[i].chrom > max_set_1) max_set_1 = mrk[i].chrom;
    if (two_sets) mrk[i].chrom += max_set_1; 
    strcpy(mrk[i++].name,t_str1);
    }
  fscanf(data_fp,"%s",str);
  if (!strstr(str,")")) {printf("error on input line %d\n",read); exit(0);}
  }

if (format == 2)
while (fscanf(data_fp,"%s %d %f",t_str1,&mrk[i].chrom,&mrk[i].loc) == 3)
  {
  if (!two_sets && mrk[i].chrom > max_set_1) max_set_1 = mrk[i].chrom;
  if (strcmp(t_str1,"genome") == 0) {two_sets = 1; continue;}
  read++;
  if (i >= MAX_MARKS) {printf("too many markers\n"); exit(0);}
  if (two_sets) mrk[i].chrom += max_set_1; 
  strcpy(mrk[i++].name,t_str1); 
  }
kept = i-1;

if (format == 0)
while (fgets(str,1000,data_fp) != NULL) /* read in the file */
  {
  if (sscanf(str,"%s %s %f",t_str1,t_str2,&loc) == 3)
    {
    read++;
    tmp_sp = strrchr(t_str1,'-');
    if (tmp_sp == NULL) {printf("bad data, no hypen in %s\n",str); exit(0);}
    tmp_sp--;
    if (isdigit(*tmp_sp)) continue;
    if (*tmp_sp < 'A' || *tmp_sp > 'D') continue;
    *tmp_sp = '\0';      /* just keep the name */
    kept++;
    sp = t_str1;
    i = strlen(t_str1);
    if (t_str1[i-1] == '\"') t_str1[i-1] = '\0';
    if (t_str1[0] == '\"') sp++;
    strcpy(mrk[kept].name,sp);
    sp = t_str2;
    while (!isdigit(sp[0])) sp++;
    sscanf(sp,"%d",&chrom);
    mrk[kept].chrom = chrom;
    mrk[kept].loc = loc;
    if (verbose)
     printf("%s %d %f\n",mrk[kept].name,mrk[kept].chrom,mrk[kept].loc);
    } else
    {printf("data problem on line %d\n",read); exit(0);}
  }
fclose(data_fp);
printf("read %d lines kept %d markers\n",read,kept);
sort_marker_info1(kept);     /* sort on marker names */


for (i = 0; i < MAX_CHROM; i++) min_loc[i] = 0;
for (i = 1; i <= kept; i++)    /* get rid of negative locations */
  if (mrk[i].loc < min_loc[mrk[i].chrom]) min_loc[mrk[i].chrom] = mrk[i].loc;
for (i = 1; i <= kept; i++)  mrk[i].loc -= min_loc[mrk[i].chrom];

if (toss_single)
{
i = 1;
while (i <= kept)            /* get rid of 1-match entries */
  {
  if (i < kept && strcmp(mrk[i].name,mrk[i+1].name) == 0) {i++; continue;}
  if (i > 1    && strcmp(mrk[i].name,mrk[i-1].name) == 0) {i++; continue;}
  if (d_num == 1)
  printf("dropping single-match %s\n",mrk[i].name);
  for (j = i; j < kept; j++) mrk[j] = mrk[j+1];
  kept--;
  }
printf("have %d different marker locations\n",kept);
}

sort_marker_info2(kept);       /* 2nd order sort on marker chromosomes */

for (i = 0; i < MAX_CHROM; i++) chrom_len[i] = 0;
for (i = 1; i <= kept; i++)
  if (mrk[i].loc > chrom_len[mrk[i].chrom]) chrom_len[mrk[i].chrom]=mrk[i].loc;
if (0) /* reverse each chrom left for right */
for (i = 1; i <= kept; i++)
  mrk[i].loc = chrom_len[mrk[i].chrom]-mrk[i].loc;


sort_marker_info3(kept);       /* 3rd order sort on location on chromosome */
i = 1;
while (++i <= kept)            /* get rid of any duplicate entries */
  if ((strcmp(mrk[i].name,mrk[i-1].name) == 0 && 
      mrk[i].chrom == mrk[i-1].chrom &&
      mrk[i].loc == mrk[i-1].loc))
    {
    printf("dropping duplicate %s %d %5.2f\n",
            mrk[i].name,mrk[i].chrom,mrk[i].loc);
    for (j = i; j < kept; j++) mrk[j] = mrk[j+1];
    kept--;
    i--;                       
    }
printf("have %d different marker locations\n",kept);


k = 0;
mrk[kept+1].name[0] = '#';
i = kept+1;
if (toss_frac > 1)
while (--i > 0)             /* use 1/toss_frac of the marker names */
  {
  if (strcmp(mrk[i].name,mrk[i-1].name) == 0) continue;
  if ((k++ % toss_frac) == save_set) continue;  
  strcpy(t_str1,mrk[i].name);
  while (strcmp(mrk[i].name,t_str1) == 0)
    {for (j = i; j <= kept; j++) mrk[j] = mrk[j+1];  kept--;}
  }
printf("using %d different marker locations\n",kept);
max_mark = kept;

n = name_top = chrom_top = 0;
for (i = 1; i <= max_mark; i++)  /* set mrk_num, name_cnt, chrom_cnt and top */
  {
  if (verbose)
    printf("%15s %d %f\n",mrk[i].name,mrk[i].chrom,mrk[i].loc);
  if (mrk[i].chrom > max_chrom) max_chrom = mrk[i].chrom;
  if (max_chrom >= MAX_CHROM) {printf("too many chromosomes\n"); exit(0);}
  if (strcmp(mrk[i].name,mrk[i-1].name) != 0)        /* new marker name */
    {name_top = i; mrk[i].name_cnt = 1; n++;}
     else mrk[name_top].name_cnt++;
  mrk[i].mrk_num = n;
  if (mrk[i].chrom != mrk[i-1].chrom || mrk[i].mrk_num != mrk[i-1].mrk_num)
    {chrom_top = i; mrk[i].chrom_cnt = 1;} else mrk[chrom_top].chrom_cnt++;
  mrk[i].name_top = name_top;
  }
printf("%d different locations  %d different markers\n", kept,n);

printf("number of markers with a given number of occurrences\n");
j = 0;
for (i = 1; i <= max_mark; i++)
  {
  if (mrk[i].name_cnt < 100) mrk_cnt[mrk[i].name_cnt]++;
  if (mrk[i].name_cnt > j) j = mrk[i].name_cnt;
  }
for (i = 1; i <= j; i++) printf("%d: %d\n",i,mrk_cnt[i]);


gml = kept; gdm = n;
total_len = 0;
for (i = 1; i <= max_chrom; i++) /* set chmk[][] array data for each chrom */
  {
  k = 0;
  for (j = 1; j <= max_mark; j++)    /* scan all markers */
    if (mrk[j].chrom == i)       /* found a marker on chrom i */
      {
      if (++k > MAX_MRK_CHRM) 
        {printf("too many markers on a chromosome\n"); exit(0);}
      n = chmk[i][k].index = j;
      chmk[i][k].mrk_num = mrk[j].mrk_num;
      chmk[i][k].loc = mrk[j].loc;
      chmk[i][k].name_top = name_top = mrk[j].name_top;
      chmk[i][k].name_cnt = mrk[name_top].name_cnt;
      }
  chmk[i][0].index = k;
  sort_chrom(chmk[i]);          /* sort by location on chromosome */
  for (j = 1; j <= k; j++) mrk[chmk[i][j].index].pos = j;
  total_len += chmk[i][k].loc;
  }
ave_dist = total_len/(float)max_mark;
if (use_scaled_fdg) fdg = fixed_fdg * ave_dist;
if (verbose)
for (i = 1; i <= max_chrom; i++)
  {
  printf("CHROM %d *******\n",i);
  for (j = 1; j <= chmk[i][0].index; j++)
    {
    k = chmk[i][j].index;
    printf("%3d %3d %15s %6.2f %3d\n",
         j,mrk[k].mrk_num,mrk[k].name,chmk[i][j].loc,chmk[i][j].name_cnt);
    }
  }

for (i = 1; i <= max_chrom; i++) ch_len[i] = chmk[i][chmk[i][0].index].loc;
if (0)
for (i = 1; i <= max_chrom; i++)
  {
  printf("CHROM %d *******\n",i);
  for (j = 0; j <= 1000; j++) cnt_array[j] = 0;
  for (j = 1; j <= chmk[i][0].index; j++)
    cnt_array[(int)(1000*(float)chmk[i][j].loc/ch_len[i])]++;  
   disp_cnt_histo(cnt_array,1000,70,5);
  }

/* fill in mkch[][] */
for (i = 1; i <= max_mark; i++)
for (j = 1; j <= max_chrom; j++)
  mkch[i][j] = -1;
for (i = 1; i <= max_mark; i++)
  if (mrk[i].chrom != mrk[i-1].chrom  || mrk[i].mrk_num != mrk[i-1].mrk_num)
    mkch[mrk[i].mrk_num][mrk[i].chrom] = i;



for (i = 1; i <= max_chrom; i++)
for (k = 1; k <= max_chrom; k++)
  com_cnt[i][k] = 0;
for (i = 1; i <= max_chrom; i++)
  {
  cnt = chmk[i][0].index;                 /* how many markers on chrom i */
  for (j = 1; j <= cnt; j++)
  for (k = 1; k <= max_chrom; k++)
    {
    n = get_count(i,j,k);                 /* how many times is j on k */ 
    if (n > 0) com_cnt[i][k]++;
    }
  }

printf("\nCommon markers\n");
printf("         ");
for (i = 1; i <= max_chrom; i++) printf("%4d ",i);
printf("\n");
for (i = 1; i <= max_chrom; i++)
  {
  printf("%2d %4d: ",i,chmk[i][0].index);
  for (k = 1; k <= max_chrom; k++)
    if (i == k) printf("  *  "); else printf("%4d ", com_cnt[i][k]);
  printf("\n");
  }

/* transfer data to cm[] */
for (i = 1; i <= max_chrom; i++) 
  {
  cnt = cm[i][0][0] = chmk[i][0].index;
  for (j = 1; j <= cnt; j++)
  for (k = 1; k <= 2*max_chrom; k++)
    cm[i][j][k] = -1;
  }
for (i = 1; i <= max_chrom; i++)   /* for each chromosome */
  {
  cnt = cm[i][0][0];                   
  for (j = 1; j <= cnt; j++)       /* for each marker on the chromosome */
    {
    cm[i][j][2*i-1] = chmk[i][j].loc;
    if (chmk[i][j].name_cnt > 1)
      {
      name_bot = chmk[i][j].name_top + chmk[i][j].name_cnt-1;
      for (k = chmk[i][j].name_top; k <= name_bot; k++)
        {
        if (k == chmk[i][j].index) continue;
        c2 = mrk[k].chrom; loc = mrk[k].loc;      
        if (cm[i][j][2*c2-1] == -1)  cm[i][j][2*c2-1] = loc;
                                else cm[i][j][2*c2] = loc;
        }
      }
    }
  }

}


print_table()
{
int i,j;
j = 0;
for (i = 0; i < 53; i++) 
  {
  printf("%2d:%5d-%2d %2d ",i,seen_known[i],seen_matched[i],(int)known[i][6]);
  if (i%5 == 4) printf("\n");
  if (seen_known[i] > 0) j++;
  }
printf("matched %d\n",j);
}

bubble(inpts,top)
int *inpts,top;
{
int i,j,j_top,last_swap,temp;
j_top = -999;
last_swap = top;
for (i = 1; i <= top; i++)
  {
  if (j_top == last_swap) break; else  j_top = last_swap;
  for (j = 1; j < j_top; j++)
  if (inpts[j] > inpts[j+1])
    {
    temp = inpts[j];
    inpts[j] = inpts[j+1];
    inpts[j+1] = temp;
    last_swap = j;
    }
  }
}

float find_cluster_cl(len1)
float len1;
{
int i,j,b1,t1,mb,mt,mark,top;
float min,diff,bdif,tdif,len2;
top = cl[0].mark;
min = 99999;
for (i = 1; i <= top-1; i++)       /* find the closest pair */
  if (cl[i+1].mark != cl[i].mark && min > (diff = cl[i+1].loc2 - cl[i].loc2))
    {min = diff; b = i; t = i+1;}
if (diff >= 9999) return(-1);
mb = cl[b].mark; mt = cl[t].mark;
for (i = 1; i <= top; i++) 
  if (cl[i].mark == mb || cl[i].mark == mt) cl[i].used= 1; else cl[i].used= 0;
cl[b].used = cl[t].used = 2;
len2 = cl[t].loc2 - cl[b].loc2;
while (len2 < (2 * len1 + 20))     /* extend ends of cluster */
  {
  t1 = b1 = -1;
  bdif = tdif = 99999;
  for (j = t+1; j <= top; j++)
    if (!cl[j].used) {t1 = j; tdif = cl[j].loc2 - cl[t].loc2; break;}
  for (j = b-1; j >= 1; j--) 
    if (!cl[j].used) {b1 = j; bdif = cl[b].loc2 - cl[j].loc2; break;}
  if (t1 == b1) break;             /* couldn't extend cluster */
  if (tdif < bdif)  {mark = cl[t1].mark; t = t1;}
  if (bdif <= tdif) {mark = cl[b1].mark; b = b1;}
  for (j = 1; j <= top; j++) if (cl[j].mark == mark) cl[j].used = 1;
  cl[b].used = cl[t].used = 2;
  len2 = cl[t].loc2 - cl[b].loc2;
  }
return(len2);
} 

sort_cl()
{
int i,j,j_top,last_swap,top;
struct cluster_info temp;
top = cl[0].mark;
last_swap = top-1;
for (i = 1; i <= top; i++)
  {
  j_top = last_swap;
  for (j = 1; j <= j_top; j++)
  if (cl[j].loc2 > cl[j+1].loc2)
    {
    temp = cl[j];
    cl[j] = cl[j+1];
    cl[j+1] = temp;
    last_swap = j;
    }
  }
}


int trm[MAX_RUN_LEN+2];
int runs[MAX_RUNS][MAX_RUN_LEN+2],usruns[MAX_RUNS][MAX_RUN_LEN+2];
float runs1[MAX_RUNS][MAX_RUN_LEN+2],runs2[MAX_RUNS][MAX_RUN_LEN+2];


hash(matched,try)
int matched,try;
{
int i,val;
val = 1;
/*for (i = 1; i <= matched; i++)  val = (val * (trm[i]+try)) % HASH_SZ; */
/*for (i = 1; i <= matched; i++)  val = val * (trm[i]+try) + try; */
for (i = 1; i <= matched; i++)  val = val * (trm[i]+try) >> try;
val = val  % HASH_SZ;
if (val < 0) val = -val;
return(val);
}

check_hash(matched)
int matched;
{
int i,hash_val,run_loc,try,mask;
tried_runs++;
for (try = 1; try <= 20; try++)
  {
  if (try > biggest_hash_try) 
    {
/*    printf("hash try = %2d saved runs = %5d tried runs = %d\n",
      try,max_runs,tried_runs);
    mask = (1<<6)-1;
    for (i = 1; i <= matched; i++) printf("%3d/%1d ",run_marks[i]>>6,
                                                run_marks[i]&mask);
    printf("\n");
    for (i = 1; i <= matched; i++) printf("%5.2f ",locs1[i]);
    printf("\n");
    for (i = 1; i <= matched; i++) printf("%5.2f ",locs2[i]);
    printf("\n"); */
    biggest_hash_try = try;
    }
  hash_val = hash(matched,try);
  run_loc = hash_tbl[hash_val];
  if (run_loc == 0)                        /* a new run, not in table */
    {hash_tbl[hash_val] = max_runs+1; return(0);}
  if (runs[run_loc][0] == matched) 
    {
    for (i = 1; i <= matched; i++) if (runs[run_loc][i] != trm[i]) break;
    if (i > matched)
       return(run_loc);      /* run already in table    */
    }
  }
printf("too many tries in check_hash() max_runs = %d\n",max_runs);
exit(0);
}


c2_dup(c1,c2,matched,run_marks)  /* are any markers on C2 used twice? */
int c1,c2,matched;
int run_marks[];
{
int cnt,index,mask,i,j,mark,m,alt;
int ind_arr[MAX_RUN_LEN+2];
cnt = 0;
mask = (1<<6)-1;
for (i = 1; i <= matched; i++)
  {
  mark = run_marks[i];
  m = mark >> 6;
  alt = mark&mask;
  index = get_index(c1,m,c2,alt);
  ind_arr[i] = index;
  for (j = 1; j < i; j++) 
    if (ind_arr[j] == index) 
      {cnt++; break;}
  }
return(cnt);
}

save_run(c1,c2,matched,up)
int matched,up;
{
int i;
if (!use_fast)
   return(save_runx(c1,c2,matched,up,&run_marks[0],&locs1[0],&locs2[0]));
save_runx(c1,c2,matched,up,&run_marks[0],&locs1[0],&locs2[0]);
if (save_subruns)
  {
  for (i = 1; i <= matched-min_len; i++)
    if (save_runx(c1,c2,matched-i,up,&run_marks[i],&locs1[i],&locs2[i])) break;
  for (i = 1; i <= matched-min_len; i++)
    if (save_runx(c1,c2,matched-i,up,&run_marks[0],&locs1[0],&locs2[0])) break;
  }
}
  
save_runx(c1,c2,matched,up,run_marks,locs1,locs2)
int c1,c2,matched,up;
int run_marks[];
float locs1[],locs2[];
{
int i,j,k,len,missed_one_old,missed_one_new,found;
for (i = 1; i <= matched; i++) trm[i] = run_marks[i];
bubble(trm,matched);
if (matched > MAX_RUN_LEN)
   {printf("run too long\n"); return(0);}
found = 0;

if (save_subruns)
  {
  found = check_hash(matched);
  if (found) return(1+runs[found][MAX_RUN_LEN+1]);  /* was it up or down? */
  }
  else
for (i = 1; i <= max_runs; i++)
  {
  missed_one_old = missed_one_new = 0;
  len = runs[i][0];
  if (save_subruns && len != matched) continue;
  j = k = 1;
  while (j <= len && k <= matched)
    {
    if (runs[i][j] == trm[k]) {j++; k++; continue;}
    if (runs[i][j] < trm[k]) {j++; missed_one_old = 1; continue;}
    k++; missed_one_new = 1;
    }
  if (j <= len) missed_one_old = 1;
  if (k <= matched) missed_one_new = 1;
  if (!missed_one_new) {found = 1; break;}  /* same or a subset of old one */
  if (!missed_one_old)                      /* same or a superset of old one */
    {
    found = 1;
    usruns[i][0] = runs[i][0] = matched;
    for (k = 1; k <= matched; k++) 
      {runs[i][k]=trm[k]; runs1[i][k]=locs1[k]; runs2[i][k]=locs2[k];
       usruns[i][k] = run_marks[k];}
    break;
    }
  }
if (!found)
  {
  max_runs++;
  if (max_runs >= MAX_RUNS) 
    {printf("%d too many runs\n",max_runs); exit(0);}
  usruns[i][0] = runs[max_runs][0] = matched;
  runs[max_runs][MAX_RUN_LEN+1] = up;
             /* temp storage for next value */
  runs1[max_runs][0] = matched-c2_dup(c1,c2,matched,run_marks);  
  for (k = 1; k <= matched; k++) 
    {runs[max_runs][k] = trm[k]; runs1[max_runs][k] = locs1[k];
     runs2[max_runs][k] = locs2[k]; usruns[max_runs][k] = run_marks[k];}
  }
return(found);
}

final_set(c1,c2,cycle)
int c1,c2,cycle;
{
int i,j,k,len1,len2,n1,n2,missed1,missed2,bgnum,mask,len_nd,cnt;
float spc,spc1,spc2,ftmp,c11,c12,c21,c22,lin1,lin2,lst_loc;
double ss;
struct run_info *rp;
int cnts[MAX_RUN_LEN+1],link[MAX_RUN_LEN+1];
for (i = 1; i <= MAX_RUN_LEN; i++) cnts[i] = link[i] = 0;
if (!save_subruns)            /* find and mark all subset runs */
for (i = 1; i <= max_runs; i++)            
for (j = i+1; j <= max_runs; j++)
  {
  if (0 == (len1 = runs[i][0])) continue;
  if (0 == (len2 = runs[j][0])) continue;
  n1 = n2 = 1;
  missed1 = missed2 = 0;
  while (n1 <= len1 && n2 <= len2)
    {
    if (runs[i][n1] == runs[j][n2]) {n1++; n2++; continue;}
    if (runs[i][n1] < runs[j][n2]) {n1++; missed1 = 1; continue;}
    n2++; missed2 = 1;
    }
  if (n1 <= len1) missed1 = 1;
  if (n2 <= len2) missed2 = 1;
  if (!missed1) {runs[i][0] = 0; break;}  /* i is same or a subset of j run */
  if (!missed2) runs[j][0] = 0;  /* i is same or a superset of j run */
  }

for (i = 1; i <= max_runs; i++)
  {
  if (0 == (len1 = runs[i][0])) continue;  /* skip subset runs */
  run_cnt++;
  cnts[len1]++; stat_cnts[c1][c2][len1]++;
  if (len1 > longest_run) longest_run = len1;
  c11 = c12 = runs1[i][1];
  c21 = c22 = runs2[i][1];
  lin1 = lin2 = 0;
  for (k = 1; k <= len1; k++)    /* find smallest and biggest in run */
    {
    ftmp = runs1[i][k]; if (ftmp < c11) c11 = ftmp; if (ftmp > c12) c12 = ftmp;
    ftmp = runs2[i][k]; if (ftmp < c21) c21 = ftmp; if (ftmp > c22) c22 = ftmp;
    if (k > 1)
      {
      if (ftmp > runs2[i][k-1]) lin1++;
      if (ftmp < runs2[i][k-1]) lin1--;
      } 
    if (k > 2)
      {
      lst_loc =  (runs2[i][k-1] + runs2[i][k-2])/2.0;
      if (ftmp > lst_loc) lin2++;
      if (ftmp < lst_loc) lin2--;
      } 
    }
  cnt = lin2 = 0;
  for (j = 1; j <= len1; j++)   /* pierre's version of global linearity */
    {
    ftmp = runs2[i][j];
    for (k = 1; k <= len1; k++)
      {
      if (ftmp == runs2[i][k]) continue;
      cnt++;
      if (k < j) {if (ftmp > runs2[i][k]) lin2++; else lin2--;}
      if (k > j) {if (ftmp < runs2[i][k]) lin2++; else lin2--;}
      } 
    }
  if (cnt > 0)  lin2 = lin2/cnt; else lin2 = 0;
/*  bgnum = xknown_run(len1,c1,c2,runs1[i],runs2[i]); */
  bgnum = zknown_run(c1,c2,c11,c12,c21,c22);
  if (!use_clust && ftmp < runs2[i][1]) {ftmp = c21; c21 = c22; c22 = ftmp;}
  ss = pow(c11-c12,2.0) + pow(c21-c22,2.0);
  spc = 0;
  for (k = 2; k <= len1; k++)
    {
    spc1 = runs1[i][k]-runs1[i][k-1]; spc2 = runs2[i][k]-runs2[i][k-1];
    spc += (1.0+fabs(spc1-spc2))/(1.0+fabs(spc1)+fabs(spc2));
    }
  spc /= (float)(len1-1);

  if (cycle == 0)   /* save them */
    {
    if (run_cnt >= MAX_ALL_RUNS) {printf("no space in all_runs\n"); exit(0);}
    rp = &all_runs[run_cnt];
    rp->c1 = c1; rp->c2 = c2; rp->len = len1; rp->ss = ss; rp->spc = spc;
    rp->tmp = rp->tmp1 = rp->cnt = rp->cnt1 = rp->cnt_tot = 0; 
    rp->bgnum = bgnum; rp->lin1 = lin1; rp->lin2 = lin2;
    rp->c1m1 = c11; rp->c1m2 = c12; rp->c2m1 = c21; rp->c2m2 = c22;
    rp->len_nd = runs1[i][0];  /* just stored it there temporarily */
    all_runs[0].len = run_cnt;
    rp->link = -1;
    if (c1c2start[c1][c2][len1] == 0) c1c2start[c1][c2][len1] = run_cnt;
       else all_runs[link[len1]].link = run_cnt;
    link[len1] = run_cnt;
    if (verbose)
      {
      printf("%5d: c1= %2d c2= %2d match= %2d/%2d %5.1f-%5.1f %5.1f-%5.1f ss= %5ld l= %4.2f",
            run_cnt,c1,c2,len1,rp->len_nd,c11,c12,c21,c22,(long)ss,lin2);
      if (bgnum >= 0) printf("    *%d ", bgnum);
      printf("\n");
      }
    }
  if (cycle > 0 && 0 < (j = c1c2start[c1][c2][len1]))   /* update statistics */
    {
    len_nd = runs1[i][0];  /* just stored it there temporarily */
    while (j != -1)
      {
      rp = &all_runs[j];
      rp->cnt_tot++;
      if (ss <= rp->ss) rp->cnt++;
/*      if (rp->len == len1 && ss <= rp->ss && rp->tmp < cycle)
        {rp->cnt++; rp->tmp = cycle;} */
/*      if (rp->len_nd == len_nd && spc <= rp->spc && rp->tmp1 < cycle)
        {rp->cnt1++; rp->tmp1 = cycle;} */
      j = rp->link;
      }
    }
  if (1 && cycle == 0 && verbose)
  {
  mask = (1<<6)-1;
  for (j = 1; j <= len1; j++) printf("%3d/%1d ",usruns[i][j]>>6,
                                                usruns[i][j]&mask);
  printf("\n");
  for (j = 1; j <= len1; j++) printf("%5.1f ",runs1[i][j]);
  printf("\n");
  for (j = 1; j <= len1; j++) printf("%5.1f ",runs2[i][j]);
  printf("\n");
  }
  }
if (cycle > 0)
for (i = 1; i <= MAX_RUN_LEN; i++)
  if (cnts[i] > 0) {stat_cnts1[c1][c2][i]++; rand_match_cnt[i] += cnts[i];}
}

known_run(matched,c1,c2,locs1,locs2)
int matched,c1,c2;
float locs1[],locs2[];
{
int i,k,n;
float ave_loc1,ave_loc2;
/*ave_loc1 = ave_loc2 = 0;
for (i = 1; i <= matched; i++) ave_loc1 += locs1[i];
ave_loc1 /= matched;
for (i = 1; i <= matched; i++) ave_loc2 += locs2[i];
ave_loc2 /= matched; */
ave_loc1 = (locs1[1]+locs1[matched])/2.0;
ave_loc2 = (locs2[1]+locs2[matched])/2.0;


n = -999;

for (k = 0; k < 53; k++) 
  if (c1 == (int)known[k][0] && c2 == (int) known[k][3] &&
    ave_loc1 >= known[k][1] && ave_loc1 <= known[k][2] &&
    ave_loc2 >= known[k][4] && ave_loc2 <= known[k][5])
    {
    n = k;
/*    printf("    *%d ", k);  */
    seen_known[k]++;
    if (matched > seen_matched[k]) seen_matched[k] = matched;
    }
return(n);
}

xknown_run(matched,c1,c2,locs1,locs2)
int matched,c1,c2;
float locs1[],locs2[];
{
int k;
float c11,c12,c21,c22;
c11 = c21 = 99999;
c12 = c22 = -99999;
for (k = 1; k <= matched; k++)
  {
  if (locs1[k] < c11) c11 = locs1[k];
  if (locs1[k] > c12) c12 = locs1[k];
  if (locs2[k] < c21) c21 = locs2[k];
  if (locs2[k] > c22) c22 = locs2[k];
  }
for (k = 0; k < 53; k++) 
  if (c1 == (int)known[k][0] && c2 == (int) known[k][3] &&
    c11 <= known[k][1] && c12 >= known[k][2] &&
    c21 <= known[k][4] && c22 >= known[k][5])
  return(k);
return(-999);
}

yknown_run(c1,c2,ave_loc1,ave_loc2)
int c1,c2;
float ave_loc1,ave_loc2;
{
int i;
for (i = 0; i < 53; i++) 
  if (c1 == (int)known[i][0] && c2 == (int) known[i][3] &&
    ave_loc1 >= known[i][1] && ave_loc1 <= known[i][2] &&
    ave_loc2 >= known[i][4] && ave_loc2 <= known[i][5])
    return(i);
return(-999);
}

zknown_run(c1,c2,c11,c12,c21,c22)
int c1,c2;
float c11,c12,c21,c22;
{
int i;
for (i = 0; i < 53; i++) 
  if (c1 == (int)known[i][0] && c2 == (int) known[i][3] &&
    c11 <= known[i][1] && c12 >= known[i][2] &&
    c21 <= known[i][4] && c22 >= known[i][5])
  return(i);
return(-999);
}

get_count(c1,m,c2)  /* how many times does the mth marker on c1 occur on c2 */
int c1,m,c2;
{
int mrk_num,index,count;
mrk_num = chmk[c1][m].mrk_num;
index = mkch[mrk_num][c2];
if (index == -1) return(0);
count = mrk[index].chrom_cnt;
return(count);
}

get_index(c1,m,c2,n) 
int c1,m,c2,n;
{
int mrk_num,index;
mrk_num = chmk[c1][m].mrk_num;
index = mkch[mrk_num][c2];
return(index+n-1);
}

float get_loc(c1,m,c2,n) 
int c1,m,c2,n;
{
int mrk_num,index;
mrk_num = chmk[c1][m].mrk_num;
index = mkch[mrk_num][c2];
return (mrk[index+n-1].loc);
}

get_m(c1,m,c2,n) 
int c1,m,c2,n;
{
int mrk_num,index;
mrk_num = chmk[c1][m].mrk_num;
index = mkch[mrk_num][c2];
return (mrk[index+n-1].pos);
}


/* Warning! Warning! Warning! Warning! Warning! Warning! Warning! 
Reading the code in this procedure may be hazardous to your health */
find_runs(c1,c2,pm1,pm2,matched,ud,max_mch,alt,can_skip,can_tol,last,small)
int c1,c2,pm1,pm2,matched,ud,max_mch,alt,can_skip,can_tol;
float last,small;
{
int i,j,chr,cnt,counter,u,d,m1,m2,om2,omatched,bgnum,c2_alt,top_swap,n,index;
int m2c2cnt,tol,found;
float ave_dist,ave_loc1,ave_loc2,tloc,olast,c1b,c1t,ftmp;
float c1m1loc,c1m2loc,c2m1loc,c2m2loc,oc1m2loc;
if (matched >= MAX_RUN_LEN) return;
m1 = mark[pm1]; om2 = m2 = mark[pm2];     /* get the "real" mark #s  */
m2c2cnt = get_count(c1,m2,c2);            /* how many times is m2 on c2 */ 
if (c1 == c2) m2c2cnt--;                  /* don't count yourself */
if (m1 == m2 && m2c2cnt == 0) return;     /* no start in c2 */
cnt = chmk[c1][0].index;                  /* how many markers on chrom c1 */
oc1m2loc = chmk[c1][m2].loc; omatched = matched; olast = last;  

/* if can swap m2 to before first marker in run, then try skipping it 
   or if can_skip > 0 can use one of those to skip it */
if (matched > 0 && (m2c2cnt > 0) && (pm2 < cnt))
  if (oc1m2loc-small <= fdg)
  find_runs(c1,c2,pm1,pm2+1,matched,ud,max_mch,-1,can_skip,can_tol,last,small);
  else if (can_skip > 0)
find_runs(c1,c2,pm1,pm2+1,matched,ud,max_mch,-1,can_skip-1,can_tol,last,small);

ftmp = oc1m2loc;
for (i = pm2; i <= cnt; i++)        /* find how far ahead you can swap */
  {
  tloc = chmk[c1][mark[i]].loc;
  if (i- pm2 > hack) break;         /* quick hack */
  if (tloc - ftmp > fdg) break;
  if (tloc < ftmp) ftmp = tloc;
  }
top_swap = i-1;
for (i = pm2; i <= top_swap; i++)
  {
  m2c2cnt = get_count(c1,mark[i],c2);     /* how many times is m2 on c2 */
  if (c1 == c2) m2c2cnt--;                /* don't count yourself */
  if (m2c2cnt == 0)                       /* if no match in c2 */
    if (pm2 == cnt) goto endrun; else      /* last marker in c1 */
    if (i > pm2)    continue;    else      /* don't swap */
      {
  find_runs(c1,c2,pm1,pm2+1,matched,ud,max_mch,-1,can_skip,can_tol,last,small);
      return;
      }

  u = ud; d = !u;                          /* run up or run down? */
  matched = omatched; last = olast;
  mark[pm2] = mark[i]; mark[i] = om2;      /* swap them */
  m1 = mark[pm1]; m2 = mark[pm2];          /* get the "real" mark #s */

  c2_alt = 0;
  m2c2cnt = get_count(c1,m2,c2);  /* how many times is m2 on c2 */
  if (matched == 0 && alt < 0) 
    {                             /* start run at each m2 match on c2 */
    for (j = 1; j <= m2c2cnt; j++)
      {
      index = get_index(c1,m2,c2,j);
      if (index == chmk[c1][m2].index) continue;  /* only if c1 == c2 */
     find_runs(c1,c2,pm1,pm2,matched,ud,max_mch,j,can_skip,can_tol,last,small);
      }
   goto next;
    }
  if (alt >= 1)
    {c2m2loc = get_loc(c1,m2,c2,alt); c2_alt = alt;}
    else
    for (j = 1; j <= m2c2cnt; j++)
      {
      index = get_index(c1,m2,c2,j);
      if (index == chmk[c1][m2].index) continue;  /* only if c1 == c2 */
      tloc = mrk[index].loc;
      if ((c2_alt == 0) ||
        (u && (last-tloc < fdg) && (c2m2loc < last || tloc < c2m2loc)) ||
        (d && (tloc-last < fdg) && (c2m2loc > last || tloc > c2m2loc)))
         {c2m2loc = tloc; c2_alt = j;}
      }
  if (c2_alt == 0) goto next;

  tol = 0;
  if (d && c2m2loc - last > fdg)
    if (can_tol > 0) tol = 1; else d = 0;         
  if (u && last - c2m2loc > fdg)
    if (can_tol > 0) tol = 1; else u = 0;         
  if (c1 == c2 && matched > 0)             /* no overlap in same chrom */
    {
    c1m2loc = chmk[c1][m2].loc;
    if (locs2[1] <= locs1[1] && c2m2loc >= locs1[1]) u = d = 0;
    if (locs2[1] >= locs1[1] && locs2[1] <= c1m2loc) u = d = 0;
    if (locs2[1] >= locs1[1] && c2m2loc <= c1m2loc)  u = d = 0;
    }

  if (u || d)                              /* got a run going */
    {
    if (matched > 0 && (locs1[matched] - chmk[c1][m2].loc > fdg)) 
      {printf("whoops, bogus run\n"); exit(0);}
    matched++;
    locs1[matched] = chmk[c1][m2].loc;
    locs2[matched] = c2m2loc;
    if (locs1[matched] < small) small = locs1[matched];
    run_marks[matched] = (m2<<6)+c2_alt;
    if (u && c2m2loc > last) last = c2m2loc;  
    if (d && c2m2loc < last) last = c2m2loc;
    if (save_subruns && matched >= min_len)
      {
      found = save_run(c1,c2,matched,u);
/*      if (found && (found-1) == u) goto next; */
      }
    if (pm2 < cnt && matched < max_mch)
      {find_runs(c1,c2,pm1,pm2+1,matched,ud,max_mch,-1,can_skip,can_tol-tol,last,small);
      goto next;}
    }

endrun:                                    /* broke run or pm2 = cnt */
  if (matched < min_len) goto next;        /* if too short, forget it */
  c1t = c1b = locs1[1];
  for (j = 1; j <= matched; j++)           
    {ftmp = locs1[j]; if (ftmp < c1b) c1b = ftmp; if (ftmp > c1t) c1t = ftmp;}
  ave_loc1 = (locs1[1]+locs1[matched])/2.0;
  ave_loc2 = 0;
  for (j = 1; j <= matched; j++) ave_loc2 += locs2[j];
  ave_loc2 /= matched;
  if (!save_subruns) save_run(c1,c2,matched,u);
next:
  mark[i] = mark[pm2]; mark[pm2] = om2;
  if (alt >= 1) break;
  }
}

already_used(index,matched)
int index,matched;
{
int i;
for (i = 1; i <= matched; i++)
  if (index2[i] == index) return(1);
return(0);
}

find_run_clust(c1,c2,m2,ud,matched,can_skip,alt,small)
int c1,c2,m2,ud,matched,can_skip,alt;
float small;
{
int i,j,mrk_cnt1,cnt2,run_done,index,c2_alt,num,save_index;
int u,d,new_matched,skipit,m2c2cnt,overlap;
float tloc,c1m2loc,c2m2loc,tlen,diff,last;
if (0 && c2 == c1) return;                   /* don't compare to yourself */
u = ud; d = !u;
mrk_cnt1 = chmk[c1][0].index;           /* how many markers on c1 */
cnt2 = get_count(c1,m2,c2);             /* how many m2s on c2 */
c1m2loc = chmk[c1][m2].loc;             /* location of m2 on c1 */

if (matched == 0 && alt == 0)           /* need to start a run */
  {
  for (i = 1; i <= cnt2; i++)           /* for each match in c2 */
    find_run_clust(c1,c2,m2,ud,0,can_skip,i,1e50);    
  return;
  }

if (cnt2 == 0)                          /* if no m2s on c2 */
  {
  if (matched > 0 && m2 < mrk_cnt1)     /* if in a run, skip it and go on */
    find_run_clust(c1,c2,m2+1,ud,matched,can_skip,0,small);
  return;
  }

/* if can swap m2 to before smallest marker in run, then try skipping it
   or if can_skip > 0 can use one of those to skip it  */
if ((matched > 0) && (cnt2 > 0) && (m2 < mrk_cnt1))
  if (c1m2loc-small <= fdg)
     find_run_clust(c1,c2,m2+1,ud,matched,can_skip,0,small); 
  else if (can_skip > 0)
     find_run_clust(c1,c2,m2+1,ud,matched,can_skip-1,0,small); 

run_done = 0;
new_matched = matched;
last = locs2[matched];
for (i = m2; i <= mrk_cnt1; i++)        /* do next any-order block on c1 */
  {
  if (chmk[c1][i].loc - c1m2loc > fdg) break; /* end of any-order block */
  c2_alt = skipit = overlap = 0;
  m2c2cnt = get_count(c1,i,c2);         /* how many times is m2 on c2 */
  if (alt > 0 && i == m2)               /* starting a run */
    {
    index = get_index(c1,i,c2,alt);
    if (index == chmk[c1][i].index) return;  /* only if c1 == c2 */
    last = c2m2loc = get_loc(c1,m2,c2,alt);
    c2_alt = alt;
    save_index = index;
    }
    else                                /* not starting a run */
    {
    for (j = 1; j <= m2c2cnt; j++)      /* find best match on c2 */
      {
      index = get_index(c1,i,c2,j);
      if (index == chmk[c1][i].index) continue;  /* only if c1 == c2 */
      if (already_used(index,new_matched)) continue;
      tloc = mrk[index].loc;
      if ((c2_alt == 0) || 
          (u && (last-tloc < fdg) && (c2m2loc < last || tloc < c2m2loc)) ||
          (d && (tloc-last < fdg) && (c2m2loc > last || tloc > c2m2loc)))
         {c2m2loc = tloc; c2_alt = j; save_index = index;}
      }
    if (c2_alt == 0) continue;          /* no match on c2 */
    if (c1 == c2 && new_matched > 0)    /* no overlap in same chrom */
      if ((locs2[1] <= small && c2m2loc >= small) ||
          (locs2[1] >= small && locs2[1] <= c1m2loc) ||
          (locs2[1] >= small && c2m2loc <= c1m2loc)) overlap = 1;
    if ((d && c2m2loc - last > fdg) || (u && last - c2m2loc > fdg))
       skipit = run_done = 1;           /* best match on c2 breaks run */
    }

  if (alt > 0) skipit = run_done = 0; 
  if (overlap) skipit = run_done = 1;
  if (!skipit)
    {
    for (j = new_matched; j > matched; j--)
      if ((u && c2m2loc < locs2[j]) || (d && c2m2loc > locs2[j]))
       {locs1[j+1]=locs1[j]; locs2[j+1]=locs2[j];
        run_marks[j+1]=run_marks[j]; index2[j+1]=index2[j];}
        else break;
      new_matched++;
      locs1[j+1] = chmk[c1][i].loc;
      locs2[j+1] = c2m2loc;
      index2[j+1] = save_index;
      run_marks[j+1] = (i<<6)+c2_alt;
      if (locs1[j+1] < small) small = locs1[j+1];
      if (save_subruns && new_matched >= min_len)
        save_run(c1,c2,new_matched,ud);
      if (!run_done && new_matched < MAX_RUN_LEN)
        find_run_clust(c1,c2,i+1,ud,new_matched,can_skip,0,small);
    if (new_matched == MAX_RUN_LEN) break;
    }
  }

if (!run_done && i <= mrk_cnt1 && new_matched < MAX_RUN_LEN)   
  if (matched == 0) 
  for (j = new_matched; j > matched; j--)
    {
    find_run_clust(c1,c2,i,ud,j,can_skip,0,small);
    if ((run_marks[j] >> 6) == m2) break;
    }
/*  else find_run_clust(c1,c2,i,ud,new_matched,can_skip,0,small); */
}


find_clust(c1,c2,m1,m2,matched,alt,can_skip,c2m1,c2m2,c2b,c2t)
int c1,c2,m1,m2,matched,alt,can_skip,c2m1,c2m2;
float c2b,c2t;
{
int i,j,m,cnt,cnt2,done,bgnum,index,c2_alt,tc2m1,tc2m2,tmatched,c1pos,over;
int update;
float tloc,c1b,c1t,c2m2loc,c1len,tlen,diff,density,fmm,expect_match;
float min_diff_ratio,pe,pv,psd;

cnt  = chmk[c1][0].index;                /* how many markers on c1 */
update = over = done = 0;

if (alt > 0)                             /* starting a cluster */
  {c2m2loc = get_loc(c1,m2,c2,alt); c2_alt = alt; m = get_m(c1,m2,c2,alt);}

if (alt < 0)                             /* extending a cluster */
  {
  cnt2 = get_count(c1,m2,c2);            /* how many m2s on c2 */
  diff = 1000 * ave_dist;                /* accept anything */
  c1len = chmk[c1][m2].loc - chmk[c1][m1].loc;
  c2m2loc = -1;                          /* if no match on c2 */
  for (i = 1; i <= cnt2; i++)            /* pick best match on c2 */
    {
    index = get_index(c1,m2,c2,i);
    if (index == chmk[c1][m2].index) continue;  /* don't match yourself */
    tloc = get_loc(c1,m2,c2,i);
    tlen = c2t - c2b;
    if (tloc > c2t) tlen = tloc-c2b;
    if (tloc < c2b) tlen = c2t-tloc;
    if (fabs(c1len-tlen) < diff) 
      {c2m2loc= tloc; c2_alt= i; diff= fabs(c1len-tlen); 
       min_diff_ratio = diff/(c1len+tlen); m= get_m(c1,m2,c2,i);}
    }  
  }

c1pos = 1+m2-m1;
if (0)   /* save everything so can go back and add if you want */
  {
  if (c1pos >= 5000) {printf("error in backfill\n"); exit(0);}
  clust_info[c1pos][1] = chmk[c1][m2].loc;
  clust_info[c1pos][2] = c2m2loc;
  clust_info[c1pos][3] = (m2<<6)+c2_alt;
  if (diff > clust_diff_max * ave_dist) clust_info[c1pos][0] = 0;
     else clust_info[c1pos][0] = 1; /* use it */  
  }

if (diff > (clust_diff_max * ave_dist)) c2m2loc = -1; 

tc2m1 = c2m1; tc2m2 = c2m2; tmatched = matched;  /* if no match */
if (c2m2loc != -1)                               /* got a match */
  {if (m < tc2m1) tc2m1= m;  if (m > tc2m2) tc2m2= m; tmatched= matched+1;}
/* density = (float)(tmatched+2)*2/((m2-m1)+(tc2m2-tc2m1));
  if (density < clust_density) done = 1; */
fmm = max_mark;
expect_match=(1-pow((fmm-(fmm/gdm-1))/fmm,(double)(1+tc2m2-tc2m1)))*(1+m2-m1);
if (use_sd)
  {
  pe = .5 *  (1+tc2m2-tc2m1)*(1+m2-m1)/(float)gdm;
  pv = .5 * ((1+tc2m2-tc2m1)*(1+m2-m1)/(float)gdm)*((gdm-1)/(float)gdm);
  psd = pow(pv,.5);
  if ((tmatched-pe)/psd < clust_density) done = 1;
  } else
  if (tmatched < clust_density * expect_match) done = 1;

if (c1 == c2 && matched > 0)             /* no overlap in same chrom */
  {
  c1b = chmk[c1][m1].loc; c1t = chmk[c1][m2].loc;
  if (c2b <= c1b && c2m2loc >= c1b)      over = done = 1;
  if (c2b >= c1b && c2b <= c1t)          over = done = 1;
  if (c2b >= c1b && c2m2loc <= c1t)      over = done = 1;
  if (over) {clust_info[c1pos][0] = 0; clust_info[c1pos][2] = -1;}
  }

if (!done && c2m2loc != -1)              /* add to the cluster */
  {
  matched++;
  if (matched >= MAX_RUN_LEN) done = 1;
  locs1[matched] = chmk[c1][m2].loc;
  locs2[matched] = c2m2loc;
  run_marks[matched] = (m2<<6)+c2_alt;
  if (c2m2loc < c2b) {c2b = c2m2loc; c2m1 = m;}
  if (c2m2loc > c2t) {c2t = c2m2loc; c2m2 = m;}
  if (0)  /* go back and add skipped genes to cluster if now fit in */
    {
    for (i = 1; i <= c1pos; i++)
      if (clust_info[i][0] == 0 && clust_info[i][2] > c2b 
                                && clust_info[i][2] < c2t)
      {clust_info[i][0] = 1; update = 1;}
    if (update)  /* include additional genes in cluster */
      {
      matched = 0;
      for (i = 1; i <= c1pos; i++) 
        if (clust_info[i][0] == 1)
          {
          matched++;
          locs1[matched] = clust_info[i][1];
          locs2[matched] = clust_info[i][2];
          run_marks[matched] = clust_info[i][3];
          if (matched >= MAX_RUN_LEN) {done = 1; break;}
          }
       }
    }
  if (save_subruns && matched >= min_len) save_run(c1,c2,matched,0);
  if (do_left && matched >= min_len && matched < MAX_RUN_LEN)
    left_extend_clust(c1,c2,m1-1,m2,matched,-1,can_skip,c2m1,c2m2,c2b,c2t);
  }

if (done && !save_subruns && matched-1 >= min_len) save_run(c1,c2,matched-1,0);
if (!done && m2 < cnt && matched < MAX_RUN_LEN)   
  find_clust(c1,c2,m1,m2+1,matched,-1,can_skip,c2m1,c2m2,c2b,c2t);
if (done && can_skip > 0 && m2 < cnt && matched < MAX_RUN_LEN)
  find_clust(c1,c2,m1,m2+1,matched,-1,can_skip-1,c2m1,c2m2,c2b,c2t);
/* if (can_skip > 0 && m2 < cnt && matched < MAX_RUN_LEN)
  find_clust(c1,c2,m1,m2+1,matched,-1,can_skip-1,c2m1,c2m2,c2b,c2t); */
}


left_extend_clust(c1,c2,m1,m2,matched,alt,can_skip,c2m1,c2m2,c2b,c2t)
int c1,c2,m1,m2,matched,alt,can_skip,c2m1,c2m2;
float c2b,c2t;
{
int i,j,m,cnt2,done,bgnum,index,c2_alt,tc2m1,tc2m2,tmatched,added;
float tloc,c1b,c1t,c2m1loc,c1len,tlen,diff,density,fmm,expect_match;

added=done = 0;
cnt2 = get_count(c1,m1,c2);            /* how many m1s on c2 */
diff = clust_diff_max * ave_dist;
c1len = chmk[c1][m2].loc - chmk[c1][m1].loc;
c2m1loc = -1;                          /* if no match on c2 */
for (i = 1; i <= cnt2; i++)            /* pick best match on c2 */
  {
  index = get_index(c1,m1,c2,i);
  if (index == chmk[c1][m1].index) continue;  /* don't match yourself */
  tloc = get_loc(c1,m1,c2,i);
  tlen = c2t - c2b;
  if (tloc > c2t) tlen = tloc-c2b;
  if (tloc < c2b) tlen = c2t-tloc;
  if (fabs(c1len-tlen) < diff) 
    {c2m1loc = tloc; c2_alt= i; diff = fabs(c1len-tlen); m = get_m(c1,m1,c2,i);}
  }  

tc2m1 = c2m1; tc2m2 = c2m2; tmatched = matched;  /* if no match */
if (c2m1loc != -1)                               /* got a match */
  {
  if (m < tc2m1) tc2m1= m;
  if (m > tc2m2) tc2m2= m;
  tmatched= matched+1;
  }

fmm = max_mark;                           /* total number of markers */
expect_match = (1-pow((fmm-(fmm/gdm-1))/fmm,(double)(1+tc2m2-tc2m1)))*(1+m2-m1);
if (tmatched < clust_density * expect_match) done = 1;

if (c1 == c2 && matched > 0)             /* no overlap in same chrom */
  {
  c1b = chmk[c1][m1].loc; c1t = chmk[c1][m2].loc;
  if (c2b <= c1b && c2m1loc >= c1b)      done = 1;
  if (c2b >= c1b && c2b <= c1t)          done = 1;
  if (c2b >= c1b && c2m1loc <= c1t)      done = 1;
  }

if (!done && c2m1loc != -1)              /* add to the cluster */
  {
  added = 1;
  matched++;
  if (matched >= MAX_RUN_LEN) done = 1;
  locs1--; locs2--; run_marks--;
  locs1[1] = chmk[c1][m1].loc;
  locs2[1] = c2m1loc;
  run_marks[1] = (m1<<6)+c2_alt;
  if (c2m1loc < c2b) {c2b = c2m1loc; c2m1 = m;}
  if (c2m1loc > c2t) {c2t = c2m1loc; c2m2 = m;}
  if (save_subruns && matched >= min_len) save_run(c1,c2,matched,0);
  }

if (!done && m1 > 1 && matched < MAX_RUN_LEN)   
  left_extend_clust(c1,c2,m1-1,m2,matched,-1,can_skip,c2m1,c2m2,c2b,c2t);
if (added)              /* added to the cluster */
  {locs1++; locs2++; run_marks++;}
}

find_clust_c1_c2(c1,c2,can_skip)
int c1,c2,can_skip;
{
int i,j,k,cnt,cnt2,max_match,index,c2m1;
float c2l;
max_runs = 0;
max_match = 999;
cnt  = chmk[c1][0].index;          /* how many markers on c1 */
for (i = 0; i < HASH_SZ; i++) hash_tbl[i] = 0;
for (i = 1; i < cnt; i++)          /* starting at each marker on c1 */
  {
  cnt2 = get_count(c1,i,c2);       /* how many times does it occur on c2 */
  for (j = 1; j <= cnt2; j++)      /* for each match on c2 */
    {
    index = get_index(c1,i,c2,j);  /* get jth match of i on c2 */
    c2m1 = get_m(c1,i,c2,j);          
    if (c1 == c2 && index == chmk[c1][i].index) continue;
    c2l = get_loc(c1,i,c2,j);      /* get its location on c2 */
    find_clust(c1,c2,i,i,0,j,can_skip,c2m1,c2m1,c2l,c2l);
    }
  }
}

find_runs_c1_c2(c1,c2,can_skip,can_tol)
int c1,c2,can_skip,can_tol;
{
int i,cnt,max_match;
               
for (i = 0; i < HASH_SZ; i++) hash_tbl[i] = 0;
tried_runs = biggest_hash_try = 0;
for (i = 0; i <= max_mark; i++) mark[i] = i;
max_runs = 0;
max_match = 999;                        /* can lower to < 10 */
cnt = chmk[c1][0].index;                /* how many markers on chrom c1 */
for (i = 1; i < cnt; i++)               /* starting at each marker in c1 */
  {
  if (use_fast)
    {
    find_run_clust(c1,c2,i,0,0,can_skip,0,0.0);        /* down */
    find_run_clust(c1,c2,i,1,0,can_skip,0,0.0);        /* up   */
    } else
    {
    find_runs(c1,c2,i,i,0,0,max_match,-1,can_skip,can_tol,1e50,1e50);
    find_runs(c1,c2,i,i,0,1,max_match,-1,can_skip,can_tol,-1e50,1e50);
    }
  }
}

float comp_prob(cycles,rp)
int cycles;
struct run_info *rp;
{
float prob,tmp;
tmp = rp->cnt_tot+1;
if (tmp < cycles)  tmp = cycles;
prob = 100.0 * (float)rp->cnt/tmp;
return(prob);
}

comp_sig_cnt(cycles,cutoff)
int cycles;
float cutoff;
{
int i,sig_cnt;
float prob;
struct run_info *rp;
sig_cnt = 0;
for (i = 1; i <= all_runs[0].len; i++)
  {
  rp = &all_runs[i];
  prob = comp_prob(cycles,rp);
  if (prob <= cutoff) sig_cnt++;
  }
return(sig_cnt);
}

set_cov_array()
{
int c,c2,dsp_len,loc,i,j,chrom_len,r,r1,r2,run_num,cnt,iprob,sig_cnt,found_one;
int max,start,stop,runcnt,nonruncnt,neg_runcnt,neg_nonruncnt;
int cutoff,poscnt,re,rs;
float tp,fp,tn,fn;
float cov_run_frac,cov_non_frac,backcnt,totalcnt;
FILE *fopen(),*fp_out;
struct run_info *rp,*rp1,*rp2;
int prob_cnt[100],tmp[1002];
run_num = all_runs[0].len;
for (cutoff = 0; cutoff < 100; cutoff++) 
  {
  sig_cnt = runcnt = 0;
  for (r1 = 1; r1 <= run_num; r1++)  /* mark non-significant ones */
  if (all_runs[r1].prob > cutoff ||
      all_runs[r1].len > len_cutoff) all_runs[r1].tmp1 = 1;
      else {all_runs[r1].tmp1 = 0; sig_cnt++;}
  for (c = 1; c <= max_chrom; c++)  
  for (c2 = 1; c2 <= max_chrom; c2++)
    {
    for (i = 0; i <= 1000; i++) tmp[i] = 0;
    chrom_len = ch_len[c];
    found_one = 0;
    for (r = 1; r <= run_num; r++)
      {
      rp = &all_runs[r];
      if (rp->c1 < c || rp->tmp1 == 1) continue;
      if (rp->c1 > c) break;
      if (rp->c2 != c2) continue;
      found_one = 1;
      rs = (rp->c1m1 * 100.0)/chrom_len;
      re = (rp->c1m2 * 100.0)/chrom_len;
      for (i = rs; i <= re; i++) tmp[i] = 1;
      }
    if (found_one) 
      for (i = 0; i <= 100; i++)
        if (tmp[i] == 1) runcnt++;
     }
  prob_cnt[cutoff] = runcnt;
  }
for (i = 100; i > 0; i--) prob_cnt[i] -= prob_cnt[i-1];
printf("coverage: probability distribution: 0 to 100\n"); 
totalcnt = backcnt = 0;
for (i = 0; i < 100; i++)
  {
  if (i%10 == 0) printf("\n %2d: ",i/10);
  printf("%4d ",prob_cnt[i]);
  totalcnt += prob_cnt[i];
  if (i >= 50) backcnt += prob_cnt[i]; /* assume everything after 50 is junk */
  }
backcnt = backcnt/50.0;
printf("\n");
printf("backcnt= %5.2f  total = %d\n",backcnt,(int)totalcnt);

if (0)   /* this should be done for clusters, not projection */
{
fp_out = fopen("roc","w");
poscnt = 0;
 for (i = 0; i < 50; i++)  
  {
  poscnt += prob_cnt[i];
  fp = backcnt*(1+i);
  tp = poscnt-fp;
  /*  fn = totalcnt-(backcnt*100) - tp;      */
  tn = (99-i)*backcnt;
  fn = totalcnt-poscnt-tn;
  fprintf(fp_out,"%6.3f  %6.3f %6.3f\n",fp/(fp+tn),tp/(tp+fn),tp/(tp+fp));
  /*
  printf("ROC %3d: tot= %4d fp= %4d tn= %4d tp= %4d fn= %4d  ",
    i,(int)poscnt,(int)fp,(int)tn,(int)tp,(int)fn);
    printf("%6.3f %6.3f\n",fp/(fp+tn),tp/(tp+fn)); */
  }
fclose(fp_out);
}

}



disp_sig_runs()
{
int c,c2,dsp_len,loc,i,j,chrom_len,r,r1,r2,run_num,cnt,iprob;
int lin_cnt,sig_cnt,found_one,rs,re;
int max,start,stop,runcnt,nonruncnt,neg_runcnt,neg_nonruncnt;
float tp,fp,tn,fn,lin_sum;
float cov_run_frac, cov_non_frac;
char symb;
struct run_info *rp,*rp1,*rp2;
char dsp[500];
int dsp_cnt[500],tmp[1002];
run_num = all_runs[0].len;
lin_cnt = lin_sum =  sig_cnt = 0;
for (r1 = 1; r1 <= run_num; r1++)  /* mark non-significant ones */
  {
  rp = &all_runs[r1];
  if (rp->prob > prob_cutoff || rp->len > len_cutoff) rp->tmp1 = 1;
  else 
    {
    rp->tmp1 = 0; 
    sig_cnt++; 
    if (rp->len > 3) {lin_sum += fabs(all_runs[r1].lin2); lin_cnt++;}
    }
  }
for (r1 = 1; r1 <= run_num; r1++)  /* mark subruns */
  {
  rp1 = &all_runs[r1];
  if (rp1->tmp1 == 1 ) continue;
  for (r2 = 1; r2 <= run_num; r2++)
    {
    if (r1 == r2) continue;
    rp2 = &all_runs[r2];
    if (rp2->c1 < rp1->c1) continue;
    if (rp2->c1 > rp1->c1) break;
    if (rp2->tmp1 == 1) continue;
    if (rp2->c1 > rp1->c1 || rp2->c2 > rp1->c2) break;
    if ((rp1->c1m1 <= rp2->c1m1 && rp1->c1m2 >= rp2->c1m2) &&
        (rp1->c2m1 <= rp2->c2m1 && rp1->c2m2 >= rp2->c2m2) &&
  1)                            /*  any subset */
/*        (rp1->ss <= rp2->ss))    only if less significant */
        {rp2->tmp1 = 1; continue;}
    if ((rp1->c1m1 >= rp2->c1m1 && rp1->c1m2 <= rp2->c1m2) &&
        (rp1->c2m1 >= rp2->c2m1 && rp1->c2m2 <= rp2->c2m2) &&
  1)                          /*     any subset */
/*        (rp2->ss <= rp1->ss))    only if less significant */
        {rp1->tmp1 = 1; break;}
    }
  }
cnt = 0;
for (r1 = 1; r1 <= run_num; r1++)
  {    
  rp = &all_runs[r1];
  if (rp->tmp1 == 0)
    {
    cnt++;
    iprob = rp->prob + .5;
    if (verbose)
      printf("%5d: c= %2d, %2d m= %2d %5.1f-%5.1f %5.1f-%5.1f p= %4.1f %5.1f=%5.1f l= %2d %3.2f\n",
      r1,rp->c1, rp->c2, rp->len, rp->c1m1, rp->c1m2, rp->c2m1, rp->c2m2,
      rp->prob, fabs(rp->c1m2 - rp->c1m1),  fabs(rp->c2m1 - rp->c2m2),
      (int)rp->lin1,rp->lin2);
    }
  }
printf("%d non-subset significant runs\n",cnt);
printf("ave | lin2 | over all significant runs > 3 = %5.2f\n",lin_sum/lin_cnt);
neg_runcnt = neg_nonruncnt = runcnt = nonruncnt = 0;
dsp_len = 70;
dsp[dsp_len+1] = '\0';
for (i = 0; i <= dsp_len; i++) dsp[i] = '.';
start = dsp_len * (art_run_cent-art_run_frac/2.0);
stop = dsp_len * (art_run_cent+art_run_frac/2.0);
for (i = start; i <= stop; i++) dsp[i] = 'X';
printf("%2d: %s\n\n",0,dsp);

for (c = 1; c <= max_chrom; c++)  /* region dsplay on c1*/
for (c2 = 1; c2 <= max_chrom; c2++)
  {
  for (i = 0; i <= 1000; i++) tmp[i] = 0;
  for (i = 0; i <= dsp_len; i++) {dsp[i] = '.'; dsp_cnt[i] = 0;}
  chrom_len = ch_len[c];
  found_one = 0;
  for (r = 1; r <= run_num; r++)
    {
    rp = &all_runs[r];
    if (rp->c1 < c || rp->tmp1 == 1) continue;
    if (rp->c1 > c) break;
    if (rp->c2 != c2) continue;
    if (rp->c1m1 < 0 || rp->c1m2 < 0) /* skip negative locations */
      {printf("negative location\n"); exit(0);}  
    found_one = 1;
    rs = (float)dsp_len * rp->c1m1/(float) chrom_len;
    re = (float)dsp_len * rp->c1m2/(float) chrom_len;
    for (i = rs; i <= re; i++)
      {
      dsp_cnt[i]++;
      if (c2 < 10) dsp[i] = '0'+c2;
            else   dsp[i] = 'a'+c2-10;
      }
    rs = (float)1000 * rp->c1m1/(float) chrom_len;
    re = (float)1000 * rp->c1m2/(float) chrom_len;
    for (i = rs; i <= re; i++)
      tmp[i] = 1;  
    }
  if (found_one) 
    {
    printf("%2d: %s\n",c,dsp);
    if (c != c2)
      for (i = 0; i <= 1000; i++)
       if (tmp[i] == 1)
         {if (i >= run_fstart*1000 && i <= run_fstop*1000)
            runcnt++; else nonruncnt++;} else
         {if (i >= run_fstart*1000 && i <= run_fstop*1000)
            neg_runcnt++; else neg_nonruncnt++;}
/*    max = 0;
    for (i = 0; i <= dsp_len; i++) if (dsp_cnt[i] > max) max = dsp_cnt[i];
    printf("    ");
    for (i = 0; i <= dsp_len; i++)
      {j =  9*dsp_cnt[i]/max; if (j > 0) printf("%1d",j); else printf(".");}
    printf("\n"); */
    }
  }

printf("\n%d runs, %5.2f %d significant, %d non-subruns\n",
        run_num,(float)sig_cnt/(float)run_num,sig_cnt,cnt);

cov_run_frac = (float)runcnt/(2.0*art_run_frac*10.0);
cov_non_frac = (float)nonruncnt/(2.0*(1.0-art_run_frac)*10.0);

/*
printf("\n%d runs, %5.2f %d significant, %d non-subruns\n",
        run_num,(float)sig_cnt/(float)run_num,sig_cnt,cnt); */


if (d_num > 0)
{
tp = runcnt; fp = nonruncnt; tn = neg_nonruncnt; fn = neg_runcnt;
printf("percent run covered = %f  non-run covered = %f\n",
     cov_run_frac, cov_non_frac);
printf("ROC: cutoff= %5.3f: fp= %4d tn= %4d tp= %4d fn= %4d  ",
  prob_cutoff,(int)fp,(int)tn,(int)tp,(int)fn);
printf("x = %6.3f y = %6.3f\n",fp/(fp+tn),tp/(tp+fn));
}
 calc_ave_sd((double)cov_run_frac,(double)cov_non_frac, 2);

/* summary display: shows center for each sig run  */
for (c = 1; c <= max_chrom; c++)  
  {
  for (i = 0; i <= dsp_len; i++) dsp[i] = '.';
  dsp[dsp_len+1] = '\0';
  chrom_len = ch_len[c];
  for (r = 1; r <= run_num; r++)
    {
    rp = &all_runs[r];
    if (rp->c1 < c || rp->tmp1 == 1) continue;
    if (rp->c1 > c) break;
    loc = dsp_len * ((rp->c1m1 + rp->c1m2)/2)/chrom_len;
    if (rp->c2 < 10) symb = '0'+rp->c2; else symb = 'a'+rp->c2-10;
    for (j = 0; j <= 20; j++)  /* find an empty spot */
      {
      if (loc-j >= 0 && dsp[loc-j] == '.') {dsp[loc-j] = symb; break;}
      if (loc-j >= 0 && dsp[loc-j] == symb) break;
      if (loc+j <= dsp_len && dsp[loc+j] == '.')
        {dsp[loc+j] = symb; break;}
      if (loc+j <= dsp_len && dsp[loc+j] == symb) break;
      }
    }
  printf("%2d: %s\n",c,dsp);
  }
}

disp_known() /* region display for runs in paper */
{
int i,j,r,c,c2,dsp_len,found_one;
float step;
char dsp[500];
printf("runs from Brandon's paper\n");
dsp_len = 70;
dsp[dsp_len+1] = '\0';
for (c = 1; c <= max_chrom; c++)  
for (c2 = 1; c2 <= max_chrom; c2++)
  {
  for (i = 0; i <= dsp_len; i++) dsp[i] = '.';
  step = (float)dsp_len/ch_len[c];
  found_one = 0;
  for (r = 0; r <= 52; r++)
    {
    if (known[r][0] < c) continue;
    if (known[r][0] > c) break;
    if (known[r][3] != c2) continue;
    found_one = 1;
    for (j = (int)known[r][1]; j <= (int)known[r][2]; j++)
       dsp[(int)(step*j)] = '0'+c2-1;
    }
  if (found_one) printf("%d: %s\n",c-1,dsp);
  }
exit(0);
}


do_adhore()
{
float cov_run_frac,cov_non_frac;
FILE *fopen(),*fp_in;
system("ADHoRe.pl tmp1 tmp2 tmp3 tmp.clt .9 20");
system("cat tmp.clt");
printf("Clusters identifed by Adhore. Statistical check next.\n");
system("brunitall");
fp_in = fopen("tmp_adhore_result","r");
fscanf(fp_in,"%f %f", &cov_run_frac,&cov_non_frac);
fclose(fp_in);
calc_ave_sd((double)cov_run_frac,(double)cov_non_frac, 2);
}

main()
{
int h,i,j,k,l,m,n,r,chr,cnt,cnt2,win,counter,u,d,ud,cycle,chrom_len;
int max_match,index,iprob,sig_cnt,rdata,pos;
char inorder,up,down;
char str[100];
int prob_cnt[1001];
float loc,ave_dist,win1_len,clust_len,loc1,loc2,ave_loc1,ave_loc2,last,tloc;
float fprob,sig_frc,ave_sig_frc,ave_runs,backcnt,totalcnt,poscnt;
float fp,tn,tp,fn;
struct run_info *rp;
FILE *fopen(),*fp_in,*fp_out;
set_options();

two_sets = ave_runs = ave_sig_frc = 0;
calc_ave_sd(0.0,0.0,1);
for (rdata = 1; rdata <= d_num; rdata++)
  {
  if (make_data) make_artif_data(rdata);
  if (use_adhore) 
    {do_adhore(); printf("random data set %d\n",rdata); continue;}
  read_data();
  for (i = 1; i <= max_chrom; i++) 
    ch_len[i] = chmk[i][chmk[i][0].index].loc;
/*  disp_known(); */
  cycle = run_cnt = longest_run = 0;
  for (i = 0; i <= max_chrom; i++)
  for (j = 0; j <= max_chrom; j++)
  for (k = 0; k <= MAX_RUN_LEN; k++) 
     stat_cnts[i][j][k] = stat_cnts1[i][j][k] = c1c2start[i][j][k] = 0;
  for (i = 0; i <= max_mark; i++) mark[i] = i;
  for (i = 0; i < 100; i++) seen_known[i] = seen_matched[i] = 0;
  
          /* Brandon's co-linear run approach */
  for (i = 1; i <= max_chrom; i++)           /* for each std chrom */
    {
    printf("\nChrom %d\n",i);
    for (j = 1; j <= max_chrom; j++)         /* for each test chrom */
      {
      if (two_sets && ((i <= max_set_1 && j <= max_set_1) ||
                       (i > max_set_1 && j > max_set_1))) continue;
      if (use_clust) find_clust_c1_c2(i,j,can_skip);
         else        find_runs_c1_c2(i,j,can_skip,can_tol);
      final_set(i,j,0);
      }
    printf("number of runs = %d\n",run_cnt);
    }
  if (verbose)  print_table();

      /* Brandon's statistical check */
  longest_run = 0;
  for (i = 0; i < 100; i++) seen_known[i] = seen_matched[i] = 0;
  printf("\n");
  for (cycle = 1; cycle <= max_cycles; cycle++)
    {
    if (cycle % 5 == 1) printf("Cycle %3d ",cycle);
    run_cnt = 0;

    for (i = 1; i <= max_chrom; i++)          /* for each std chrom, c1 */
      {
      cnt  = chmk[i][0].index;                /* how many markers on c1 */
      for (j = 1; j <= max_chrom; j++)        /* for each test chrom, c2 */
        {
        if (two_sets && ((i <= max_set_1 && j <= max_set_1) ||
                         (i > max_set_1 && j > max_set_1))) continue;
        cnt2 = chmk[j][0].index;              /* how many markers on c2 */
        if (rand_type == 1)                   /* randomize locations */
        for (k = 1; k <= max_mark; k++)
          mrk[k].loc = rand_int(0,(int)ch_len[mrk[k].chrom]);
        if (rand_type == 2)                   /* shuffle all locations on c2 */
        for (k = 1; k <= cnt2; k++)
          {
          loc = mrk[chmk[j][k].index].loc;
          pos = mrk[chmk[j][k].index].pos;
          m = rand_int(1,cnt2);
          mrk[chmk[j][k].index].loc = mrk[chmk[j][m].index].loc;
          mrk[chmk[j][m].index].loc = loc;
          mrk[chmk[j][k].index].pos = mrk[chmk[j][m].index].pos;
          mrk[chmk[j][m].index].pos = pos;
          }
        /* shuffle only those markers on c2 that also occur on c1 */
        if (rand_type == 3)    
        for (k = 1; k <= cnt; k++)
          {
          n = get_count(i,k,j);         /* how many times does k occur on c2 */
          for (h = 1; h <= n; h++)      /* for each occurrence on c2 */
            {                           
            index = get_index(i,k,j,n); /* get pointer to it */    
            loc = mrk[index].loc;       
            m = rand_int(1,cnt);
            while (get_count(i,m,j) == 0) m = rand_int(1,cnt);
            m = get_index(i,m,j,1);
            mrk[index].loc = mrk[m].loc;
            mrk[m].loc = loc;
            }
          }
        if (use_clust) find_clust_c1_c2(i,j,can_skip);
           else        find_runs_c1_c2(i,j,can_skip,can_tol);
        final_set(i,j,cycle);
        }
      }
    sig_cnt = comp_sig_cnt(cycle,1.0);
    sig_frc = (float)sig_cnt/(float)all_runs[0].len;
    printf("%3d-%4.3f ",run_cnt,sig_frc);
/*    printf("%3d-%3d ",run_cnt,sig_cnt); */
    if (cycle%5 == 0) printf("\n");
    }
  if (verbose)  print_table();
  for (k = 2; k <= longest_run; k++)            /* for each length */
    {
    printf("\nLength = %d\n    ",k);
    for (i = 1; i <= max_chrom; i++) printf("%4d ",i);
    printf("\n");
    for (i = 1; i <= max_chrom; i++)
      {
      printf("%2d: ",i);
/*      for (j = 1; j <= max_chrom; j++)  printf("%4d ",stat_cnts[i][j][k]); */
      for (j = 1; j <= max_chrom; j++) 
        printf("%4d ",(int)(100 * (float)stat_cnts1[i][j][k]/max_cycles));
      printf("\n");
      }
    }

  fp_out = fopen("pierre_ps","w");
  for (i = 1; i <= all_runs[0].len; i++)
    {
    rp = &all_runs[i];
    rp->prob = comp_prob(max_cycles,rp);
    fprintf(fp_out,"%7.5f\n",rp->prob/100.0);
    }
  fclose(fp_out);

  while (verbose)
    {
    fp = tp = fn = tn = poscnt = backcnt = totalcnt = 0;
    for (i = 0; i <= 1000; i++) prob_cnt[i] = 0;
    for (i = 1; i <= all_runs[0].len; i++) 
      prob_cnt[(int)(10*all_runs[i].prob)]++;
    printf("probability distribution: 0 to 10\n"); 
    for (i = 0; i <= 100; i++)
      {
      if (i%10 == 0) printf("\n %2d: ",i/10);
      printf("%4d ",prob_cnt[i]);
      }
    printf("\n");
    for (i = 0; i <= 1000; i++) prob_cnt[i] = 0;
    for (i = 1; i <= all_runs[0].len; i++) 
      prob_cnt[(int)(all_runs[i].prob)]++;
    printf("probability distribution: 0 to 100\n"); 
    for (i = 0; i < 100; i++)
      {
      if (i%10 == 0) printf("\n %2d: ",i/10);
      printf("%4d ",prob_cnt[i]);
      totalcnt += prob_cnt[i];
      if (i >= 90) backcnt += prob_cnt[i];
      }
    printf("\n");
    backcnt = backcnt/10.0;
    printf("backcnt= %5.2f  total = %d\n",backcnt,(int)totalcnt);
if (0)
    for (i = 0; i < 100; i++)
      {
      poscnt += prob_cnt[i];
      fp = backcnt*(1+i);
      tp = poscnt-fp;
      fn = totalcnt-(backcnt*100) - tp;      
      tn = (99-i)*backcnt;
      fn = totalcnt-poscnt-tn;
      printf("ROC %3d: tot= %4d fp= %4d tn= %4d tp= %4d fn= %4d  ",
        i,(int)poscnt,(int)fp,(int)tn,(int)tp,(int)fn);
      printf("%6.3f %6.3f\n",fp/(fp+tn),tp/(tp+fn));
/*      if (i%10 == 0) printf("\n %2d: ",i/10);
      printf("%3d ",(int) (100.0 * tp/poscnt)); */
      }
    sig_cnt = 0;    
    prob_cutoff = ans_real("what percent cutoff? (<=5) < 0 to quit");
    if (prob_cutoff < 0) break;
    if (15 < MAX_RUN_LEN) j = 15; else j = MAX_RUN_LEN;
    for (i = 3; i <= j; i++) printf("%5d ",i);
    printf("\n");
    for (i = 3; i <= j; i++) printf("%5d ",rand_match_cnt[i]/max_cycles);
    printf("\n");
    len_cutoff = ans_int("what is maximum number of markers in a run?");
    for (i = 1; i <= all_runs[0].len; i++)
      {
      rp = &all_runs[i];
      fprob = rp->prob;
      if (fprob <= prob_cutoff) sig_cnt++;
      iprob = fprob + .5;
      if (verbose && fprob <= prob_cutoff && rp->len <= len_cutoff)
        {
        printf("%5d: c= %2d, %2d m= %2d %5.1f-%5.1f %5.1f-%5.1f p= %4.1f %5.1f=%5.1f ",
        i,rp->c1,rp->c2,rp->len,rp->c1m1, rp->c1m2, rp->c2m1, rp->c2m2, fprob,
        fabs(rp->c1m2 - rp->c1m1), fabs(rp->c2m1 - rp->c2m2));
        /*  (int) (100 * (float)rp->cnt1/(float)max_cycles) */
        if (rp->bgnum >= 0) printf("    *%d ", rp->bgnum);
        printf("\n");
        }
      }
    set_cov_array(); /* for ROC stuff */
    disp_sig_runs();
    }
    if (!verbose) disp_sig_runs();
    i = all_runs[0].len;
    printf("number of run = %d those with pb <= %4.2f = %d, frac = %5.3f\n",
          i,prob_cutoff,sig_cnt,(float)sig_cnt/(float)i);
    ave_sig_frc += sig_cnt/(float)i;
    ave_runs += i;
    printf("after data set %d,  ave_runs = %5.3f ave sig frac = %5.3f\n",
             rdata, ave_runs/(float)rdata, ave_sig_frc/(float)rdata);

  }

if (d_num > 1) calc_ave_sd(0.0,0.0,3);
}














