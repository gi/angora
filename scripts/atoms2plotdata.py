#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Reads atoms from stdin and writes the plotdata to stdout

import sys
import argparse
from math import ceil, isinf
from collections import defaultdict, Counter
from statistics import mean, stdev

parser = argparse.ArgumentParser(description='Reads atoms data from stdin and writes plotdata to stdout.',
                                 epilog='Works for 2 sequences only. Input or pipe the output to rdotplot.R later.')

group = parser.add_argument_group(title='Family filters', description='If multiple options are given, the largest cut value is used.')
group.add_argument('--absolute', '-a', metavar='𝑎', type=int,
                    help='ignore families larger than 𝑎')
group.add_argument('--percent', '-p', metavar='𝑝', type=float,
                    help='ignore families larger than 𝑝%% of the families ' +
                         '(e.g. 𝑝 = 97.5 ignores the 2.5%% largest families)')
group.add_argument('--deviation', '-d', action='store_true',
                    help='ignore families larger than the ceil(mean + stdev)')

parser.add_argument('--show-cut', '-s', action='store_true',
                    help='show only the cut value (for which larger families are ignored), don\'t print the plotdata output')

parser.add_argument('-x', metavar='chrname', type=str, required=False,
                    help='use this chromosome for the X axis')


args = parser.parse_args()


chromosomes = defaultdict(lambda: defaultdict(list))
families = Counter() # counts family sizes

for line in sys.stdin:
    if line.startswith("#"):
        continue
    
    cname, gene_number, family, strand, start, end = line.split()
    if strand == "+":
        chromosomes[cname][family].append( (start, end) )
    else:
        chromosomes[cname][family].append( (end, start) )
    families[family] += 1

cnames = [ i for i in chromosomes.keys() ][::-1] # invert, otherwise we'll probably switch the axis in plot data

if args.x and cnames[0] != args.x:
    cnames.remove(args.x)
    cnames.insert(0, args.x)

cut = 0
if args.absolute:
    cut = max(cut, args.absolute)
if args.percent:
    pos = ceil( args.percent / 100.0 * (len(families)-1) )
    cut = max(cut, sorted(families.values())[pos])
if args.deviation:
    m = mean(families.values())
    s = stdev(families.values())
    cut = max(cut, ceil(m + s))
if cut == 0: # not set by any option
    cut = float("inf")

#import pdb
#sys.stdin = open('/dev/tty')
#pdb.set_trace()

if args.show_cut:
    if isinf(cut):
        print("No cut")
    else:
        print(cut)
    exit(0)
    
print("\t".join(cnames))
for family,aligns in chromosomes[cnames[0]].items():

    if families[family] > cut:
        continue
    
    for al1 in aligns:
        for al2 in chromosomes[cnames[1]][family]:
            print("%s\t%s\n%s\t%s\nNA\tNA" % (al1[0], al2[0], al1[1], al2[1]))

exit(0)
