#!/usr/bin/env python3

from sys import argv, exit, stdout, stderr
from Bio import SeqIO, Entrez, SeqFeature as SF
from os import fdopen, unlink
import logging

LOG_FILENAME = 'info.log'
LINE_WIDTH = 69

logging.basicConfig(filename=LOG_FILENAME,filemode='w', level=logging.INFO,
        format= "%(levelname)s\t%(asctime)s\t++ %(message)s")

def extractExons(feature, seq):
    res = ''
    for sub in feature.sub_features:
        if sub.type == 'CDS':
            res+= seq[sub.location.start.position-1:sub.location.end.position]
    return res

def printProtSeqs(record, withGFF, out1, out2):

    printed_loci = set()

    for f in record.features:
        # XXX maybe check untranslated proteins for stop codons?
        seq = None
        if f.type != 'CDS':
            continue
        if 'translation' in f.qualifiers:
            protSeq = f.qualifiers['translation'][0]
            protSeq = protSeq.replace('\n', '').replace('\r', '').replace('\t', '').replace(' ', '')
        else:
            protSeq = f.extract(record.seq).translate()
       
        # sort out protein identifier
        protId = None
        gi = None
        geneID = None
        locus = 'locus_tag' in f.qualifiers and f.qualifiers['locus_tag'][0] or '-unknown locus -'
        if 'locus_tag' not in f.qualifiers: # 1st try
            logging.error('Unknown locus tag for feature %s' %f)
            if 'gene' in f.qualifiers:
                locus = f.qualifiers['gene'][0] # 2nd try
            else:
                locus = f.qualifiers['name'][0] # 3rd try

        locus = locus.replace(' ', '')
                
        if locus in printed_loci:
            logging.warning('Gene with locus %s already printed, skipping gene...')
            continue


        if 'protein_id' in f.qualifiers:
            protId = f.qualifiers['protein_id'][0]
        if 'db_xref' in f.qualifiers:
            gis = [x[3:] for x in f.qualifiers['db_xref'] if x.startswith('GI')]
            ged = [x[7:] for x in f.qualifiers['db_xref'] if
                    x.startswith('GeneID')]
            gi = gis and gis[0]
            geneID = ged and ged[0]
       
#        # also write product information 
#        if f.qualifiers.has_key('product') and len(f.qualifiers['product']) \
#                and not protId.startswith(f.qualifiers['product'][0]):
#            protId += '|%s' %f.qualifiers['product'][0]
        
        if withGFF: 
            # print header for fasta entry
            print('>%s|%s' %(locus,protId), file=out1)
            
            info = (gi and 'ID=%s;' %gi or '') + ('Name=%s;'%protId) + (geneID
                    and 'GeneID=%s;' %geneID or '') + ('ChromosomeID=%s;' %record.id)
            print('\t'.join((locus, record.annotations['source'], f.type,
                    str(f.location.start.position), str(f.location.end.position), '.',
                    f.strand == 1 and '+' or '-', '.', info)), file=out2)
        else: 
            header = '|'.join([_f for _f in (gi and 'gi|%s' %gi or 'gi|-unknown-',
                geneID and ('ge|%s' %geneID) or 'ge|-unknown-', protId and 'gb|%s' %protId, locus, 
                'strand|%s' %(f.strand == 1 and '+' or '-'), record.name and 'chromosome|%s' %record.name) if _f])
            print('>%s' %header, file=out1)

        # with GFF file or without, either case we'll print the protein sequence!
        for i in range(0, len(protSeq), LINE_WIDTH):
            print(protSeq[i:i+LINE_WIDTH], file=out1)

        printed_loci.add(locus)


if __name__ == '__main__':
    if len(argv) < 2: 
        print('\tusage: %s [ --with-gff ] <GBK FILE 1> .. <GBK FILE N>' %argv[0])
        exit(1)
  
    withGFF = argv[1] == '--with-gff'
    # remove option for further processing of arguments
    if withGFF:
        del argv[1]

    out1 = stdout
    out2 = stderr

    for f in argv[1:]:
        logging.info('Opening file %s' %f)
        seqRecords = SeqIO.parse(f, 'genbank')
        for record in seqRecords:
            # print it out
            logging.info('Scanning record %s' %record.description)
            printProtSeqs(record, withGFF, out1, out2)

    out1.flush()
    out1.close()
    out2.flush()
    out2.close()

