#!/usr/bin/env python3
# Reads a table in the format bellow with shared chromosome content and prints some of the latex lines for pgfplot
# Reads from stdin and writes to stdout

# Input example:
# 1	6	68.70
# 1	7	31.30
# 2	13	23.95
# 2	15	20.43
# 2	16	17.50
# 2	3	17.40
# 2	14	15.25
# 2	10	5.47
# 3	5	100.00
# 4	17	60.87
# 4	9	39.13
# 5	19	73.23
# 5	12	26.77
# 6	8	53.90
# 6	11	46.10
# 7	14	51.69
# 7	7	25.06
# 7	4	23.25
# 8	2	60.57
# 8	4	39.43
# 9	10	39.29
# 9	3	23.47
# 9	1	18.88
# 9	4	13.27
# 9	13	5.10
# 10	18	100.00
# 11	1	100.00

import sys
from collections import defaultdict

# Threshold for printing chromosome number (label)
LABEL_THRESHOLD = 5.0

xy = defaultdict(dict)
for line in sys.stdin.readlines():
    if line.startswith("#"):
        continue
    x, y, v = [ eval(item) for item in line.split() ]
    xy[x][y] = v

maxx = max(xy.keys())
maxy = max([ max(xy[x].keys()) for x in xy.keys() ])

for x in range(1,maxx+1):
    for y in range(1,maxy+1):
        if y not in xy[x]:
            xy[x][y] = 0

print("\\begin{axis}[ybar stacked,bar shift=??pt,symbolic x coords={%s}]" % ",".join( [ str(x) for x in range(1,maxx+1) ] ))
for y in range(1,maxy+1):
    print("\\addplot+[] coordinates\n{", end=" ")
    for x in range(1,maxx+1):
        label = "%d" % y if xy[x][y] >= LABEL_THRESHOLD else ""
        print("(%d,%.2f)[%s]" % (x, xy[x][y], label), end=" ")
    print("};")
print("\\end{axis}")    

#sys.stdin = open('/dev/tty')
#import pdb
#pdb.set_trace()

exit(0)
