#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Calculates the average and summed DCJ distance and similarity for
# all ACSs chosen for CARs by ANGES.

 
import sys
import argparse
from collections import defaultdict, OrderedDict as odict, Counter
from statistics import mean
from pprint import pprint



# the dcj dst/sim for a cluster is the average of dcj dst/sim of all occurrences
def compute_dcj(clusters):
    for c in clusters.values():
        values_sim = [occ["sim"] for occ in c["occs"].values()]
        c["sim"] = sum(values_sim) / len(values_sim)
        values_dst = [occ["dst"] for occ in c["occs"].values()]
        c["dst"] = sum(values_dst) / len(values_dst)

# key function for sorting by sim
def key_sim(c):
    return c["sim"]

# cmp function for sorting by dst
def cmp_dst(c1, c2):
    if c1["dst"] != c2["dst"]:
        return c1["dst"] - c2["dst"]
    else:
        return c1["sim"] - c2["sim"]

# key function for sorting by dst
def key_dst():
    'Convert the cmp= function into a key= function'
    class K(object):
        def __init__(self, obj, *args):
            self.obj = obj 
        def __lt__(self, other):
            return cmp_dst(self.obj, other.obj) < 0 
        def __gt__(self, other):
            return cmp_dst(self.obj, other.obj) > 0 
        def __eq__(self, other):
            return cmp_dst(self.obj, other.obj) == 0
        def __le__(self, other):
            return cmp_dst(self.obj, other.obj) <= 0
        def __ge__(self, other):
            return cmp_dst(self.obj, other.obj) >= 0
        def __ne__(self, other):
            return cmp_dst(self.obj, other.obj) != 0
    return K


EPILOG="""\
"""

parser = argparse.ArgumentParser(description='Calculates the average and summed DCJ distance and similarity for all ACSs chosen for CARs by ANGES.',
                                 epilog=EPILOG,
                                 formatter_class=argparse.RawDescriptionHelpFormatter)

ingroup = parser.add_argument_group(title="Input files", description="Files containing data we need.")

ingroup.add_argument('--acs-c1p', metavar='file', type=argparse.FileType('r'), required=True,
                     help='file containing the set of ACSs that satisfies the consecutive ones property (C1P) given by ANGES, weighted by DCJ similarity or not.')

ingroup.add_argument('--clustersmap', metavar='file', type=argparse.FileType('r'), required=True,
                     help='file containing the mapping of ACSs IDs (ANGES) to cluster IDs given by Gecko (each line has <acs id> <cluster id>).')

ingroup.add_argument('--clusters', metavar='file', type=argparse.FileType('r'), required=True,
                     help='file containing clusters found by Gecko-DCJ (exported as \"clusterData\").')

args = parser.parse_args()



# read clustersmap
cmap = dict()
for line in args.clustersmap:
    new, orig = (int(v) for v in line.split()) # should be 2
    cmap[new] = orig

    
# read ACSs and save them with the original IDs
acss = []
for line in args.acs_c1p:
    acss.append(cmap[int(line.split("|", maxsplit=1)[0])])


# read clusters
clusters = odict()
clusters_f = args.clusters
clusters_f.readline() # discard first line
line = clusters_f.readline()
while line:
    cid, pvalue, refseq = [ i.split("=")[1].strip() for i in line.split(":")[1].split(",") ]
    cid = int(cid)
    line = clusters_f.readline()
    occs = odict()
    line = clusters_f.readline()
    while not line.startswith("ChromosomeNr"):
        occ, rest = line.split(":")
        species, rest = rest.split(",", maxsplit=1)
        species = species.strip()
        chrname, rest = rest.split("[")
        chrname = chrname.replace("chromosome", "").strip()
        rest = "[" + rest
        pos, rest = rest.split(maxsplit=1)

        genes = set()
        seq = []
        for s in rest.split():
            gene = int(s.strip("<>"))
            strand = "-" if s.startswith("<") else "+"
            if (gene != 0): genes.add(gene)
            seq.append(gene if strand == "+" else -gene)
            
        occs[occ] = {"sp":species, "chr":chrname, "pos":pos, "seq":seq, "genes":genes}

        line = clusters_f.readline()

    # skip to where we find similiarity scores
    while not line.startswith("DCJ similarities"):
        line = clusters_f.readline()
    line = clusters_f.readline()

    while "DCJ similarity" in line:
         occ, rest = line.split(":")
         occ = occ.strip()
         rest = rest.split(", ")
         sim = float(rest[0].replace("DCJ similarity =", "").strip())
         dst = float(rest[5].replace("DCJs to sort =", "").strip())
         occs[occ]["sim"] = sim
         occs[occ]["dst"] = dst
         refmap = eval(rest[-1].replace("refseq positions =", "").strip()) # should be a list
         occs[occ]["refmap"] = refmap # map to genes in reference sequence (ignoring "0" families)
         line = clusters_f.readline()

    genes = set()
    while line.strip(): # while we dont find an empty line, we read genes in the cluster and discard their occurrences
        if line.startswith("\t"):
            pass # gene occ
        else: # gene in reference
            genes.add(int(line.split(":", maxsplit=1)[0]))
        line = clusters_f.readline()
    genes = sorted(genes)
    
    cluster = defaultdict(list)
    cluster.update({"cid":cid, "genes":genes, "pvalue":pvalue, "refseq":refseq, "occs":occs})
    clusters[cid] = cluster

    # start next cluster
    line = clusters_f.readline()
    
compute_dcj(clusters)

clusters_c1p = [ clusters[cid] for cid in acss ]
#clusters_c1p.sort(key=key_sim, reverse=True)
clusters_c1p.sort(key=key_dst())
clusters_filtered = []

# filter clusters that overlap too much
for c in clusters_c1p:
    good = True
    genes = set(c["genes"]) 
    for c2 in clusters_filtered:
        if len(genes.intersection(c2["genes"])) > len(genes)/2:
            good = False
    if good:
        clusters_filtered.append(c)

similarities = [ c["sim"] for c in clusters_c1p ]
distances = [ c["dst"] for c in clusters_c1p ]

print("DCJ similarity (avg): %s" % mean(similarities))
print("DCJ similarity (sum): %s" % sum(similarities))
print("DCJ distance (avg): %s" % mean(distances))
print("DCJ distance (sum): %s" % sum(distances))

similarities = [ c["sim"] for c in clusters_filtered ]
distances = [ c["dst"] for c in clusters_filtered ]

print("DCJ similarity (avg): %s" % mean(similarities))
print("DCJ similarity (sum): %s" % sum(similarities))
print("DCJ distance (avg): %s" % mean(distances))
print("DCJ distance (sum): %s" % sum(distances))


exit(0)
