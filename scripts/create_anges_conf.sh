#!/bin/bash
# Creates a configuration file for ANGES

# Exit if some error happens
set -e

if [ $# -ne 7 ]
then
  echo ""
  echo "Wrong number of parameters. They must be (in that order):"
  echo ""
  echo "<name of ancestor> <markers file> <species tree file> <ACSs file> <output dir> <use dcj similarity weights> <run branch-and-bound>"
  echo ""
  echo "Where <use dcj similarity weights> can be:"
  echo "  1 (use weights given in ACSs file), or"
  echo "  0 (ask ANGES to (re)compute them by linear interpolation)"
  echo ""
  echo "And <run branch-and-bound> can be:"
  echo "  1 (run branch-and-bound after heuristic in C1P step), or"
  echo "  0 (run only heuristic in C1P step)"
  echo ""
  exit 1
fi

ancestor="$1"
markers="$2"
tree="$3"
acss="$4"
outdir="$5"
use_dcj_sim="$6"
use_bab="$7"

if [ $use_dcj_sim -eq 1 ]
then
  compute_weights=0
else
  compute_weights=1
fi

echo """\
# Parameters File
# Project: $ancestor
#
## Input files
#
markers_file $markers # file name for markers
tree_file $tree # file name for species tree
#
## Output options
#
output_directory $outdir # directory name
output_ancestor $ancestor # ancestor name
#
## Markers options
#
markers_doubled 0 # 0 = original markers, 1 = doubled markers
markers_unique 0 # 0 = repeated markers allowed, 1 = ingroup unique, 2 = unique
markers_universal 0 # 0 = missing markers allowed, 1 = ingroup universal, 2 = universal
#
## ACS options
#
#acs_pairs  # name of file containing the species pairs to compare
acs_sa 0 # supported adjacencies: 0 = not computed, 1 = computed
acs_ra 0 # reliable adjacencies: 0 = not computed, 1 = computed
acs_sci 0 # strong common intervals: 0 = not computed, 1 = computed
acs_mci 0 # maximal common intervals: 0 = not computed, 1 = computed
acs_aci 0 # all common intervals: 0 = not computed, 1 = computed
acs_file $acss # ACS provided by user
acs_weight $compute_weights # weighting ACS: 0 = no (use provided in acs file), 1 = linear interpolation
acs_correction 0 # Correcting for missing markers: 0 = none, 1 = adding markers spanned by intervals, 2 = X, 3 = both
#
## C1P model
#
c1p_linear 1 # 1 f or working with linear chromosomes
c1p_circular 0 # 1 for working with a unique circular chromosomes
#
## Telomeres (model+optimization)
#
c1p_telomeres 0 # 0: no telomere, 1: added after optimization (greedy heuristic), 2: added after optimization (bab), 3: added during optimization (bab)
#
## C1P optimization options
#
c1p_heuristic 1 # Using a greedy heuristic
c1p_bab $use_bab # Using a branch-and-bound
c1p_spectral 0 # Using spectral seriation
#
## Spectral seriation options
#
c1p_spectral_alpha 1.0 # Spectral seriation alpha value
#
# END\
"""

exit 0
