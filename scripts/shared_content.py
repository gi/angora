#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Prints some info about annotated genes in one species covered by reconstructed ancestor.

 
import sys
import argparse
from bisect import bisect_left
from collections import defaultdict, OrderedDict as odict, Counter
from pprint import pprint

DEFAULT_THRESHOLD = 1.0 # in %

# given the sorted start/end positions for the ordered list of genes
# in chromosome, returns the gene(s) that are intersected by
# [start,end], start/end are basepairs and each index of chromosome
# has a gene
def covered_genes(chromosome, start, end):
    # each index in chromosome has a tuple (start, end, gene name)
    covered = []
    idx = bisect_left(chromosome, (start, start, ""))
    while idx < len(chromosome) and chromosome[idx][0] <= end:
        g = chromosome[idx]
        if overlap(start, end, g[0], g[1]):
            covered.append(g[2]) # name of gene
        idx += 1
    return covered

# given two ranges [start,end], return true if they overlap
def overlap(s1, e1, s2, e2):
    return min(e1, e2) - max(s1, s2) >= 0

# compute map of occurrences for clusters for sp1 -> sp2
def compute_map(clusters, sp1, sp2):
    cm = defaultdict(Counter)
    for c in clusters:
        for chr1 in [ occ1["chr"] for occ1 in c["occs"].values() if occ1["sp"] == sp1 ]:
            for chr2 in [ occ2["chr"] for occ2 in c["occs"].values() if occ2["sp"] == sp2 ]:
                cm[chr1][chr2] += 1
    return cm

# compute map of occurrences for clusters for sp1 -> sp2
def compute_map2(clusters, sp1, sp2):
    cm = defaultdict(Counter)
    for c in clusters:
        for chr1 in set([ occ1["chr"] for occ1 in c["occs"].values() if occ1["sp"] == sp1 ]):
            for chr2 in set([ occ2["chr"] for occ2 in c["occs"].values() if occ2["sp"] == sp2 ]):
                cm[chr1][chr2] += 1
    return cm

# prints a map
def print_map(m, threshold):
    out = []
    for i in sorted(m.keys()):
        above_th = 0
        for j in sorted(m[i].keys()):
            perc = 100*m[i][j]/sum(m[i].values())
            if perc >= threshold:
                above_th += m[i][j]
        for j in sorted(m[i].keys()):
            perc = 100*m[i][j]/sum(m[i].values())
            if perc >= threshold:
                perc_thresh = 100*m[i][j]/above_th
                out.append((i, -perc, j, m[i][j], perc_thresh))                
    out.sort()
    for o in out:
        print("%d %d %d %.2f%% %.2f%%" % (o[0],o[2],o[3],-o[1], o[4]))
            

EPILOG="""\

The annotated genes file must be given in the --gene-pos argument in
the following format (each line):

<gene name> <chromosome number> <start> <end>

"""


parser = argparse.ArgumentParser(description='Prints for some species info about annotated genes covered in grape by reconstructed ancestor and shared chromosome content.',
                                 epilog=EPILOG,
                                 formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('--species', metavar='name', type=str, required=True,
                    help='name of the species we want to check.')

parser.add_argument('--cog', metavar='file', type=argparse.FileType('r'), required=True,
                     help='file in COG format given as input to Gecko-DCJ, contains all chromosomes data.')

parser.add_argument('--acs-c1p', metavar='file', type=argparse.FileType('r'), required=True,
                    help='file containing the set of ACSs that satisfies the consecutive ones property (C1P) given by ANGES.')

parser.add_argument('--clustersmap', metavar='file', type=argparse.FileType('r'), required=True,
                    help='file containing the mapping of ACSs IDs (ANGES) to cluster IDs given by Gecko (each line has <acs id> <cluster id>).')

parser.add_argument('--clusters', metavar='file', type=argparse.FileType('r'), required=True,
                    help='file containing clusters found by Gecko-DCJ (exported as \"clusterData\").')

parser.add_argument('--gene-pos', metavar='file', type=argparse.FileType('r'), required=True,
                    help='file containing gene positions (each line has <gene name> <chromosome number> <start> <end>).')

parser.add_argument('--threshold', metavar='float', type=float, default=DEFAULT_THRESHOLD,
                    help='threshold value in [0,100] for considering coverage in table (bellow this is considered noise), default: %g.' % DEFAULT_THRESHOLD)

args = parser.parse_args()



# read cog/markers
chrs_cog = defaultdict(list) # chromosomes content only for the species we are interested (key is <chromosome number>)
species = chrname = comment = ""
chrmap = odict()
chrcount = Counter()
line = args.cog.readline()
while line:
    line = line.strip()
    if not line: # end of chromosome
        species = chrname = comment = ""
    elif not chrname: # first line of new chromosome
        species, chrname = line.split(",")
        species = species.strip()
        chrname = chrname.strip().replace("chromosome", "").strip()
        chrcount[species] += 1
        chrnum = chrcount[species]# len(chrmap) + 1
        chrmap[chrname] = chrnum
    elif not comment: # second line of new chromosome
        comment = line
    elif species == args.species: # gene line
        gene = line.split()
        family = int(gene[0])
        start,end = gene[4].split("..")
        gene_info = {"start":int(start), "end":int(end)-1, "family":family} # position defined in basepairs
        chrs_cog[chrnum].append(gene_info)

    line = args.cog.readline()


# read clustersmap
cmap = dict()
for line in args.clustersmap:
    new, orig = (int(v) for v in line.split()) # should be 2
    cmap[new] = orig

    
# read ACSs and save them with the original IDs
acss = []
for line in args.acs_c1p:
    acss.append(cmap[int(line.split("|", maxsplit=1)[0])])


# read clusters
clusters = odict()
clusters_f = args.clusters
clusters_f.readline() # discard first line
line = clusters_f.readline()
while line:
    cid, pvalue, refseq = [ i.split("=")[1].strip() for i in line.split(":")[1].split(",") ]
    cid = int(cid)
    line = clusters_f.readline()
    occs = odict()
    line = clusters_f.readline()
    while not line.startswith("ChromosomeNr"):
        occ, rest = line.split(":")
        species, rest = rest.split(",", maxsplit=1)
        species = species.strip()
        chrname, rest = rest.split("[")
        chrname = chrname.replace("chromosome", "").strip()
        rest = rest
        pos, rest = rest.split(maxsplit=1)
        idxstart, idxend = pos.strip("[]").split(",") # 1-based
        idxstart, idxend = int(idxstart)-1, int(idxend)-1 # convert to 0-based
        
        genes = set()
        seq = []
        for s in rest.split():
            gene = int(s.strip("<>"))
            strand = "-" if s.startswith("<") else "+"
            if (gene != 0): genes.add(gene)
            seq.append(gene if strand == "+" else -gene)

        chrnum = chrmap[chrname]
        occs[occ] = {"sp":species, "chr":chrnum, "idxs":idxstart, "idxe":idxend, "seq":seq, "genes":genes} # positions defined by index in chromosome gene sequence

        line = clusters_f.readline()

    # skip to where we find similiarity scores
    while not line.startswith("DCJ similarities"):
        line = clusters_f.readline()
    line = clusters_f.readline()

    while "DCJ similarity" in line:
         occ, rest = line.split(":")
         occ = occ.strip()
         rest = rest.split(", ")
         sim = float(rest[0].replace("DCJ similarity =", "").strip())
         dst = float(rest[5].replace("DCJs to sort =", "").strip())
         occs[occ]["sim"] = sim
         occs[occ]["dst"] = dst
         refmap = eval(rest[-1].replace("refseq positions =", "").strip()) # should be a list
         occs[occ]["refmap"] = refmap # map to genes in reference sequence (ignoring "0" families)
         line = clusters_f.readline()

    genes = set()
    while line.strip(): # while we dont find an empty line, we read genes in the cluster and discard their occurrences
        if line.startswith("\t"):
            pass # gene occ
        else: # gene in reference
            genes.add(int(line.split(":", maxsplit=1)[0]))
        line = clusters_f.readline()
    genes = sorted(genes)

    #for occid in list(occs.keys()):
    #    if occs[occid]["sp"] != args.species:
    #        del occs[occid]
    
    cluster = defaultdict(list)
    cluster.update({"cid":cid, "genes":genes, "pvalue":pvalue, "refseq":refseq, "occs":occs})
    clusters[cid] = cluster

    # start next cluster
    line = clusters_f.readline()

    
# filter clusters
clusters_c1p = [ clusters[cid] for cid in acss ]


# read gene positions
genes_chr = defaultdict(dict)
for line in args.gene_pos:
    name, chrnum, start, end = line.split()
    chrnum, start, end = int(chrnum), int(start), int(end)
    genes_chr[chrnum][name] = {"start":start, "end":end, "coveredby":list()} # positions defined in basepairs


# create a list of sorted triples (start, end, gene name) for each chromosome
chrs = odict()
for chrnum,glist in genes_chr.items():
    chromosome = [ (pos["start"],pos["end"],name) for name,pos in glist.items() ] # positions defined in basepairs
    chromosome.sort()
    for idx,gene in enumerate(chromosome):
        name = gene[2]
        glist[name]["idx"] = idx
    chrs[chrnum] = chromosome


allcovered = set()
for c in clusters_c1p:
    c["covered"] = set() # only in species we are interested
    for occ in c["occs"].values():
        if occ["sp"] != args.species:
            continue
        chrnum = occ["chr"]
        idxs = occ["idxs"] # idx start
        idxe = occ["idxe"]
        seq = chrs_cog[chrnum][idxs:idxe+1]
        # one way of doing, counting annotated genes overlaped by ours
        #covered = set() # names of genes covered
        #for g in seq:
        #    genes = covered_genes(chrs[chrnum], g["start"], g["end"])
        #    covered.update(genes)
        # another way of doing, counting annotated genes inside some selected cluster interval
        covered = covered_genes(chrs[chrnum], seq[0]["start"], seq[-1]["end"])
        occ["covered"] = covered
        c["covered"].update(covered)
        for g in covered:
            genes_chr[chrnum][g]["coveredby"].append((c, occ))
    allcovered.update(c["covered"])

print("%d out of %d genes crossed by clusters" % (len(allcovered), sum(len(genes) for genes in genes_chr.values())))
print("%d out of %d clusters crossing some gene" % (len([c for c in clusters_c1p if len(c["covered"]) > 0]), len(clusters_c1p)))
print()

print("==Clusters coffee -> grape==")
print("coffee, grape, %, %only considering entries above threshold")

clustersmap = compute_map(clusters_c1p, "coffee", "grape")
print_map(clustersmap, args.threshold)
            

# filter clusters that overlap too much (more than half of gene with another in the filtered set)
clusters_c1p_filtered = []
count = 1
for c in clusters_c1p:
    good = True
    genes = set(c["genes"])
    #print("%d/%d" %(count, len(clusters_c1p)))
    count += 1
    for c2 in clusters_c1p_filtered:
        if len(genes.intersection(c2["genes"])) > len(genes)/2:
            good = False
    if good:
        clusters_c1p_filtered.append(c)
print()
print("==Clusters coffee -> grape (filtering overlapping)==")
print("coffee, grape, %, %only considering entries above threshold")

clustersmap_filtered = compute_map(clusters_c1p_filtered, "coffee", "grape")
print_map(clustersmap_filtered, args.threshold)

#import pdb
#pdb.set_trace()

exit(0)

