#!/usr/bin/env python

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter as ADHF, \
        FileType
from sys import stdout, stderr, exit
from os.path import dirname, join, basename, relpath
from itertools import combinations
import logging
import csv
import re

import numpy as np


BREWER_COL = ['blues-%s-seq-3', 'greens-%s-seq-3', 'oranges-%s-seq-3', 'purples-%s-seq-3', \
        'reds-%s-seq-3', 'purd-%s-seq-3']

BREWER_COL_RANGE = range(3,11)

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)


def readAtoms(data, chromosomes_only=None):

    res = list()
    for line in csv.reader(data, delimiter='\t'):
        if line[0].startswith('#'):
            continue
        if chromosomes_only == None or line[0] in chromosomes_only:
            res.append((line[0], int(line[4]), int(line[5]), line[2]))
    return res


def getChromNamesAndSizes(atoms):
    res = dict()

    for chrx, startx, endx, _ in atoms:
        if not res.has_key(chrx):
            res[chrx] = 0
        res[chrx] = max(res[chrx], endx)

    return zip(*res.items())


def writeLinks(fam2atoms, links_out, circos_out, radius):

    fam2col = dict(zip(sorted(fam2atoms.keys(), key=lambda x: not x.isdigit() \
            and x or int(x)), np.linspace(0, 360, len(fam2atoms))))

    for fam, members in fam2atoms.items():
        for (chrx, startx, endx), (chry, starty, endy) in combinations(members,
                2):
            print >> links_out, '%s %s %s %s %s %s family=%s,color=hue%03d' %(
                    chrx, startx, endx, chry, starty, endy,
                    fam, fam2col[fam])

    print >> circos_out, '<links>\n<link>'
    print >> circos_out, 'file             = %s' %relpath(links_out.name,
            dirname(circos_out.name))
    print >> circos_out, 'radius          = %sr' %radius
    print >> circos_out, 'bezier_radius    = 0r'
    print >> circos_out, 'ribbon           = yes'
    print >> circos_out, 'flat             = yes'
    print >> circos_out, 'show             = yes'
    print >> circos_out, 'condition        = 1'
    print >> circos_out, 'color            = eval(var(color))'
    print >> circos_out, 'flow             = continue'
    print >> circos_out, '</link>\n</links>'


def writeKaryotype(chromsizes, karyotype_out, circos_out):
    for c, s in chromsizes:
        # chr - ID LABEL START END COLOR
        print >> karyotype_out, 'chr - %s %s 0 %s ideogram.chr%s' %(c, c, s, c)

    print >> circos_out, 'karyotype = %s' %relpath(karyotype_out.name,
            dirname(circos_out.name))
    print >> circos_out, 'chromosomes_units= 1000000'
    print >> circos_out, 'chromosomes_display_default = no'
    print >> circos_out, 'chromosomes      = %s' %(';'.join(map(lambda x:
        x[0], chromsizes)))


def writeHighlights(highlight_regions, highlight_out, circos_out, outer_radius,
        inner_radius, color_prefix, enumerateColors=True, z=1):

    # assumes coords are sorted
    print >> circos_out, '<highlight>'
    print >> circos_out, 'stroke_thickness = 0'
    print >> circos_out, 'show             = yes'
    print >> circos_out, 'z                = %s' %z
    print >> circos_out, 'file             = %s' %relpath(
            highlight_out.name, dirname(circos_out.name))
    print >> circos_out, 'r0               = %sr' %outer_radius
    print >> circos_out, 'r1               = %sr' %inner_radius
    print >> circos_out, '</highlight>'

    for i, (chrx, start, end) in enumerate(highlight_regions):
        print >> highlight_out, '%s %s %s fill_color=%s%s' %(chrx, start, end,
                color_prefix, enumerateColors and str(i) or '')


def writeColors(out, chrs):

    c_max = len(BREWER_COL_RANGE) * len(BREWER_COL)
    if len(chrs) > c_max:
        print >> stderr, '!! Too many chromosomes. Not enough colors ' + \
                'available to color all links! Exiting'
        exit(1)

    print >> out, '<colors>'
    for x, c in enumerate(chrs):
        r = x / len(BREWER_COL)
        i = x % len(BREWER_COL)
        print >> out, 'ideogram.chr%s = %s' %(x, BREWER_COL[i]
                %BREWER_COL_RANGE[r])
        print >> out, 'band.chr%s = vvlgrey' %x
    print >> out, '</colors>'


def writeGeneralConf(image_path, out):

    print >> out, '<ideogram>'
    print >> out, '<spacing>'
    print >> out, 'default          = 0.005r'
    print >> out, '</spacing>'
    print >> out, 'radius           = 0.90r'
    print >> out, 'thickness        = 0p'
    print >> out, 'stroke_thickness = 0'
    print >> out, 'show_bands       = yes'
    print >> out, 'fill_bands       = yes'
    print >> out, 'band_stroke_thickness = 0'
    print >> out, 'band_transparency= 1'
    print >> out, 'show_label       = yes'
    print >> out, 'label_font       = default'
    print >> out, 'label_radius     = 1.1r'
    print >> out, 'label_size       = 60'
    print >> out, 'label_parallel   = no'
    print >> out, '</ideogram>'
    print >> out, 'show_ticks       = yes'
    print >> out, 'show_tick_labels = yes'
    print >> out, '<ticks>'
    print >> out, 'radius           = 1r'
    print >> out, 'color            = black'
    print >> out, 'thickness        = 2p'
    print >> out, 'multiplier       = 1e-6'
    print >> out, 'label_separation = 10p'
    print >> out, 'format           = %dMb'
    print >> out, '<tick>'
    print >> out, 'spacing          = 0.1u'
    print >> out, 'size             = 5p'
    print >> out, '</tick>'
    print >> out, '<tick>'
    print >> out, 'spacing          = 1u'
    print >> out, 'size             = 10p'
    print >> out, '</tick>'
    print >> out, '<tick>'
    print >> out, 'spacing          = 5u'
    print >> out, 'size             = 15p'
    print >> out, 'show_label       = yes'
    print >> out, 'label_size       = 30p'
    print >> out, 'label_offset     = 10p'
    print >> out, '</tick>'
    print >> out, '</ticks>'
    print >> out, '<image>\nfile  = %s' %image_path
    print >> out, 'dir   = .\npng   = yes\nradius         = 1500p'
    print >> out, 'angle_offset      = -90'
    print >> out, 'auto_alpha_colors = yes\nauto_alpha_steps  = 5'
    print >> out, 'background = white\n</image>'
    print >> out, '<<include etc/colors_fonts_patterns.conf>>'
    print >> out, '<<include etc/colors.brewer.conf>>'
    print >> out, '<<include %s>>' %relpath('etc/housekeeping.conf',
            dirname(out.name))
    print >> out, 'data_out_of_range* = warning'


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ADHF)
    parser.add_argument('atoms_file', type=FileType('r'),
            help='Output file of Atomizer')
    parser.add_argument('-o', '--out_dir', type=str, default='.',
            help = 'output directory')
    parser.add_argument('-c', '--chromosomes_only', type=str, nargs='+',
            help = 'only display specified chromosomes')

    args = parser.parse_args()

    #
    # setup logging
    #
    ch = logging.StreamHandler(stderr)
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
    LOG.addHandler(ch)

    atoms = readAtoms(args.atoms_file, chromosomes_only=args.chromosomes_only)
    chromnames, chromsizes = getChromNamesAndSizes(atoms)

    fam2atoms = dict()
    for chrx, start, end, family in atoms:
        if not fam2atoms.has_key(family):
            fam2atoms[family] = list()
        fam2atoms[family].append((chrx, start, end))

    #
    # write configuration
    #
    file_prefix = join(args.out_dir, 'circos')
#    file_prefix = join(args.out_dir, basename( \
#            args.atoms_file.name).rsplit('.atoms', 1)[0])

    # open file handles for circos configuraiton
    circos_out = open('%s.conf' %file_prefix, 'w')

    # write karyotypes
    karyotype_out = open(join(args.out_dir, 'karyotype.txt'), 'w')
    writeKaryotype(zip(chromnames, chromsizes), karyotype_out, circos_out)
    writeGeneralConf('%s.png' %file_prefix, circos_out)

    outer_band_r = 0.935
    inner_radius = outer_band_r

    # write links
    links_out = open(join(args.out_dir, 'links.txt'), 'w')
    writeLinks(fam2atoms, links_out, circos_out, inner_radius)
    writeColors(circos_out, chromnames)

    print >> circos_out, '<highlights>'
    highlight_out = open(join(args.out_dir, 'chromosomes.txt'), 'w')
    writeHighlights(map(lambda x: (x[0], 0, x[1]), zip(chromnames,
        chromsizes)), highlight_out, circos_out, 1.0, outer_band_r, 'band.chr',
        1)
    highlight_out.close()
    print >> circos_out, '</highlights>'

