#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Reads gene families (in ANGES markers format) from stdin and writes gecko .cog file to stdout

# If multiple files need to be given as stdin, there are some options:
#  ./script <(cat file1 file2)
#  cat file1 file2 | ./script

# data in .cog files is organized as follows:
# <Genome Data>
# Empty Line
# <GenomeData>
# Empty Line
# With <Genome Data> being:
#
# GenomeName <COMMA> Descriptive Text  <NEWLINE>
# Descriptive Text (ignored) <NEWLINE>
# <Genome Content> <NEWLINE>
#
# And each line <Genome Content> is composed by the following fields:
# <Homology> <TAB> Strand (+ or -) <TAB> functional category <TAB> Gene Name <TAB> functional annotation <NEWLINE>
#

import sys
import argparse
import re
from math import ceil, isinf
from collections import defaultdict, Counter
from statistics import mean, stdev


# Parses one gene line
def parse_gene_line(line):
    gname, line = line.split(".")
    cname, line = line.split(":")
    start, line = line.split("-", maxsplit=1)
    end, strand = line.split()
    return gname, cname, int(start), int(end), strand

# key function for sorting genes in a chromosome
def key_gene(g):
    return g[4] # start position

# text to int, if possible
def atoi(text):
    return int(text) if text.isdigit() else text

# key function for sorting strings in natural order, from:
# http://nedbatchelder.com/blog/200712/human_sorting.html
def key_natural(text):
    return tuple( atoi(c) for c in re.split(r'(\d+)', text) )

        
EPILOG="""\
The family attribute is replaced by 0 (no family) for any gene whose
family has only the gene itself.
"""

parser = argparse.ArgumentParser(description='Reads gene families (in ANGES markers format) from stdin and writes gecko .cog file to stdout.',
                                 epilog=EPILOG)
parser.add_argument('--note', metavar='text', type=str,
                    help='some text to be written in description of all chromosomes ' +
                    '(i.e. parameters used for computation)')

filters = parser.add_argument_group(title='Family filters', description='If multiple options are given, the largest cut value is used.')
filters.add_argument('--absolute', '-a', metavar='𝑎', type=int,
                    help='ignore families larger than 𝑎')
filters.add_argument('--percent', '-p', metavar='𝑝', type=float,
                    help='ignore families larger than 𝑝%% of the families ' +
                         '(e.g. 𝑝 = 97.5 ignores the 2.5%% largest families)')
filters.add_argument('--deviation', '-d', action='store_true',
                    help='ignore families larger than the ceil(mean + stdev)')

filters.add_argument('--show-cut', '-s', action='store_true',
                    help='show only the cut value (for which larger families are ignored), don\'t print the .cog output')

filters0 = parser.add_mutually_exclusive_group()
filters0.add_argument('--collapse0', '-c', action='store_true',
                    help='collapse multiple sequential occurrences of 0 (unknown family) into one')
filters0.add_argument('--ignore0', '-i', action='store_true',
                    help='ignore occurrences of 0 (unknown family)')

args = parser.parse_args()


genomes = defaultdict(lambda: defaultdict(list))
families = Counter() # counts family sizes
if args.note is None:
    note = ''
else:
    note = ', ' + args.note

gene_number = 1
family = None
for line in sys.stdin:
    line = line.strip()
    if line.startswith("#"):
        pass
    elif not line: # end of some family
        pass
    elif line.startswith(">"): # beginning of some family
        family = line.split()[0].strip(">")
    else: # some gene in family
        gname, cname, start, end, strand = parse_gene_line(line)
        genomes[gname][cname].append( [family, strand, "?", cname + ":" + str(gene_number), start, end] )
        families[family] += 1
        gene_number += 1
        

# sort chromosomes
for genome in genomes.values():
    for chromosome in genome.values():
        chromosome.sort(key=key_gene)
        
# save original sizes
orig_sizes = dict()
for genome in genomes.values():
    for cname, chromosome in genome.items():
        orig_sizes[cname] = len(chromosome)

        
# replaces families ocurring one time by 0
for gname, genome in list(genomes.items()):
    for cname, chromosome in list(genome.items()):
        for gene in chromosome:
            if families[gene[0]] == 1:
                del families[gene[0]]
                gene[0] = '0'

# do statistics
cut = 0
if args.absolute:
    cut = max(cut, args.absolute)
if args.percent:
    pos = ceil( args.percent / 100.0 * (len(families)-1) )
    cut = max(cut, sorted(families.values())[pos])
if args.deviation:
    m = mean(families.values())
    s = stdev(families.values())
    cut = max(cut, ceil(m + s))
if cut == 0: # not set by any option
    cut = float("inf")
    
#import pdb
#sys.stdin = open('/dev/tty')
#pdb.set_trace()
    
if args.show_cut:
    if isinf(cut):
        print("No cut")
    else:
        print(cut)
    exit(0)
     

# remove families above cut
for gname, genome in list(genomes.items()):
    for cname, chromosome in list(genome.items()):
        chr_new = []
        for gene in chromosome:
            if families[gene[0]] > cut:
                continue
            chr_new.append(gene)
        genome[cname] = chr_new

# collapse families 0
if args.collapse0: # [family, strand, "?", cname + ":" + gene_number, start + ".." + end]
    for gname, genome in list(genomes.items()):
        for cname, chromosome in list(genome.items()):
            last0 = None
            chr_new = []
            for gene in chromosome:
                if gene[0] != '0':
                    chr_new.append(gene)
                    last0 = None
                elif not last0:
                    last0 = gene
                    chr_new.append(gene)
                else:
                    last0[5] = gene[5]
            genome[cname] = chr_new
# or ignore them
elif args.ignore0:
    for gname, genome in list(genomes.items()):
        for cname, chromosome in list(genome.items()):
            chr_new = []
            for gene in chromosome:
                if gene[0] == '0':
                    continue
                chr_new.append(gene)
            genome[cname] = chr_new

# write cog
for gname in sorted(genomes.keys(), key=key_natural):
    genome = genomes[gname]
    for cname in sorted(genome.keys(), key=key_natural):
        chromosome = genome[cname]
        print("%s, chromosome %s" % (gname, cname))
        print("%d genes (%d before cuts)%s" % (len(chromosome), orig_sizes[cname], note))
        for gene in chromosome:
            print("%s\t%s\t%s\t%s\t%d..%d" % tuple(gene))
        print()
    
exit(0)
