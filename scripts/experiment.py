#!/usr/bin/env python3

from itertools import combinations, product, chain
from os.path import join
import configparser as cp
import re

from Bio import SeqIO
import numpy as np

DATASET_CONFIG_FILE                 = 'datset.cfg'
WINDOW_SIZE                         = 1000
N_THRESHOLD                         = 0.9
MAXBREAKS                           = 5


class Manager(object):

    def __init__(self,
            config_file             = DATASET_CONFIG_FILE):

        #
        # read config
        #

        self.config = ConfigParser()
        self.config.readfp(open(config_file))
        self.genomes = list()
        self.chromosomes = list()
        self.chromlengths = list()
        self.breakpoints = list()
        for s in self.config.sections():
            self.genomes.append(s)
            gbk_file = self.config.get(s, 'gbk', fallback=None)
            if self.config.has_option(s, 'chromosomes') and \
                    self.config.has_option(s, 'breakpoints') and \
                    self.config.has_option(s, 'chromlengths'):
                chrs = map(lambda x: x.strip(), self.config.get(s,
                    'chromosomes').split(';'))
                breakpoints = list(map(lambda x: list(map(int, x.split(','))),
                    self.config.get(s, 'breakpoints').split(';')))
                chrlengths = map(lambda x: int(x), self.config.get(s,
                    'chromlengths').split(';'))
            elif gbk_file:
                chrs = list()
                breakpoints = list()
                chrlengths = list()
                for rec in SeqIO.parse(open(gbk_file), 'gb'):
                    chrs.append(rec.name)
                    chrlengths.append(len(rec))
                    breakpoints.append(getBreakpointsCandidates(rec,
                        WINDOW_SIZE, N_THRESHOLD))
                self.config.set(s, 'chromosomes', ';'.join(chrs))
                self.config.set(s, 'chromlengths', ';'.join(map(str, chrlengths)))
                self.config.set(s, 'breakpoints', ';'.join(map(lambda x:
                    ','.join(map(str, x)), breakpoints)))
                conf_f = open(config_file, 'w')
                self.config.write(conf_f)
                conf_f.close()
            else:
                chrs = list()
                breakpoints = list()
                chrlengths = list()

            self.chromosomes.append(tuple(chrs))
            self.chromlengths.append(tuple(chrlengths))
            breakpoints = map(reduceBreakpoints, breakpoints)
            self.breakpoints.append(tuple(map(tuple, breakpoints)))

        self.chromosomes = tuple(self.chromosomes)
        self.genome2id = dict(zip(self.genomes, range(len(self.genomes))))
        #self.chromname2len = dict(zip(self.chromosomes, self.chromlengths))
        self.chromname2len = dict(zip(chain.from_iterable(self.chromosomes),
                                      chain.from_iterable(self.chromlengths)))


    def getChromosomes(self, genome):
        gid = self.genome2id.get(genome, -1)
        if gid >  -1:
            return self.chromosomes[gid]
        return ()

    def getChromosomeLength(self, chrname):
        return self.chromname2len.get(chrname, 0)

    def chromosomeFiles(self):
        for i in range(len(self.genomes)):
            for j in range(len(self.chromosomes[i])):
                yield join(self.genomes[i], self.chromosomes[i][j])


    def chromosomeSegmentFiles(self):
        for i in range(len(self.genomes)):
            for j in range(len(self.chromosomes[i])):
                for k in range(len(self.breakpoints) - 1):
                    yield join(self.genomes[i], self.chromosomes[i][j],
                            '-'.join((str(self.breakpoints[i][j][k]+1),
                                str(self.breakpoints[i][j][k+1]))))


    def pwAlignmentFiles(self, genome_pair = None, chr_pair = None):
        assert not chr_pair or genome_pair

        res = list()
        genome_comb = combinations(range(len(self.genomes)), 2)
        if genome_pair:
            genome_comb = (tuple(sorted((self.genome2id[genome_pair[0]],
                self.genome2id[genome_pair[1]]))), )
        for i1, i2 in genome_comb:
            chr_comb = product(range(len(self.chromosomes[i1])),
                    range(len(self.chromosomes[i2])))
            if chr_pair:
                chr_comb = ((self.chromosomes[i1].index(chr_pair[0]),
                        self.chromosomes[i2].index(chr_pair[1])), )
            for j1, j2 in chr_comb:
                for k1, k2 in product(range(len(self.breakpoints[i1][j1])-1),
                        range(len(self.breakpoints[i2][j2])-1)):
                    r1 = '-'.join((str(self.breakpoints[i1][j1][k1]+1),
                                str(self.breakpoints[i1][j1][k1+1])))
                    r2 = '-'.join((str(self.breakpoints[i2][j2][k2]+1),
                                str(self.breakpoints[i2][j2][k2+1])))
                    res.append(join(':'.join((self.genomes[i1],
                        self.genomes[i2])), ':'.join((self.chromosomes[i1][j1],
                            self.chromosomes[i2][j2])), ':'.join((r1, r2))))
        return res

    def genomeChromsomePairs(self):
        res = list()
        for i1, i2 in combinations(range(len(self.genomes)), 2):
            for j1, j2 in product(range(len(self.chromosomes[i1])),
                    range(len(self.chromosomes[i2]))):
                res.append(join(':'.join((self.genomes[i1],
                    self.genomes[i2])), ':'.join((self.chromosomes[i1][j1],
                        self.chromosomes[i2][j2]))))
        return res

    # s should be species1:species2/chromosome_sp1:chromosome_sp2
    def getChromosomePairPlotTitle(self, s):
        s = s.replace("/",":").split(":")
        return "%s %s vs %s %s" % (s[0], s[2], s[1], s[3])

    # s should be species1:species2/chromosome_sp1:chromosome_sp2
    def getChromosomePairLengths(self, s):
        s = s.replace("/",":").split(":")
        len1 = self.getChromosomeLength(s[2])
        len2 = self.getChromosomeLength(s[3])
        return "%s %s" % (len1, len2)

    def getGenomesChromosomesList(self):
        return " ".join([ '"' + g + ':' + ','.join(self.getChromosomes(g)) + \
                          '"' for g in self.genomes ])
        #import pdb
        #pdb.set_trace()

    def get(self, section, variable, fallback=None):
        return self.config.get(section, variable, fallback=fallback)


class ConfigParser(cp.ConfigParser):
    """Can get options() without defaults
    """
    def options(self, section, no_defaults=False, **kwargs):
        if no_defaults:
            try:
                return list(self._sections[section].keys())
            except KeyError:
                raise NoSectionError(section)
        else:
            return super().options(section, **kwargs)


    def has_option(self, section, option, no_defaults=False, **kwargs):
        """Check for the existence of a given option in a given section."""
        if no_defaults:
            if not section or section == cp.DEFAULTSECT or section not in \
                    self._sections:
                return False
            else:
                option = self.optionxform(option)
                return option in self._sections[section]
        else:
            return super().has_option(section, option, **kwargs)


def getBreakpointsCandidates(rec, window_size, threshold):

    res = [0]
    W = float(window_size)

    buf = list()
    c = sum(1 for x in rec.seq[:window_size] if x.upper() == 'N')
    for i in range(window_size, len(rec.seq)):
        v = c/W
        if v < threshold:
            if buf:
                bp = i-len(buf) + max(zip(buf, range(len(buf))))[1] - \
                        window_size/2
                res.append(int(bp))
                buf = list()
        else:
            buf.append(v)
        if rec.seq[i-window_size].upper() == 'N':
            c -= 1
        if rec.seq[i].upper() == 'N':
            c += 1
    res.append(len(rec.seq))
    return res


def reduceBreakpoints(breakpoints, maxbps=MAXBREAKS):
    b = len(breakpoints)
    if len(breakpoints) > maxbps:
        res = [breakpoints[i] for i in np.linspace(0, b-1, maxbps, dtype=int)]
    else:
        res = breakpoints
    return res

if __name__ == '__main__':
    print('THIS IS A MODULE')

