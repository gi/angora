#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Reads files given by CloseUp.pl and bastp families post-processing
# and writes clusters (ACSs) input file for ANGES.

import sys
import argparse
import pprint as pp
from collections import OrderedDict as odict

# used mostly when debugging
pprint = pp.PrettyPrinter(indent=4).pprint


def parse_overview_line(chr_map, line):
    # fields are: c1 c2 #markers ss c1s c1e c2s c2e sig id
    fields = line.split()
    cluster_id = fields[9]
    headers = ["c1", "c2", "#markers", "ss", "c1s", "c1e", "c2s", "c2e", "sig"]
    data = dict()
    for idx,h in enumerate(headers):
        data[h] = fields[idx]
    data["sp1"] = chr_map[data["c1"]]["sp"]
    data["c1o"] = chr_map[data["c1"]]["chr"] # original name of chr1
    data["sp2"] = chr_map[data["c2"]]["sp"]
    data["c2o"] = chr_map[data["c2"]]["chr"] # original name of chr2
    data["markers"] = []
    data["markers_c1pos"] = []
    data["markers_c2pos"] = []
    return cluster_id, data


def parse_detail_file(f, clusters):
    line = f.readline()
    if not line.startswith("=========="):
        raise Exception('Wrong detail file format, the file should start with "==========" (got %s)' % line)
    while line:
        # we should be in some line starting with ==========, e.g. ========== id: 28 BETWEEN Chr 8 and Chr 1
        fields = line.split()
        cluster_id = fields[2]
        #c1 = fields[5]
        #c2 = fields[8]
        line = f.readline()
        while line and not line.startswith("=========="):
            marker, c1pos, c2pos = line.split()
            clusters[cluster_id]["markers"].append(marker)
            clusters[cluster_id]["markers_c1pos"].append(c1pos)
            clusters[cluster_id]["markers_c2pos"].append(c2pos)
            line = f.readline()
    return


EPILOG="""\
Each line in the chromosome map file must be in the following format:

chromosome_number species_name chromosome_name

where chromosome_number is the chromosome number in CloseUp output,
and species_name and chromosome_name are the original names. The values
cannot contain spaces.

The input files must not contain empty lines.
"""

parser = argparse.ArgumentParser(description='Reads files given by CloseUp.pl and bastp families post-processing ' +
                                             'and writes clusters (ACSs) input file for ANGES.',
                                 epilog=EPILOG,
                                 formatter_class=argparse.RawDescriptionHelpFormatter)

ingroup = parser.add_argument_group(title="Input files", description="Files given by CloseUp computation.")

ingroup.add_argument('--overview', metavar='file', type=argparse.FileType('r'), required=True,
                     help='overview file, which contains clusters summary table.')

ingroup.add_argument('--detail', metavar='file', type=argparse.FileType('r'), required=True,
                     help='detail file, which contains clusters markers and positions.')

ingroup.add_argument('--closeup-chrmap', metavar='file', type=argparse.FileType('r'), required=True,
                     help='chromosome map file, which describes which chromosome number in ' +
                          'CloseUp output corresponds to each original chromosome (because ' +
                          'CloseUp uses unique integers as chromosome names).')

ingroup.add_argument('--markers-in', metavar='file', type=argparse.FileType('r'), required=True,
                     help='input file describing all marker/families and their occurrences ' +
                     'given by postprocess_blastpfamilies.py, contains chromosomes in the ' +
                     'format <species name>.<chromosome name>).')


outgroup = parser.add_argument_group(title="Output files", description="Files that will be generated for ANGES.")

outgroup.add_argument('--acs', metavar='file', type=argparse.FileType('w'), required=True,
                     help='describes all weighted ACSs (clusters) and species they occur.')

outgroup.add_argument('--chrmap', metavar='file', type=argparse.FileType('w'), required=True,
                      help='file to write chromosome map, mapping the chromosome name to the format ' +
                           'used in ANGES (<species name>.<chromosome number>).')

outgroup.add_argument('--markers', metavar='file', type=argparse.FileType('w'), required=True,
                      help='file to write chromosome map, mapping the chromosome name to the format ' +
                           'used in ANGES (<species name>.<chromosome number>).')


args = parser.parse_args()

cl_chrmap = dict()
clusters = dict()
chrmap = odict() # new chrmap

# read closeup chrmap
for line in args.closeup_chrmap:
    fields = line.split()
    if len(fields) == 0:
        continue
    chrnum, species, chrname = fields
    cl_chrmap[chrnum] = { "sp":species, "chr":chrname }
    
    if species not in chrmap:
        chrmap[species] = odict()
    chrnum = len(chrmap[species]) + 1
    chrmap[species][chrname] = chrnum

    
# read overview
args.overview.readline() # skip header
for line in args.overview:
    cluster_id, data = parse_overview_line(cl_chrmap, line)
    clusters[cluster_id] = data


# read detail
parse_detail_file(args.detail, clusters)


# read markers
line = args.markers_in.readline()
markers = odict() # stores occurrences of markers
marker = ""
while line:
    if not line.strip(): # empty line
        pass
    elif line.startswith(">"): # new marker
        marker = int(line.split(maxsplit=1)[0].replace(">",""))
        markers[marker] = []
    else: # occurrence of marker
        species,rest = line.split(".", maxsplit=1)
        chrname,data = rest.split(":", maxsplit=1)
        markers[marker].append({"species":species, "chrname":chrname, "data":data.rstrip()})
    line = args.markers_in.readline()


# write new chrmap
chrmap_f = args.chrmap
for s, clist in chrmap.items():
    chrmap_f.write("%s\n" % s)
    for cname, cnum in clist.items():
        chrmap_f.write("%s: %s.%s\n" % (cname, s, cnum))
    chrmap_f.write("\n")


# write ACSs
for cluster_id, data in clusters.items():
    args.acs.write("%s|%s;%s,%s:%s\n"
                        % (cluster_id,
                           data["sig"],
                           data["sp1"], # falta usar o chrmap p converter nome do cromossomo p int
                           data["sp2"],
                           " ".join(sorted(set(data["markers"])))))


# write new markers file
markers_f = args.markers
for marker,occs in markers.items():
    markers_f.write(">%d\n" % marker)
    for occ in occs:
        markers_f.write("%s.%d:%s\n" % (occ["species"], chrmap[occ["species"]][occ["chrname"]], occ["data"]))
    markers_f.write("\n")


exit(0)
