#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Takes Gecko-DCJ input and output files and generates files needed by ANGES.
# We filter genes in unknown families (family = 0) and families with just one
# gene (in both markers and clusters files)

# TODO: refactor using classes (maybe, in the far future)
 
import sys
import argparse
from os.path import basename
from collections import defaultdict, OrderedDict as odict, Counter
from itertools import combinations
from networkx import Graph
from networkx.algorithms.components import connected_components
from itertools import chain
import re
import copy
from pprint import pprint
from datetime import datetime



SCORE_NORM_NEGATIVE = 0.001

# given a cluster, percentage of occurrences an adjacency must happen
# to be written as an ACS
THRESHOLD_ADJ = 0.8 #0.8
THRESHOLD_DELTA = 0.01 # for >= float comparisons
THRESHOLD_ADJ -= THRESHOLD_DELTA


# preety printer (debugging)
def pr(data):
    pprint(data, width=200, compact=True)
    

# the dcj sim score for a cluster is the average of dcj sim of all occurrences
def compute_dcj_sim(clusters):
    for c in clusters.values():
        values = [occ["sim"] for occ in c["occs"].values()]
        c["sim"] = sum(values) / len(values)


# we divide the similarity score by the best possible without discarding genes,
# which is exactly the number of genes in families != 0, negative scores are
# set to 0.001
def normalize_dcj_sim(clusters):
    for c in clusters.values():
        best_sim = len([ g for g in c["occs"][c["refseq"]]["seq"] if g not in ("+0", "-0") ])
        c["sim"] = (c["sim"] / best_sim) if c["sim"] > 0 else SCORE_NORM_NEGATIVE


# returns start and end positions (from a string in format "[start, end]")
def get_start_end(pos):
    start, end = pos.strip("[]").split(",")
    return int(start), int(end)


# returns true if clusters c1 and c2 have some occurrence that overlap
def overlap(c1, c2):
    occs1 = c1["occs"]
    occs2 = c2["occs"]
    for occ1 in occs1.values():
        s1, e1 = get_start_end(occ1["pos"])
        for occ2 in occs2.values():
            s2, e2 = get_start_end(occ2["pos"])
            if occ1["sp"] == occ2["sp"] and \
               occ1["chr"] == occ2["chr"] and \
               min(e1, e2) - max(s1, s2) >= 0:
                return True
    return False


# creates new families for genes in the set of genes in occurrences of some cluster,
# returns the new maximum family number and a map mapping new families to old families
def relabel_families_cluster(cluster, families, max_family, chromosomes, refine_sim):
    return relabel_families_clusterset((cluster,), families, max_family, chromosomes, refine_sim)


# creates new families for genes in the set of genes in occurrences of some cluster,
# returns the new maximum family number and a map mapping new families to old families
def relabel_families_clusterset(cset, families, max_family, chromosomes, refine_sim):
    # set of genes in all clusters (NOT that OCCUR in all clusters)
    genes = sorted(set.union(*(set(c["genes"]) for c in cset)))
    
    # maps old genes to new ones, create a new family for each gene in the clusters
    families_map = odict((g,max_family+i+1) for i,g in enumerate(genes))
    max_family += len(families_map)

    ## FIRST: simple independent cluster / overlapping clusters family relabeling ##
    
    # relabel families in this cluster set
    for cluster in cset:
        for occ in cluster["occs"].values(): # for each cluster occurrence
            for idx, g_occ in enumerate(occ["seq"]): # for each gene in this occ
                g = abs(g_occ)
                if g in families_map: # a gene in the cluster (and that we need to rename)
                    new_family = families_map[g]
                    relabel_family_occ_all(g, new_family, idx, occ, families, chromosomes)

        # updated set of genes
        cluster["genes"] = sorted( families_map[g] if g in families_map else g for g in cluster["genes"])

    rev_map = odict((v,k) for k,v in families_map.items()) # reverse the map 

    if not refine_sim:
        return max_family, rev_map
    
    ## SECOND: relabeling based on DCJ similarity ##
        
    # relabel families in this cluster set, assigning new families
    # based on assignments made for DCJ similarity computation...
    # each position in the reference genome covered by refseqs will be
    # defined as a new family together with the positions in other
    # sequences (not reference) they are mapped to in DCJ similarity
    # computation

    # have to generate again because they've probably changed
    genes = sorted(set.union(*(set(c["genes"]) for c in cset)))

    # new family map, but now values are lists because one family can be split in many
    families_map = odict()

    # build a global map of associations of genes in reference genome to others
    globalmap = defaultdict(odict)
    for cluster in cset:
        refseq = cluster["occs"][cluster["refseq"]]
        clusterid = cluster["cid"]
        for occid, occ in cluster["occs"].items(): # for each cluster occurrence
            update_global_refmap(globalmap, refseq, occ, clusterid, occid)

    # now we create this mapping to know if there are two different
    # positions in the refseq (A) mapped to the same position in some
    # cluster occurrence (B)
    posBmap = defaultdict(set)
    for posA,assoc in globalmap.items():
        for key,posB in assoc.items():
            posBmap[(key[1], posB)].add(posA) # key[1] is spchr
    
    # for each position in reference genome, find the gene family
    globalfamilies = odict()
    for cluster in cset:
        refseq = cluster["occs"][cluster["refseq"]]
        start, _ = get_start_end(refseq["pos"])
        for idx,g in enumerate(refseq["seq"]):
            globalfamilies[start+idx] = abs(g)
            
    # find when we have different positions in refseq associated to
    # the same position in some cluster occurrence
    cant_relabel = set(chain.from_iterable(posA for posA in posBmap.values() if len(posA) > 1))
    #import pdb
    #pdb.set_trace()
    # and finally relabel families (that we can)
    for posA,assoc in globalmap.items():
        family = globalfamilies[posA]
        if family == 0 or family not in genes or posA in cant_relabel:
            continue
        new_family = max_family = max_family+1
        families_map.setdefault(family, []).append(new_family)

        for key,posB in assoc.items(): # gene in posA will be renamed when the occ that is the reference one is processed here
            cid, spchr, occid = key
            cluster = [ c for c in cset if c["cid"] == cid ][0] # find cluster
            occ = cluster["occs"][occid] # find occurrence
            startB, _ = get_start_end(occ["pos"])
            idx = posB - startB
            relabel_family_occ_one(family, new_family, idx, occ, families, chromosomes, cluster["occs"][cluster["refseq"]])

    # and update the gene set of each cluster, removing the gene from the cluster if it isn't in the genes of refseq anymore
    for cluster in cset:
        refseq = cluster["occs"][cluster["refseq"]] #!!!! esse family map talvez tenha q ser uma (nova var?) lista, pq p 1 familia original pode gerar varias outras
        genes = set()
        for g in cluster["genes"]:
            if g not in families_map: # not changed
                genes.add(g)
            else: # changed, but may still exist
                for new in families_map[g]:
                    genes.add(new)
                if g in refseq["genes"]:
                    genes.add(g)
        cluster["genes"] = sorted(genes)

    # update rev_map
    for old, newlist in families_map.items():
        rev_map.update((new,old) for new in newlist)

        
    return max_family, rev_map


# relabel some family in the position idx of sequence in some cluster occurrence,
# also updating the familiy map if not done before, supposing that all genes in that
# family will be relabeled
def relabel_family_occ_all(old, new, idx, occ, families, chromosomes):
    occ["genes"].discard(old) # ok if was removed by a previous copy of this gene
    occ["genes"].add(new) # ok if was added by a previous copy of this gene
    occ["seq"][idx] = new if occ["seq"][idx] >= 0 else -new
    start, end = get_start_end(occ["pos"]) # start position is 1-based
    gene_info = chromosomes[occ["spchr"]][start-1 + idx]

    # if this exact gene copy wasn't already updated by the same gene copy in an overlapping occurrence of this cluster
    if gene_info["family"] != new:
    #if gene_info["family"] == old:
        families[old].remove(gene_info)
        families[new].append(gene_info)
        gene_info.setdefault("oldfamily", []).append(old)
        gene_info["family"] = new


# relabel some family in the position idx of sequence in some cluster occurrence,
# also updating the familiy map if not done before, supposing that the genes at that
# index will be relabeled but other may remain in the old family
def relabel_family_occ_one(old, new, idx, occ, families, chromosomes, refseq):
    occ["genes"].add(new)
    occ["seq"][idx] = new if occ["seq"][idx] >= 0 else -new
    if old not in occ["seq"] and -old not in occ["seq"]: # that was the last copy of that family
        occ["genes"].discard(old)

    start, end = get_start_end(occ["pos"]) # start position is 1-based
    gene_info = chromosomes[occ["spchr"]][start-1 + idx]
    
    # if this exact gene copy wasn't already updated by the same gene copy in an overlapping occurrence of this cluster
    #if gene_info["family"] != new and gene_info["family"] != old:
    #    import pdb
    #    pdb.set_trace()
    #if gene_info["family"] != new:
    if gene_info["family"] == old:
        families[old].remove(gene_info)
        families[new].append(gene_info)
        gene_info.setdefault("oldfamily", []).append(old)
        gene_info["family"] = new


# Gets a map from genes/positions (global-coordinates) of refseq (A)
# associated to genes/positions in other sequence (B), the reference
# sequence (A) and a local-coordinates map from genes/positions of the
# other sequence (B) to the genes/positions in refseq (A), and updates the
# global map (taking into account that the local-coordinate maps
# ignores occurrences of the family "0"). Also gets the current cluster id
# and occurrence id in that cluster. Assumes global map is a defaultdict(OrderedDict)
def update_global_refmap(globalmap, refseq, occ, clusterid, occid):
    startA, _ = get_start_end(refseq["pos"])
    startB, _ = get_start_end(occ["pos"])

    # first we must re-map the refmap, because now we want absolute positions and we need to include 0
    occ["globalmap"] = mapBtoA = local_to_global_refmap(occ, startA, refseq)
    globalkey = (clusterid, occ["spchr"], occid)
    for i,pos in enumerate(mapBtoA):
        if pos == -1:
            continue
        globalmap[pos][globalkey] = startB + i

    
# given a local-coordinates association map from cluster occurrence to
# refseq, and a shift (global-coordinates start), return a new map
# with global-cordinates and -1 for positions not associated (or "0"
# family)
def local_to_global_refmap(occ, shiftocc, refseq):
    # we must compute how many zeros refseq has to up to each index to shift the map properly
    zeros_ref = []
    count = 0
    for i in range(len(refseq["seq"])):
        if refseq["seq"][i] == 0:
            count += 1
        else:
            zeros_ref.append(count)
            
    newmap = []
    idx = 0
    zeros = 0
    for g in occ["seq"]:
        if g == 0: # family 0, wasn't in the refmap
            newmap.append(-1)
            zeros += 1
        elif occ["refmap"][idx] == -1: # -1, was in the refmap but not associated to any in refseq
            newmap.append(-1)
            idx += 1
        else:
            #newmap.append(shiftocc + occ["refmap"][idx] + zeros)
            posref = occ["refmap"][idx]
            newmap.append(shiftocc + posref + zeros_ref[posref])
            idx += 1
    return newmap
    

# returns an integer representing a gene
def get_gene(g):
    return abs(int(g))


# returns a frozenset representing an adjacency for unsigned genes (genes are str)
def unsigned_adjacency(g1, g2):
    return frozenset((abs(int(g1)), abs(int(g2))))


# returns a frozenset representing an adjacency for signed genes
# (genes are str) in a "double markers" scenario (each gene is "split"
# in tail and head with ids 2*g-1 and 2*g respectively)
def signed_adjacency(g1, g2):
    g1 = int(g1)
    g2 = int(g2)
    g1 = 2*abs(g1)-1 if g1 < 0 else 2*abs(g1)
    g2 = 2*abs(g2) if g2 < 0 else 2*abs(g2)-1
    return frozenset((g1, g2))


# given an adjacency for unsigned genes add it to an existing ACS or create a new one
def add_unsigned_adj_to_ACSs(next_cid, adj, data, adjmap, clusters):
    cluster = None
    if adj in adjmap: # cluster exists (we created it in this function before OR there is a conserved cluster found by gecko with exactly these 2 genes)
        cluster = clusters[adjmap[adj]]
        if data["sim"] > cluster["sim"]:
            cluster["sim"] = data["sim"]
    else:
        occs = odict()
        cluster = defaultdict(list)
        cluster.update({"genes":adj, "pvalue":float('nan'), "refseq":0, "occs":occs, "nocc":0, "sim":data["sim"]})
        clusters[next_cid] = cluster
        adjmap[adj] = next_cid
        next_cid += 1

    # for each occ in the cluster that gave "birth" to adjacency,
    # we check if it has the adjacency and if yes we add that occ
    # to the existing ACS whose gene set is the adjacency
    adj_occ = len(cluster["occs"]) - cluster["nocc"] + 1
    for occ in data["occs"].values():
        if adj in occ["unsig_adj"]:
            key = "a" + str(adj_occ)
            adj_occ += 1
            cluster["occs"][key] = {"spchr":occ["spchr"], "sp":occ["sp"], "chr":occ["chr"], "pos":"?", "seq":[], "genes":adj}
                
    return next_cid


# given an adjacency for signed genes add it to an existing ACS or create a new one
def add_signed_adj_to_ACSs(adj, data, adjmap):
    cluster = None
    if adj in adjmap: # cluster exists (we created it in this function before OR there is a conserved cluster found by gecko with exactly these 2 genes)
        cluster = adjmap[adj]
        if data["sim"] > cluster["sim"]:
            cluster["sim"] = data["sim"]
    else:
        occs = odict()
        cluster = defaultdict(list)
        cluster.update({"genes":adj, "pvalue":float('nan'), "refseq":0, "occs":occs, "sim":data["sim"]})
        adjmap[adj] = cluster

    # for each occ in the cluster that gave "birth" to adjacency,
    # we check if it has the adjacency and if yes we add that occ
    # to the existing ACS whose gene set is the adjacency
    adj_occ = len(cluster["occs"])+1
    for occ in data["occs"].values():
        if adj in occ["sig_adj"]:
            key = "a" + str(adj_occ)
            adj_occ += 1
            cluster["occs"][key] = {"spchr":occ["spchr"], "sp":occ["sp"], "chr":occ["chr"], "genes":adj}


# key function to sort clusters based on DCJ sim
def key_cluster_sim(cluster):
    return cluster["sim"]


# cmp function to sort clusters based on cluster length
def cmp_cluster_len(c1, c2):
    if len(c1["genes"]) < len(c2["genes"]):
        return -1
    elif len(c1["genes"]) > len(c2["genes"]):
        return 1
    else:
        return len(c1["occs"]) - len(c2["occs"])

# key function to sort clusters based on cluster length
def key_cluster_len():
    'Convert the cmp= function into a key= function'
    class K(object):
        def __init__(self, obj, *args):
            self.obj = obj 
        def __lt__(self, other):
            return cmp_cluster_len(self.obj, other.obj) < 0 
        def __gt__(self, other):
            return cmp_cluster_len(self.obj, other.obj) > 0 
        def __eq__(self, other):
            return cmp_cluster_len(self.obj, other.obj) == 0
        def __le__(self, other):
            return cmp_cluster_len(self.obj, other.obj) <= 0
        def __ge__(self, other):
            return cmp_cluster_len(self.obj, other.obj) >= 0
        def __ne__(self, other):
            return cmp_cluster_len(self.obj, other.obj) != 0
    return K
    
    

EPILOG="""\
Note that this script does not generate all data needed by ANGES, just
the data related to Gecko-DCJ.

##COG file##
We expect each species header to be exactly in the following format:
<species_name>, chromosome <chromosome_name>

And that each gene comment is the position of the marker in the format
start..end, for instance (starts at 12832 and ends at 13147):
45924   +       ?       NC_012007.3:69665       12832..13147


##Clusters file###
Must be obtained by exporting Gecko's results in clusterData format. 
 
"""

parser = argparse.ArgumentParser(description='Takes Gecko-DCJ input and output files and generates files needed by ANGES.',
                                 epilog=EPILOG,
                                 formatter_class=argparse.RawDescriptionHelpFormatter)

refgroup = parser.add_mutually_exclusive_group()

refgroup.add_argument('--noref', dest="ref_overlap", action='store_false', default=False,
                      help='do not refine families (default).')

refgroup.add_argument('--ref-overlap', action='store_true', default=False,
                      help='try to define refine families for each set of overlapping clusters and their occurrences, ' +
                         'that is, each overlapping cluster set and their occurrences will not share gene families ' +
                         'with other overlapping cluster sets and their occurrences (doesn\'t take into account genes ' +
                         'that are in the interval of a cluster occurrence but are not in the set of genes ' +
                         'of the cluster), see --familiesmap. ')

parser.add_argument('--ref-sim', action='store_true',
                    help='used together with --refine-overlap, for each set of overlapping ' +
                    'clusters the script also tries to relabel multiple occurrences of genes by ' +
                    'their association in the DCJ similarities computed.')

parser.add_argument('--fix-adjacencies', action='store_true',
                    help='try to fix conserved adjacencies by creating new ACSs with them in order ' +
                    'to have a PQ-tree given by ANGES with more blocks with defined gene order.')

limitgroup = parser.add_mutually_exclusive_group()

limitgroup.add_argument('--max-overlap-sim', metavar='int', type=int, default=None,
                        help='limit to this number the amount of overlapping clusters (keep highest DCJ similarity scores)')

limitgroup.add_argument('--max-overlap-len', metavar='int', type=int, default=None,
                        help='limit to this number the amount of overlapping clusters (keep longest)')

ingroup = parser.add_argument_group(title="Input files", description="Files containing data we need to compute outputs.")

ingroup.add_argument('--cog', metavar='file', type=argparse.FileType('r'), required=True,
                     help='file in COG format given as input to Gecko-DCJ, contains all chromosomes data.')

ingroup.add_argument('--clusters', metavar='file', type=argparse.FileType('r'), required=True,
                     help='file containing clusters found by Gecko-DCJ (exported as \"clusterData\").')

outgroup = parser.add_argument_group(title="Output files for ANGES", description="Files that will be generated for ANGES.")

outgroup.add_argument('--markers', metavar='file', type=argparse.FileType('w'), required=True,
                     help='describes all marker/families and their occurrences')

outgroup.add_argument('--acs', metavar='file', type=argparse.FileType('w'), required=True,
                     help='describes all weighted ACSs (clusters) and species they occur.')

outgroup.add_argument('--acsdoubled', metavar='file', type=argparse.FileType('w'), required=True,
                      help='describes all weighted ACSs (clusters) and species they occur with ' +
                           'doubled markers (used by ANGES to represent heads and tails of genes).')

outgroup.add_argument('--chrmap', metavar='file', type=argparse.FileType('w'), required=True,
                      help='file to write chromosome map, mapping the chromosome name to the format ' +
                           'used in ANGES (<species name>.<chromosome number>).')

outgroup.add_argument('--clustersmap', metavar='file', type=argparse.FileType('w'), required=True,
                      help='file to write the correspondence between new clusters (ACSs) ids and old ones.')

outgroup.add_argument('--familiesmap', metavar='file', type=argparse.FileType('w'), required=False,
                      help='file to write the correspondence between new families and old families ' +
                      'when --refine-overlap is used, ignored if --refine-overlap is not used.')

outgroup.add_argument('--ref-cog', metavar='file', type=str, required=False,
                      help='file to write in .cog format genome data with refined families.')

args = parser.parse_args()

if args.ref_overlap and not args.familiesmap:
    parser.print_usage(sys.stderr)
    sys.stderr.write("%s: error: --ref-overlap requires --familiesmap\n" % basename(sys.argv[0]))
    exit(1)

if args.ref_sim and not args.ref_overlap:
    parser.print_usage(sys.stderr)
    sys.stderr.write("%s: error: --ref-sim requires --ref-overlap\n" % basename(sys.argv[0]))
    exit(1)

if args.ref_cog and not args.ref_overlap:
    parser.print_usage(sys.stderr)
    sys.stderr.write("%s: error: --ref-cog requires --ref-overlap\n" % basename(sys.argv[0]))
    exit(1)
    

# read cog/markers
print("%s Reading cog" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
families = defaultdict(list) # families (genes) and their occurrences (copies)
chromosomes = defaultdict(list) # chromosomes content (key is <species name>.<chromosome number>)
species = chrname = comment = ""
chrmap = odict()
line = args.cog.readline()
while line:
    line = line.strip()
    if not line: # end of chromosome
        species = chrname = comment = ""
    elif not chrname: # first line of new chromosome
        species, chrname = line.split(",")
        species = species.strip()
        chrname = chrname.strip().replace("chromosome", "").strip()
        if species not in chrmap:
            chrmap[species] = odict()
        chrnum = len(chrmap[species]) + 1
        chrmap[species][chrname] = chrnum
    elif not comment: # second line of new chromosome
        comment = line
    else: # gene line
        gene = line.split()
        family = int(gene[0])
        strand = gene[1]
        func = gene[2]
        name = gene[3]
        spchr = "%s.%d" % (species, chrnum)
        pos = gene[4].replace("..", "-")
        gene_info = {"spchr":spchr, "pos":pos, "family":family, "strand":strand, "func":func, "name":name}
        if family != 0:
            families[family].append(gene_info)
        chromosomes[spchr].append(gene_info)

    line = args.cog.readline()


# read clusters
print("%s Reading clusters" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
clusters = odict()
family_cl = defaultdict(list) # for each family saves which clusters have it
clusters_f = args.clusters
clusters_f.readline() # discard first line
line = clusters_f.readline()
while line:
    cid, pvalue, refseq = [ i.split("=")[1].strip() for i in line.split(":")[1].split(",") ]
    cid = int(cid)
    line = clusters_f.readline()
    occs = odict()
    line = clusters_f.readline()
    while not line.startswith("ChromosomeNr"):
        occ, rest = line.split(":")
        species, rest = rest.split(",", maxsplit=1)
        species = species.strip()
        chrname, rest = rest.split("[")
        chrname = chrname.replace("chromosome", "").strip()
        rest = "[" + rest
        pos, rest = rest.split(maxsplit=1)

        genes = set()
        seq = []
        for s in rest.split():
            gene = int(s.strip("<>"))
            strand = "-" if s.startswith("<") else "+"
            if (gene != 0): genes.add(gene)
            seq.append(gene if strand == "+" else -gene)
            
        spchr = "%s.%d" % (species, chrmap[species][chrname])
        occs[occ] = {"spchr":spchr, "sp":species, "chr":chrname, "pos":pos, "seq":seq, "genes":genes}

        line = clusters_f.readline()

    # skip to where we find similiarity scores
    while not line.startswith("DCJ similarities"):
        line = clusters_f.readline()
    line = clusters_f.readline()

    while "DCJ similarity" in line:
         occ, rest = line.split(":")
         occ = occ.strip()
         rest = rest.split(", ")
         sim = float(rest[0].replace("DCJ similarity =", "").strip())
         occs[occ]["sim"] = sim
         refmap = eval(rest[-1].replace("refseq positions =", "").strip()) # should be a list
         occs[occ]["refmap"] = refmap # map to genes in reference sequence (ignoring "0" families)
         line = clusters_f.readline()

    genes = set()
    while line.strip(): # while we dont find an empty line, we read genes in the cluster and discard their occurrences
        if line.startswith("\t"):
            pass # gene occ
        else: # gene in reference
            genes.add(int(line.split(":", maxsplit=1)[0]))
        line = clusters_f.readline()
        
    # remove genes which are the single ones in their families
    for g in list(genes):
        if len(families[g]) < 2:
            genes.remove(g)

    for g in set.union(*(set(occ["genes"]) for occ in occs.values())):
        family_cl[g].append(cid)
            
    genes = sorted(genes)
    
    cluster = defaultdict(list)
    cluster.update({"cid":cid, "genes":genes, "pvalue":pvalue, "refseq":refseq, "occs":occs})
    clusters[cid] = cluster

    # start next cluster
    line = clusters_f.readline()


print("%s Computing average DCJ similarity for clusters" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
compute_dcj_sim(clusters)
#normalize_dcj_sim(clusters)


if args.ref_overlap or args.max_overlap_sim or args.max_overlap_len:
    print("%s   Computing overlaps" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
    # for cid, cid2 in combinations(clusters, 2):
    #     cluster = clusters[cid]
    #     cluster2 = clusters[cid2]
    #     if set(cluster["genes"]).isdisjoint(cluster2["genes"]): # skip if they don't share any gene
    #         continue
    #     if overlap(cluster, cluster2):
    #         cluster["overlaps"].append(cid2)
    #         cluster2["overlaps"].append(cid)
    already_computed = set()
    count = 1
    for f,clist in family_cl.items():
        print("%s     With family %d (%d clusters), %d/%d" % (datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"), f, len(clist), count, len(family_cl)))
        count += 1
        for cid, cid2 in combinations(clist, 2):
            pair = frozenset((cid, cid2))
            if pair in already_computed:
                continue
            already_computed.add(pair)
            cluster = clusters[cid]
            cluster2 = clusters[cid2]
            if overlap(cluster, cluster2):
                cluster["overlaps"].append(cid2)
                cluster2["overlaps"].append(cid)

    if args.max_overlap_sim or args.max_overlap_len:
        max_overlap = args.max_overlap_sim or args.max_overlap_len
        print("%s   Filtering overlapping clusters" % (datetime.now().strftime("[%d/%m/%Y %H:%M:%S]")))
        overlapping = [ cid for cid,c in clusters.items() if len(c["overlaps"]) > 0 ]
        g = Graph()
        g.add_nodes_from(overlapping)
        for cid in overlapping:
            g.add_edges_from( (cid, cid2) for cid2 in clusters[cid]["overlaps"] )
        components = list(sorted(connected_components(g), key=len, reverse=True))
        for i, component in enumerate(components):
            if len(component) <= max_overlap:
                break
            print("%s     Component %d/%d (%d clusters)" % (datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"), i+1, len(components), len(component)))
            nodes = [ clusters[cid] for cid in component ]
            if (args.max_overlap_sim):
                nodes.sort(key=key_cluster_sim, reverse=True)
            else:
                nodes.sort(key=key_cluster_len(), reverse=True)
            for cluster in nodes[max_overlap:]:
                cid = cluster["cid"]
                for ov in cluster["overlaps"]:
                    if ov in clusters: # it the other cluster wasn't removed
                        clusters[ov]["overlaps"].remove(cid)
                del clusters[cid]


# relabeling families
newfamilies_map = odict()
if args.ref_overlap:
    print("%s Relabeling families" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
    # checking overlapping clusters

    #ov = [ c for c in clusters.values() if len(c["overlaps"]) > 0 ] # for testing/statistics only

    max_family = max(families.keys())
    
    # solve easily the refinement of families for clusters/occurrences that do not overlap with others
    print("%s   Relabeling families in non-overlapping clusters" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
    count = 1
    nonoverlapping = list((cid,cluster) for cid,cluster in clusters.items() if cluster["overlaps"] == 0)
    #import pdb
    #pdb.set_trace()
    for cid, cluster in nonoverlapping:
        print("%s     Cluster %d/%d" % (datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"), count, len(nonoverlapping)))
        count += 1
        max_family, new_families = relabel_families_cluster(cluster, families, max_family, chromosomes, args.ref_sim)
        newfamilies_map.update(new_families)

    # now solve for clusters that do overlap, creating a graph and finding connected components to define new families for each component
    overlapping = [ cid for cid,c in clusters.items() if len(c["overlaps"]) > 0 ]
    print("%s   Relabeling for %d overlapping clusters" % (datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"), len(overlapping)))
    g = Graph()
    g.add_nodes_from(overlapping)
    for cid in overlapping:
        g.add_edges_from( (cid, cid2) for cid2 in clusters[cid]["overlaps"] )
    components = list(connected_components(g))
    for i, component in enumerate(components):
        print("%s     Component %d/%d (%d clusters)" % (datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"), i+1, len(components), len(component)))
        max_family, new_families = relabel_families_clusterset([ clusters[c] for c in component ], families, max_family, chromosomes, args.ref_sim)
        newfamilies_map.update(new_families)

    # remove empty occurrences from both maps
    for family in list(newfamilies_map.keys()):
        if len(families[family]) == 0:
            del newfamilies_map[family]
    for family,occs in list(families.items()):
        if len(occs) == 0:
            del families[family]


# write a new .cog with relabeled families
if args.ref_cog:
    print("%s Writing refined cog" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
    with open(args.ref_cog, "w") as f:
        for species,chrnames in chrmap.items():
            for chrname, chrnum in chrnames.items():
                chromosome = chromosomes["%s.%d" % (species, chrnum)]
                #import pdb
                #pdb.set_trace()
                f.write("%s, chromosome %s\n" % (species, chrname))
                
                cog_chr = []
                for g in chromosome:
                    occsp = set(occ['spchr'].split(".")[0] for occ in families[g["family"]]) # species it occurs
                    if len(occsp) > 1:# this gene goes to the .cog file only if it happens in more than one species
                        cog_chr.append(g)
                    #cog_chr.append(g)

                f.write("%d atoms, with refined families\n" % len(cog_chr))
                for g in cog_chr:
                    f.write("%d\t%s\t%s\t%s\t%s\n" % (g["family"], g["strand"], g["func"], g["name"], g["pos"].replace("-","..")))
                f.write("\n")
                

            
# write families, map of new families (empty file if --noref)
for new_family, old_family in newfamilies_map.items():
    args.familiesmap.write("%d %d\n" % (new_family, old_family))

        
# add to clusters genes appearing in more than half of occurrences and remove singletons
print("%s Filtering/inserting genes in clusters" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
for cluster in clusters.values():
    if len(cluster["occs"]) < 3: # doesn't make sense if the cluster has only 2 occurrences, half is only one...
        continue
    # count how many times genes appear in cluster occurrences
    genes_in_occs = Counter()
    for occ in cluster["occs"].values():
        for g in occ["genes"]:
            genes_in_occs[g] += 1
    # add to cluster genes that appear in at least half of occurrences
    genes = set(cluster["genes"])
    half = len(cluster["occs"]) / 2.0
    for g, n in genes_in_occs.items():
        if n >= half and g not in genes:
            genes.add(g)
    # remove genes which occur in just one species
    for g in list(genes):
        occsp = set(occ['spchr'].split(".")[0] for occ in families[g]) # species it occurs
        #if len(families[g]) < 2: # this would keep families occurring 2 times in a single species
        if len(occsp) < 2:
            genes.remove(g)
    cluster["genes"] = sorted(genes)

    
# write markers
print("%s Writing markers" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
markers_f = args.markers
for family in sorted(families.keys()):
    copies = families[family]
    if len(copies) < 2:
        continue
    markers_f.write(">%d%s\n" % (family, " was %d" % newfamilies_map[family] if family in newfamilies_map else ""))
    for gene in copies:
        markers_f.write("%s:%s %s\n" %
                        (gene["spchr"],
                         gene["pos"],
                         gene["strand"]))
    markers_f.write("\n")


# write chromosomes names
chrmap_f = args.chrmap
print("%s Writing chromosome names" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
for s, clist in chrmap.items():
    chrmap_f.write("%s\n" % s)
    for cname, cnum in clist.items():
        chrmap_f.write("%s: %s.%s\n" % (cname, s, cnum))
    chrmap_f.write("\n")

    
# deep copy of clusters (soon this data will be changed)
print("%s Saving copy of data" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
clusters_doubled = copy.deepcopy(clusters)


# before changing save for later
print("%s Computing genes in clusters" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
genes_in_clusters = sorted(set(chain.from_iterable(cluster["genes"] for cluster in clusters.values())))


# save the original number of occurrences of each cluster (before modifying clusters set)
for data in clusters.values():
    data["nocc"] = len(data["occs"])

# add to clusters set ACSs for adjacencies that "probably" are conserved in the ancestor
if args.fix_adjacencies:
    print("%s Fixing adjacencies" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
    adjmap = dict()
    for cid,data in clusters.items():
        if len(data["genes"]) == 2:
            genes = list(data["genes"])
            adjmap[unsigned_adjacency(genes[0], genes[1])] = cid
    next_cid = max(clusters.keys())
    for data in list(clusters.values()): # list, since we may change the dict
        if len(data["genes"]) == 2:
            continue
        for occ in data["occs"].values():
            seq = occ["seq"]
            adjset = set()
            for i in range(len(seq)-1):
                g1 = get_gene(seq[i])
                g2 = get_gene(seq[i+1])
                if g1 != g2 and g1 != 0 and g2 != 0: # not the same gene, cluster is defined by a SET of genes
                    adjset.add(unsigned_adjacency(seq[i], seq[i+1]))
            occ["unsig_adj"] = adjset
        adjcount = Counter()
        for occ in data["occs"].values():
            for adj in occ["unsig_adj"]:
                if adj.issubset(data["genes"]): # only if both genes are in 
                    adjcount[adj] += 1
        for adj,nocc in adjcount.items(): # check if greater than threshold based on original number of occurrences
            if nocc/float(data["nocc"]) >= THRESHOLD_ADJ:
                next_cid = add_unsigned_adj_to_ACSs(next_cid, adj, data, adjmap, clusters)


# write ACSs
print("%s Writing ACSs" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
new_cid = 0
for data in clusters.values():
    args.acs.write("%d|%s;%s:%s\n"
                   % (new_cid,
                      data["sim"],
                      ",".join(set(occ["sp"] for occ in data["occs"].values())),
                      " ".join(map(str, data["genes"]))))
    new_cid += 1

    
# write ACSs mapping (ACS id to cluster id)
print("%s Writing mapping of ACS IDs to cluster IDs" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
new_cid = 0
for cid in clusters.keys():
    args.clustersmap.write("%d %d\n" % (new_cid,cid))
    new_cid += 1

    
# double markers for genes sets, but keep sequences as they are (original marker ids)
print("%s Doubling markers" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
for data in clusters_doubled.values():
    genes = []
    for g in data["genes"]:
        genes.append(2*g-1)
        genes.append(2*g)
    data["genes"] = genes
    for occ in data["occs"].values():
        genes = set()
        for g in occ["genes"]:
            genes.add(2*g-1)
            genes.add(2*g)
        occ["genes"] = genes


# create a new set ACSs for adjacencies that "probably" are conserved in the ancestor (signed/doubled markers)
if args.fix_adjacencies:
    print("%s Fixing adjacencies in doubled ACSs" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
    adjmap = odict()
    for data in clusters_doubled.values():
        for occ in data["occs"].values():
            seq = occ["seq"]
            adjset = set()
            for i in range(len(seq)-1):
                g1 = get_gene(seq[i])
                g2 = get_gene(seq[i+1])
                if g1 != g2 and g1 != 0 and g2 != 0: # not the same gene, cluster is defined by a SET of genes
                    adjset.add(signed_adjacency(seq[i], seq[i+1]))
            occ["sig_adj"] = adjset
        adjcount = Counter()
        for occ in data["occs"].values():
            for adj in occ["sig_adj"]:
                if adj.issubset(data["genes"]): # only if both genes are in the cluster
                    adjcount[adj] += 1
        for adj,nocc in adjcount.items():
            if nocc/float(len(data["occs"])) >= THRESHOLD_ADJ:
                add_signed_adj_to_ACSs(adj, data, adjmap)


# write ACSs with doubled markers
print("%s Writing doubled ACSs" % datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"))
new_cid = 0
for data in clusters_doubled.values():
    args.acsdoubled.write("%s|%s;%s:%s\n"
                          % (new_cid,
                             data["sim"],
                             ",".join(set(occ["sp"] for occ in data["occs"].values())),
                             " ".join(map(str, data["genes"])))) # doubled already
                             #" ".join("%d %d" % (2*g-1, 2*g) for g in data["genes"]))) # if not doubled yet
    new_cid += 1


# we also need to include an ACS with each doubled marker as an adjacency and weight 10000
# this is important for head and tail staying together for each gene
# THIS code for only families happening in some cluster
#cid = max(families.keys()) + 1
for g in genes_in_clusters: 
    args.acsdoubled.write("%d|100000;%s:%d %d\n"
                          % (new_cid,
                             ",".join(set(re.sub('\.[0-9]*$', '', occ["spchr"]) for occ in families[g])),
                             2*g-1,
                             2*g))
    new_cid += 1
# or THIS code for all families:
# cid = max(families.keys()) + 1
# for g,data in families.items(): # this is important for head and tail staying together for each gene
#     args.acsdoubled.write("%s|10000;%s:%d %d\n"
#                           % (cid,
#                              ",".join(set(re.sub('\.[0-9]*$', '', occ["spchr"]) for occ in data)),
#                              2*g-1,
#                              2*g))
#     cid += 1

# write ACSs of conserved adjacencies
if args.fix_adjacencies:
    for data in adjmap.values():
        args.acsdoubled.write("%s|%s;%s:%s\n"
                              % (new_cid,
                                 data["sim"],
                                 ",".join(set(occ["sp"] for occ in data["occs"].values())),
                                 " ".join(map(str, data["genes"])))) # doubled already
                                 #" ".join("%d %d" % (2*g-1, 2*g) for g in data["genes"]))) # if not doubled yet
        new_cid += 1

print("%s Done (%d ACSs, %d ACSs doubled)" % (datetime.now().strftime("[%d/%m/%Y %H:%M:%S]"), len(clusters), new_cid))

exit(0)
