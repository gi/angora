#!/usr/bin/env python3

from argparse import ArgumentParser, FileType, ArgumentDefaultsHelpFormatter as ADHF
from sys import stdout, stderr, exit
from collections import deque
import csv

from Bio import SeqIO
import networkx as nx
import numpy as np

def readBlastOutput(*data):

    res = dict()

    for f in data:
        for qid, sid, percent_identity, aln_len, mismatch, gap_open, qstart, \
                qend, sstart, send, evalue, bitscore in csv.reader(f, \
                delimiter='\t'):
            if qid < sid:
                pair_id = (qid, sid)
            else:
                pair_id = (sid, qid)

            if pair_id not in res:
                res[pair_id] = list()
            res[pair_id].append((float(percent_identity), int(aln_len),
                int(mismatch), int(gap_open), int(qstart), int(qend),
                int(sstart), int(send), float(evalue), float(bitscore)))
    return res


def readSeqLens(data):

    res = list()
    for rec in SeqIO.parse(data, 'fasta'):
        res.append((rec.id, len(rec)))

    return res


def getNonOverlappingHSPs(hsps):
    """ identify a set of non-overlapping hsps by approximating the independent
    set problem """

    if len(hsps) <= 1:
        return hsps

    G = nx.Graph()
    G.add_nodes_from(range(len(hsps)))
    idx = sorted(range(len(hsps)), key=lambda x: hsps[x][4:6]) 
    stack = deque()
    for i in idx:
        while stack and hsps[stack[0]][5] < hsps[i][4]:
            stack.popleft()
        for j in stack:
            G.add_edge(i, j)
        stack.append(i)

    idy = sorted(range(len(hsps)), key=lambda x: hsps[x][6:8]) 
    stack = deque()
    for i in idy:
        while stack and hsps[stack[0]][7] < hsps[i][6]:
            stack.popleft()
        for j in stack:
            G.add_edge(i, j)
        stack.append(i)

    mx_set = nx.algorithms.maximal_independent_set(G)

    if len(hsps) > 2:
        sample_size = len(hsps) * 10
        for _ in range(sample_size-1):
            s = nx.algorithms.maximal_independent_set(G)
            if len(s) > len(mx_set):
                mx_set = s
    return [hsps[x] for x in mx_set]
        


def blastOut2HomologyGraph(blast_out, seqlens, t_cip, t_calp):

    G = nx.Graph()
    for (g1, g2), hsps in list(blast_out.items()):
        non_overlapping_hsps = getNonOverlappingHSPs(hsps)
        cip = computeCIP(non_overlapping_hsps)
        calp = computeCALP(non_overlapping_hsps, max(seqlens[g1], seqlens[g2]))

        if cip >= t_cip and calp >= t_calp:
            G.add_edge(g1, g2)

    return G


def computeCIP(hsps):
    """ Cumulative Identity Percentage (CIP) is defined as the sum of the
    number of matching nucleotides for each high-scoring segment pair (HSP) of
    a pair of genes divided by the total lengths of those HSPs.
    """

    nident = 0
    total_len = 0
    for hsp in hsps:
        percent_identity, aln_len = hsp[:2]
        qstart, qend, sstart, send = hsp[4:8]
        nident += int(np.round(percent_identity/100 * aln_len))
        total_len += max(send-sstart, qstart-qend)

    return float(nident)/total_len


def computeCALP(hsps, seq_len):
    """ Cumulative alignment length percentage (CALP) is defined as the sum of
    the alignment lengths of all HSPs of a matching gene pair divided by the
    total length of the query sequence."""

    total_len = 0
    for hsp in hsps:
        total_len += hsp[1]
    return float(total_len)/seq_len


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ADHF)
    parser.add_argument('fasta_files', type=FileType('r'), nargs='+',
            help='fasta files of BLASTed sequences')
    parser.add_argument('blast_output', type=FileType('r'), nargs='+',
            help='BLAST output in standard table format')
    parser.add_argument('-i', '--cip', type=float, default=0.6,
            help='threshold for CIP (Cumulative Identity Percentage)')
    parser.add_argument('-a', '--calp', type=float, default=0.7,
            help='threshold for CALP (Cumulative Alignment Length Percentage)')
    args = parser.parse_args()


    seqlens = dict()
    for f in args.fasta_files:
        seqlens.update(readSeqLens(f))

    blast_out = readBlastOutput(*args.blast_output)
    
    G = blastOut2HomologyGraph(blast_out, seqlens, args.cip, args.calp)
    #import pdb; pdb.set_trace() 

    out = stdout
    for C in nx.connected_components(G):
        print('\t'.join(sorted(C)), file=out)

