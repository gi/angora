#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Reads markers from file passed as argument (ANGES markers format)
# and prints some statistics


import argparse
from collections import Counter, defaultdict, OrderedDict
import statistics
import math
from numpy import histogram

try:
    import matplotlib.pyplot as plt
except ImportError:
    plt = None

DEFAULT_HISTOGRAM_BINS = "[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]"
DEFAULT_HISTOGRAM_PERC = 0.99
DEFAULT_REFERENCE = "grape"

class ClustersStatistics:

    def __init__(self, histogram_cut, histogram_bins, plot):
        self.histogram_cut = histogram_cut
        self.histogram_bins = histogram_bins
        self.plot = plot and plt
        self.figures = 0

    # Statistics for some datasets (prints and generate figures)
    def statistics(self, names, datasets, note):
        for data in datasets:
            data.sort()

        datasets_cut = []
        for name, data in zip(names, datasets):
            print("Statistics for %s:" % name)
            self._pprint("Number", len(data))
            self._pprint("Min", min(data))
            self._pprint("Max", max(data))
            self._pprint("Mean", statistics.mean(data))
            for p in [0.5, 0.75, 0.9, 0.99]:
                self._pprint("Percentile %d%%" % int(p*100), int(self._percentile(data, p)))
            hdata = data[:self._percentile_pos_low(data, self.histogram_cut)+1]
            datasets_cut.append(hdata)

            hist, bin_edges = histogram(hdata, bins=self.histogram_bins)
            self._pprint("Histogram in percentile %d%%" % int(self.histogram_cut*100))
            for i in range(len(hist)):
                self._pprint("[%s,%s%s" % (self._float2str(bin_edges[i]), self._float2str(bin_edges[i+1]),
                                     "]" if i == len(hist)-1 else ")"),
                       "%d (%s%%)" % (hist[i], self._float2str(100*hist[i]/len(hdata))),
                       indentation=2)

        averages = []
        print("Average number of cluster occurrences (in percentile %d%%):" % int(self.histogram_cut*100))
        for name, data in zip(names, datasets):
            avg = sum(data)/float(len(data))
            averages.append(avg)
            self._pprint(name, avg, indentation=1)
                
        print()
        if plot:
            self.figures += 1
            plt.figure(self.figures)
            x_pos = range(len(names))
            plt.xticks(x_pos, names)
            plt.suptitle("Average number of cluster occurrences (in percentile %d%%)" % int(self.histogram_cut*100))
            plt.title(note)
            for i, v in enumerate(averages):
                plt.text(i, v+0.05, "%.2f" % v, ha='center', fontweight='bold')
            axes = plt.gca()
            axes.set_ylim([None, max(averages) + 0.25])
            plt.bar(x_pos, averages)
            
            self.figures += 1
            plt.figure(self.figures)
            plt.hist(datasets_cut, bins=self.histogram_bins, label=names)
            plt.suptitle("Histograms in percentile %d%%" % int(self.histogram_cut*100))
            plt.title(note)
            plt.legend(loc='upper right')
            #plt.yscale('log')

    # Show figures generated so far (blocking)
    def show_figures(self):
        if plot:
            plt.show()

    # Pretty print
    def _pprint(self, name, value = None, indentation = 1):
        if isinstance(value, float):
            value_str = "%.1f" % value
        elif isinstance(value, int):
            value_str = "%d" % value
        elif value is None:
            value_str = ""
        else:
            value_str = str(value)

        indentstr = "    "
        print("%s%-15s %s" % (indentstr*indentation, name+":", value_str))

    # Returns a formated string without trailing zeros
    def _float2str(self, value):
        return ("%.1f" % value).rstrip('0').rstrip('.')

    # Returns the position of percentile
    def _percentile_pos_low(self, data, percent):
        return math.floor((len(data)-1) * percent)

    # Calculates percentile (based on http://code.activestate.com/recipes/511478/)
    # Data must be sorted
    def _percentile(self, data, percent): 
        if not data:
            return None
        k = (len(data)-1) * percent
        f = math.floor(k)
        c = math.ceil(k)
        if f == c:
            return data[int(k)]
        d0 = data[int(f)] * (c-k)
        d1 = data[int(c)] * (k-f)
        return d0+d1


EPILOG="""
Note 1: Percentile 𝑝% shows a value 𝑣 for which 𝑝% of data is ≤ 𝑣,
e.g. Percentile 75%: 21 means 75% of data is ≤ 21.

Note 2: Histogram in percentile 𝑝% shows, for the 𝑝% smaller values in
the data, equal-width ranges and the numbers of elements in the data
that fit that ranges.

Note 3: The parameter hbins can be a number or a method among auto,
fd, doane, scott, stone, rice, sturges or sqrt. See
<https://docs.scipy.org/doc/numpy/reference/generated/
 numpy.histogram_bin_edges.html#numpy.histogram_bin_edges>
for details on methods.
"""

parser = argparse.ArgumentParser(description='Reads clusters from file passed as argument (Gecko clusterData export format)\nand prints some statistics.',
                                 epilog=EPILOG, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('clusters', metavar='file', type=argparse.FileType('r'),
                    help='contains clusters exported by Gecko in clusterData format.')
parser.add_argument('--hcut', metavar='float', type=float, default=DEFAULT_HISTOGRAM_PERC,
                    help='compute histogram for percentile (default: %g)' % DEFAULT_HISTOGRAM_PERC)
parser.add_argument('--hbins', metavar='value', type=str, default=DEFAULT_HISTOGRAM_BINS,
                    help='number of histogram bins/ranges, method or list of values (default: %s)' % DEFAULT_HISTOGRAM_BINS)
parser.add_argument('--plot', '-p', action='store_true',
                    help='show histogram plot (if matplotlib.pyplot is available)')
parser.add_argument('--reference', metavar='name', type=str, default=DEFAULT_REFERENCE,
                    help='reference species name, which will be ignored in plots (default: %s)' % DEFAULT_REFERENCE)
args = parser.parse_args()

plot = args.plot
hcut = args.hcut

if args.hbins.isdigit():
    hbins = int(args.hbins)
else:
    try: # try to check if its a list...
        hbins = eval(args.hbins)
    except:
        hbins = args.hbins


# read clusters
clusters_f = args.clusters
clusters = OrderedDict()
clusters_f.readline() # discard first line
line = clusters_f.readline()
species_names = set()
while line:
    
    cid, pvalue, refseq = [ i.split("=")[1].strip() for i in line.split(":")[1].split(",") ]
    line = clusters_f.readline()
    occs = OrderedDict()
    line = clusters_f.readline()
    while not line.startswith("ChromosomeNr"):
        occ, rest = line.split(":")
        species, rest = rest.split(",", maxsplit=1)
        species = species.strip()
        chrname, rest = rest.split("[")
        chrname = chrname.replace("chromosome", "").strip()
        rest = "[" + rest
        pos, rest = rest.split(maxsplit=1)
        seq = [ "%s%s" % ("-" if s.startswith("<") else "+", s.strip("<>")) for s in rest.split() ]

        occs[occ] = {"sp":species, "chr":chrname, "pos":pos, "seq":seq}
        species_names.add(species)

        line = clusters_f.readline()

    # skip to where we find similiarity scores
    while not line.startswith("DCJ similarities"):
        line = clusters_f.readline()
    line = clusters_f.readline()

    while "DCJ similarity" in line:
         occ, rest = line.split(":")
         occ = occ.strip()
         sim, rest = rest.split(",", maxsplit=1)
         sim = float(sim.replace("DCJ similarity =", "").strip())
         occs[occ]["sim"] = sim
         line = clusters_f.readline()

    genes = []
    while line.strip(): # while we dont find an empty line, we read genes in the cluster and discard their occurrences
        if line.startswith("\t"):
            pass # gene occ
        else: # gene in reference
            genes.append(int(line.split(":", maxsplit=1)[0]))
        line = clusters_f.readline()
    genes.sort()
    
    clusters[cid] = {"genes":genes, "pvalue":pvalue, "refseq":refseq, "occs":occs}

    # start next cluster
    line = clusters_f.readline()

    
occ_counter = defaultdict(Counter)
occ_counter_allsp = defaultdict(Counter)
for cid, data in clusters.items():
    occs = data["occs"]
    for occ in occs.values():
        occ_counter[occ["sp"]][cid] += 1
    if len(set( [ occ["sp"] for occ in occs.values() ] )) == len(species_names):
        for occ in occs.values(): # we count just if it occurs in all species
            occ_counter_allsp[occ["sp"]][cid] += 1

stat = ClustersStatistics(hcut, hbins, plot)
       
note = "(all clusters)"
print(note)
names = [ n for n in occ_counter.keys() if n != args.reference ]
datasets = [ list(occ_counter[n].values()) for n in names ]
stat.statistics(names, datasets, note)

note = "(clusters that occur in all sp.)"
print(note)
names = [ n for n in occ_counter_allsp.keys() if n != args.reference ]
datasets = [ list(occ_counter_allsp[n].values()) for n in names ]
stat.statistics(names, datasets, note)

stat.show_figures()

exit(0)
