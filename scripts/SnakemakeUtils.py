#!/usr/bin/env python3

import os
from sys import stderr
from itertools import filterfalse, chain

from snakemake.workflow import Workflow
from snakemake.dag import DAG
from snakemake.logging import setup_logger


# Lists all shell commands that must be executed for some give target.
# By default doesn't list commands to recompute files that exist and are not empty.
# Also by default creates the directory tree needed for storing the target files.
class SnakemakeShellCmds:
    UMASK = 0o007

    def __init__(self, snakefile, force=False, create_dirs=True):
        setup_logger()
        self.FORCE = force
        self.CREATE_DIRS = create_dirs

        # change working dir to where the snakefile is and update its path
        self.snakefile = os.path.abspath(snakefile)
        self.wdir = os.path.dirname(self.snakefile)
        self.snakefile = os.path.basename(self.snakefile)
        os.chdir(self.wdir)

        # load workflow and snakefile
        self.w = Workflow(snakefile=self.snakefile)#, printshellcmds=True)
        self.w.include(self.snakefile, overwrite_first_rule=True)#, print_compilation=True)

        # get the manager
        self.manager = self.w.globals["MANAGER"]

    @staticmethod
    def is_empty_file(fpath):
        return not os.path.isfile(fpath) or os.path.getsize(fpath) == 0

    def rules(self, items):
        return map(self.w.rules, filter(self.w.is_rule, items))

    def files(self, items):
        relpath = lambda f: f if os.path.isabs(f) else os.path.relpath(f)
        return map(relpath, filterfalse(self.w.is_rule, items))

    def get(self, var):
        return self.w.globals[var]

    # Return cmds for generating targets and creates their parent directories, if they don't exist
    def shellcmds(self, targets):
        self.w.check_localrules()
        targetrules = set(chain(self.rules(targets)))
        targetfiles = set(chain(self.files(targets)))
        self.w.global_resources = dict()

        if self.FORCE:
            forcefiles = targetfiles
            forcerules = targetrules
        else:
            forcefiles = None
            forcerules = None
        
        dag = DAG(
            self.w, self.w.rules,
            dryrun=True,
            targetfiles=targetfiles,
            targetrules=targetrules,
            priorityfiles=set(),
            priorityrules=set(),
            forcefiles=forcefiles,
            forcerules=forcerules)
            #forceall=self.FORCE) # this would force everything... we should't do this just to find later the files are there and ignore the shellcmd

        dag.init()
        dag.check_dynamic()
        dag.postprocess()

        jobs = [ job for job in dag.needrun_jobs ]

        if self.CREATE_DIRS:
            dirs = set()
            for job in jobs:
                for f in job.output:
                    d = os.path.dirname(f)
                    if not os.path.exists(d):
                        dirs.add(d)
                    
            os.umask(self.UMASK) # so we create directories with the correct permissions
            for d in dirs:
                os.makedirs(d)

        cmds = []
        for job in jobs:
            #if self.FORCE or any(self.is_empty_file(out) for out in job.output): # ignore jobs with all files present and not empty
            if job.shellcmd is not None:
                cmds.append(job.shellcmd)

        return cmds

## lines used for testing that may be useful
#snakemake.snakemake("../Snakefile", listrules=True, print_compilation=True, dryrun=True)
#w.list_rules()
#r=w.get_rule("combine_all_chr_pairs")
#w.execute(targets=[atoms_out + '/sunflower:lettuce/NC_035442:lg_4/224078901-246315975:338621347-377529803.psl'],dryrun=True)
#w.execute(targets=[atoms_out + '/sunflower:lettuce/NC_035442:lg_4/224078901-246315975:338621347-377529803.psl'],dryrun=True,forcetargets=True,updated_files=list())

if __name__ == '__main__':
    print("Error: this file should be used as a module!", file=stderr)
    exit(1)

