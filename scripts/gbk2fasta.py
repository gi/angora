#!/usr/bin/env python3

from sys import stdout, stderr, exit, argv
from Bio import SeqIO

if __name__ == '__main__':
    if len(argv) != 2:
        print('\tusage: %s <GBK FILE>' %argv[0])
        exit(1)

    for rec in SeqIO.parse(open(argv[1]), 'genbank'):
        SeqIO.write(rec, stdout, 'fasta')



