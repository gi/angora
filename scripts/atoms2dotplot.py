#!/usr/bin/env python3

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter as ADHF, \
        FileType
from sys import stdout, stderr, exit
from itertools import combinations
import logging
import csv
import re

import os
if not os.environ.get('DISPLAY', None):
    import matplotlib; matplotlib.use('Agg')

import numpy as np
import matplotlib.pylab as plt


LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)

RESOLUTION = 500


def readAtoms(data, chromosomes_only=None):

    res = list()
    for line in csv.reader(data, delimiter='\t'):
        if line[0].startswith('#'):
            continue
        if chromosomes_only == None or line[0] in chromosomes_only:
            res.append((line[0], int(line[4]), int(line[5]), line[3], line[2]))
    return res


def getChromNamesAndSizes(atoms):
    res = dict()

    for chrx, startx, endx, _, _ in atoms:
        if chrx not in res:
            res[chrx] = 0
        res[chrx] = max(res[chrx], endx)

    return list(zip(*list(res.items())))


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ADHF)
    parser.add_argument('atoms_file', type=FileType('r'),
            help='Output file of Atomizer')
    parser.add_argument('-c', '--chromosomes_only', type=str, nargs='+',
            help = 'only display specified chromosomes')

    args = parser.parse_args()

    #
    # setup logging
    #
    ch = logging.StreamHandler(stderr)
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
    LOG.addHandler(ch)

    atoms = readAtoms(args.atoms_file, chromosomes_only=args.chromosomes_only)
    chromnames, chromsizes = getChromNamesAndSizes(atoms)

    if len(chromnames) != 2:
        LOG.fatal(('currently, this view only supports the visualization of ' +
                'exactly two chromosomes, but %s are given') %len(chromnames))
        exit(1)

    chr1size = int(chromsizes[0]/float(max(chromsizes)) * RESOLUTION)
    chr2size = int(chromsizes[1]/float(max(chromsizes)) * RESOLUTION)

    f1 = chr1size/float(chromsizes[0])
    f2 = chr2size/float(chromsizes[1])

    mtrx = np.zeros((chr1size, chr2size), dtype=bool)

    fam2atoms = dict()
    for chrx, start, end, orient, family in atoms:
        if family not in fam2atoms:
            fam2atoms[family] = list()
        fam2atoms[family].append((chrx, start, end, orient))

    for fam, members in list(fam2atoms.items()):
        for (chrx, startx, endx, orientx), (chry, starty, endy, orienty) in \
                combinations(members, 2):
            if chrx == chry:
                continue
            if chrx != chromnames[0]:
                (chry, starty, endy, orienty),(chrx, startx, endx, orientx) = \
                        (chrx, startx, endx, orientx), (chry, starty, endy, \
                        orienty)
            sx = int(startx * f1)
            sy = int(starty * f2)
            ex = int(endx * f1)
            ey = int(endy * f2)
            for i, j in enumerate(np.linspace(0, ex-sx+1, ey-sy+1, dtype=int)):
                if orientx == orienty:
                    mtrx[sx+i-1,sy+j-1] = 1
                else:
                    mtrx[sx+i-1,sy-j-1] = 1

    plt.figure()
    plt.imshow(mtrx, cmap='binary', origin='lower')
    plt.title('%s vs %s' %tuple(chromnames))
    plt.xlabel(chromnames[1])
    plt.ylabel(chromnames[0])
    #plt.show()
    plt.savefig(stdout, format='pdf')


