#!/usr/bin/env python3
# Reads from two files tables in the format bellow with shared chromosome content and prints the average difference

# Input example:
# 1	6	68.70
# 1	7	31.30
# 2	13	23.95
# 2	15	20.43
# 2	16	17.50
# 2	3	17.40
# 2	14	15.25
# 2	10	5.47
# 3	5	100.00
# 4	17	60.87
# 4	9	39.13
# 5	19	73.23
# 5	12	26.77
# 6	8	53.90
# 6	11	46.10
# 7	14	51.69
# 7	7	25.06
# 7	4	23.25
# 8	2	60.57
# 8	4	39.43
# 9	10	39.29
# 9	3	23.47
# 9	1	18.88
# 9	4	13.27
# 9	13	5.10
# 10	18	100.00
# 11	1	100.00

import sys
import argparse
from collections import defaultdict
from statistics import mean, stdev


parser = argparse.ArgumentParser(description='Reads from two files tsv tables with shared chromosome content and prints the average difference.',
                                 formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('file1', metavar='file1.tsv', type=argparse.FileType('r'),
                    help='first tsv file.')

parser.add_argument('file2', metavar='file2.tsv', type=argparse.FileType('r'),
                    help='second tsv file.')

args = parser.parse_args()


xy1 = defaultdict(dict)
for line in args.file1:
    if line.startswith("#"):
        continue
    x, y, v = [ eval(item) for item in line.split() ]
    xy1[x][y] = v

xy2 = defaultdict(dict)
for line in args.file2:
    if line.startswith("#"):
        continue
    x, y, v = [ eval(item) for item in line.split() ]
    xy2[x][y] = v

maxx = max(xy1.keys())
maxy = max([ max(xy1[x].keys()) for x in xy1.keys() ])
maxx = max(maxx, max(xy2.keys()))
maxy = max(maxy, max([ max(xy2[x].keys()) for x in xy2.keys() ]))

for x in range(1,maxx+1):
    for y in range(1,maxy+1):
        if y not in xy1[x]:
            xy1[x][y] = 0
        if y not in xy2[x]:
            xy2[x][y] = 0

diffs = []
for x in range(1,maxx+1):
    for y in range(1,maxy+1):
        p1 = xy1[x][y]
        p2 = xy2[x][y]
        if p1 != 0 or p2 != 0:
            diffs.append(abs(p1-p2))

print("Average difference = %.1f" % mean(diffs))
print("Standard deviation = %.1f" % stdev(diffs))

exit(0)
