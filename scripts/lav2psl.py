#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Reads LAV (containing alignment for a *pair* of sequences) from stdin and writes PSL to stdout
# LAV format: https://www.bx.psu.edu/miller_lab/dist/lav_format.html
# PSL format: https://www.ensembl.org/info/website/upload/psl.html


import argparse
import sys
import decimal

class Block:
    def __init__(self, s1start, s2start, s1end, s2end, identity):
        self.s1start = int(s1start) - 1 # convert from 1-based [start,end] to 0-based [start,end)
        self.s2start = int(s2start) - 1
        self.s1end = int(s1end)
        self.s2end = int(s2end)
        self.identity = int(identity)

    def __repr__(self):
        return "%d-%d:%d-%d(%d)" % (
            self.s1start, self.s1end, self.s2start, self.s2end, self.identity)

    def __len__(self):
        return self.s1end - self.s1start

    def matches(self):
        # Python 3.x rounds .5 values to a neighbour which is even
        # ("Banker's rounding") producing different results from
        # lavToPsl (at least in current implementations of C
        # round. This code produces the same result as in C code, so
        # we can check this script correctness compared to lavToPsl
        # (we could also add, say, 0.00000000001 to the result before
        # rounding). Notice that even so, some differences can arise
        # due to float precision.
        return int(decimal.Decimal(len(self) * self.identity / 100.0).quantize(
            decimal.Decimal('1'), rounding=decimal.ROUND_HALF_UP))
        #return round(len(self) * self.identity / 100.0)

    def misMatches(self):
        return len(self) - self.matches()

class Alignment:
    def __init__(self, score, s1start, s2start, s1end, s2end):
        self.score = int(score)
        self.s1start = int(s1start) - 1 # convert from 1-based [start,end] to 0-based [start,end)
        self.s2start = int(s2start) - 1
        self.s1end = int(s1end)
        self.s2end = int(s2end)
        self.blocks = []
        
    def __repr__(self):
        return "%d-%d:%d-%d(%d)" % (
            self.s1start, self.s1end, self.s2start, self.s2end, self.score) + \
            " " + repr(self.blocks)

    def addBlock(self, block):
        self.blocks.append(block)

    # matches - Number of matching bases that aren't repeats
    def matches(self):
        return sum(b.matches() for b in self.blocks)

    # misMatches - Number of bases that don't match
    def misMatches(self):
        return sum(b.misMatches() for b in self.blocks)

    # repMatches - Number of matching bases that are part of repeats
    def repMatches(self):
        return 0
    
    # nCount - Number of 'N' bases
    def nCount(self):
        return 0

    # qNumInsert - Number of inserts in query (query is sequence 2)
    def qNumInsert(self):
        inserts = 0
        for i in range(1, len(self.blocks)):
            if self.blocks[i].s2start > self.blocks[i-1].s2end:
                inserts += 1
        return inserts

    # qBaseInsert - Number of bases inserted into query
    def qBaseInsert(self):
        bases = 0
        for i in range(1, len(self.blocks)):
            bases += self.blocks[i].s2start - self.blocks[i-1].s2end # 0 if there is no base insert
        return bases

    # tNumInsert - Number of inserts in target (target is sequence 1)
    def tNumInsert(self):
        inserts = 0
        for i in range(1, len(self.blocks)):
            if self.blocks[i].s1start > self.blocks[i-1].s1end:
                inserts += 1
        return inserts

    # tBaseInsert - Number of bases inserted into target
    def tBaseInsert(self):
        bases = 0
        for i in range(1, len(self.blocks)):
            bases += self.blocks[i].s1start - self.blocks[i-1].s1end # 0 if there is no base insert
        return bases

    # blockCount - Number of blocks in the alignment
    def blockCount(self):
        return len(self.blocks)
    
    # blockSizes - List of sizes of each block
    def blockSizes(self):
        return [ b.s1end - b.s1start for b in self.blocks ]
    
    # qStarts - List of fixed start position of each block in query,
    # if strand is "-" this coordinates are still relative to the end,
    # unlike qStart and qEnd values
    def qStarts(self, strand, qRangeStart, qRangeEnd, qChrSize):
        if strand == "+":
            return [ qRangeStart + b.s2start for b in self.blocks ]
        else:
            # was the distance to qRangeEnd, now is to qChrSize
            return [ b.s2start + qChrSize - qRangeEnd for b in self.blocks ]
    
    # tStarts - List of fixed start position of each block in target
    def tStarts(self, tRangeStart):
        return [ tRangeStart + b.s1start for b in self.blocks ]

    
lnum = 0
def readline_or_eof(start=None):
    global lnum
    lnum += 1
    line = sys.stdin.readline()
    if line:
        if start and not line.startswith(start):
            Exception('Malformed file, line %d should start with "%s".' % (lnum, start))
            line = line.strip()
    return line

def readline(start=None):
    global lnum
    lnum += 1
    line = sys.stdin.readline()
    if line:
        if start and not line.startswith(start):
            Exception('Malformed file, line %d should start with "%s".' % (lnum, start))
        line = line.strip()
    else:
        raise Exception('Malformed file, premature EOF.')
    return line

def skip_stanza():
    line = readline()
    while not line.startswith("}"):
        line = readline()
    return None

def parse_d():
    stanza = {"type":"d", "data":[]}
    
    line = readline().strip('"').split()
    align = line[0]
    args = line[3:]
    stanza["data"].append("aligner=%s %s " % (align, "  ".join(args)))
    
    header = readline().split() # should be table header
    table = []
    for i in range(len(header)):
        table.extend(readline().split())
    stanza["data"].append("matrix=%s %d %s" % (align, len(table), ",".join(table)))
        
    params = [ p.replace(" ", "") for p in readline().strip('"').split(",") ]
    stanza["data"].append("gapPenalties=%s" % " ".join([align] + [p for p in params if p.startswith("O=") or p.startswith("E=")]))
    stanza["data"].append("blastzParms=%s" % ",".join(params))
    
    line = readline()
    while not line.startswith("}"): # just in case, but we don't expect more lines
        stanza["data"].append(line)
        line = readline()

    return stanza

def parse_s():
    stanza = {"type":"s"}
    
    line = readline().split()
    line[0] = line[0].strip('"')
    strand = "-" if line[0].endswith("-") or line[3] == "1" else "+"
    fname = line[0][:-1] if line[0].endswith("-") else line[0]
    # converts start from from 1-based [start,end] to 0-based [start,end)
    stanza["s1"] = {"fname":fname, "start":int(line[1]) - 1, "end":int(line[2]), "strand":strand, "seqnum":int(line[4])}

    line = readline().split()
    line[0] = line[0].strip('"')
    strand = "-" if line[0].endswith("-") or line[3] == "1" else "+"
    fname = line[0][:-1] if line[0].endswith("-") else line[0]
    stanza["s2"] = {"fname":fname, "start":int(line[1]) - 1, "end":int(line[2]), "strand":strand, "seqnum":int(line[4])}
    
    line = readline()
    if not line.startswith("}"):
        raise Exception('Malformed file, S stanza with more than one pair of sequences.')

    return stanza

def parse_h():
    stanza = {"type":"h"}

    line = [ v.strip('"') for v in readline().strip('"').split(maxsplit=1) ]
    name = line[0][1:]
    descr = "" if len(line) == 1 else line[1]
    stanza["s1"] = {"name":name, "descr":descr}

    line = [ v.strip('"') for v in readline().strip('"').split(maxsplit=1) ]
    name = line[0][1:]
    descr = "" if len(line) == 1 else line[1]
    stanza["s2"] = {"name":name, "descr":descr}
    
    line = readline()
    if not line.startswith("}"):
        raise Exception('Malformed file, H stanza with missing "}" after two lines.')

    return stanza

def parse_a():
    stanza = {"type":"a"}

    line = readline("s").split() # should be s entry
    score = line[1]
    
    line = readline("b").split() # should be b entry
    s1start = line[1]
    s2start = line[2]

    line = readline("e").split() # should be e entry
    s1end = line[1]
    s2end = line[2]

    align = Alignment(score, s1start, s2start, s1end, s2end)

    line = readline()
    while not line.startswith("}"):
        if not line.startswith("l"):
            Exception('Malformed file, line %d should start with "l".' % lnum)

        line = line.split()
        align.addBlock(Block(*line[1:]))
            
        line = readline()

    stanza["align"] = align
    return stanza

def parse_x():
    return skip_stanza()

def parse_m():
    return skip_stanza()

def parse_Census():
    return skip_stanza()


EPILOG="""\
X, M and Census stanzas are ignored in the LAV file.

LAV format:
  https://www.bx.psu.edu/miller_lab/dist/lav_format.html

PSL format:
  https://www.ensembl.org/info/website/upload/psl.html

LASTZ (which produces the LAV files) takes 2 sequences (or 2 slices or
ranges of sequences), the first one is called *target* in PSL and its
corresponding columns appear latter in PSL columns order, and the
second one is called *query* and its corresponding columns appear
first in PSL columns order.

The --fix-psl-coordinates option fix coordinates in the output PSL
file. This is because LASTZ aligns slices of the original chromosomes
but the way this is dealt with give rise to some inconsistencies. Read
the whole story below.

When --fix-psl-coordinates is used, two values in the LAV file (which
are ignored when --dont-fix-psl-coordinates is used) are taking into
account:
* tRangeStart: start of sequence 1 slice
* qRangeStart: start of sequence 2 slice
where tRangeStart is the start position of the target sequence range
(given first to LASTZ, appears latter in the columns of PSL format),
qRangeStart is the start position of the query sequence range (given
last to LASTZ, appears first in the columns of PSL format).

Also, when --fix-psl-coordinates is used it must be followed by 2
mandatory arguments we need to correct coordinates:
 * tChrSize: size of chromosome 1
 * qChrSize: size of chromosome 2
where tChrSize and qChrSize are the sizes of the whole chromosomes
related to tRangeStart and qRangeStart, respectively. Positions
tRangeStart and qRangeStart are in 1-based [start,end] format in LAV
files but are converted to 0-based [start,end) in PSL files.

Note: all ranges/positions in LAV files are 1-based [start,end] and in
      PSL files are 0-based [start,end)


== About PSL coordinates inconsistencies ==

Inconsistencies happen when LASTZ computes alignments for subranges of
original sequences, because the original chromosome sizes are not
explicitly nor implicitly in the data. Besides, the original UCSC
Genome Browser's lavToPsl ignores the range starts (tRangeStart or
qRangeStart) given in LAV files. When LASTZ computes alignments for,
say, the subrange [1001,3000], lavToPsl doesn't take into account the
whole size of original sequence and the range start, then it thinks
the range is [0,3000) instead of [1000,3000). Thus, when the second
subsequence (query) is in the positive strand the aligment start
position is counted from 0, but when it is in the negative strand the
start is counted back from the end (3000 for the example). Also, for
the negative strand case, the positions in the list "qStarts" are the
distances relative to the end. The coordinates from start must
consider the range start (tRangeStart or qRangeStart) and coordinates
relative to the end must consider the end of the whole chromosome
(tChrSize or qChrSize), not only the slice/range LASTZ used for
finding alignments.


== List of PSL columns ==

matches - Number of bases that match that aren't repeats
misMatches - Number of bases that don't match
repMatches - Number of bases that match but are part of repeats
nCount - Number of "N" bases
qNumInsert - Number of inserts in query
qBaseInsert - Number of bases inserted in query
tNumInsert - Number of inserts in target
tBaseInsert - Number of bases inserted in target
strand - "+" or "-" for query strand. For translated alignments, second
    "+" or "-" is for target genomic strand.
qName - Query sequence name
qSize - Query sequence size.
qStart - Alignment start position in query
qEnd - Alignment end position in query
tName - Target sequence name
tSize - Target sequence size
tStart - Alignment start position in target
tEnd - Alignment end position in target
blockCount - Number of blocks in the alignment (a block contains no gaps)
blockSizes - Comma-separated list of sizes of each block. If the query is a
    protein and the target the genome, blockSizes are in amino acids.
qStarts - Comma-separated list of starting positions of each block in query
tStarts - Comma-separated list of starting positions of each block in target


== Example of how coordinates work in PSL files ==

Here is a 61-mer containing 2 blocks that align on the minus strand and 2 blocks
that align on the plus strand (this sometimes happens due to assembly errors):


0         1         2         3         4         5         6 tens position in query
0123456789012345678901234567890123456789012345678901234567890 ones position in query
                      ++++++++++++++                    +++++ plus strand alignment on query
    ------------------              --------------------      minus strand alignment on query
0987654321098765432109876543210987654321098765432109876543210 ones position in query negative strand coordinates
6         5         4         3         2         1         0 tens position in query negative strand coordinates

Plus strand:
     qStart=22
     qEnd=61
     blockSizes=14,5
     qStarts=22,56

Minus strand:
     qStart=4
     qEnd=56
     blockSizes=20,18
     qStarts=5,39

"""

parser = argparse.ArgumentParser(description='Reads LAV (containing alignment for a *pair* of sequences)\nfrom stdin and writes PSL to stdout.',
                                 formatter_class=argparse.RawDescriptionHelpFormatter,
                                 epilog=EPILOG)

fixgroup = parser.add_mutually_exclusive_group(required=True)
fixgroup.add_argument('--dont-fix-psl-coordinates', dest='fix', action='store_false',
                      help="convert as UCSC Genome Browser's lavToPsl would do (read epilog)")
fixgroup.add_argument('--fix-psl-coordinates', dest='fix', type=int, metavar="<int>", nargs=2,
                      help="convert fixing coordinates based on the 2 given chromosome sizes (read epilog)")

args = parser.parse_args()
if args.fix:
    tChrSize, qChrSize = args.fix
else:
    tChrSize = qChrSize = None
fix = args.fix != False

line = readline_or_eof()

while line:
    # comment
    if line.startswith("#"):
        pass;
    
    # empty
    elif line.strip() == "":
        pass;

    # D Stanza
    elif line.startswith("d {"):
        d = parse_d()
        for l in d["data"]:
            print("##%s" % l)

    # S Stanza
    elif line.startswith("s {"):
        s = parse_s()

    # H Stanza
    elif line.startswith("h {"):
        h = parse_h()

    # A Stanza
    elif line.startswith("a {"):
        a = parse_a()
        al = a["align"]

        # matches - Number of matching bases that aren't repeats
        # misMatches - Number of bases that don't match
        # repMatches - Number of matching bases that are part of repeats
        # nCount - Number of 'N' bases
        # qNumInsert - Number of inserts in query
        # qBaseInsert - Number of bases inserted into query
        # tNumInsert - Number of inserts in target
        # tBaseInsert - Number of bases inserted into target
        
        # strand - Defined as + (forward) or - (reverse) for query strand
        # qName - Query sequence name
        # qSize - Query sequence size
        # qStart - Alignment start position in query
        # qEnd - Alignment end position in query
        # tName - Target sequence name
        # tSize - Target sequence size
        # tStart - Alignment start position in target
        # tEnd - Alignment end position in target

        strand = s["s2"]["strand"]
        
        qName = h["s2"]["name"]
        qRangeStart = s["s2"]["start"] if fix else 0
        qRangeEnd = s["s2"]["end"]
        qSize = qChrSize if fix else qRangeEnd
        qStart = al.s2start
        qEnd = al.s2end
        if not fix: qChrSize = qSize # to simplify, fixing steps will change no value in this case

        if strand == "+": # if "+" strand just shift
            qStart += qRangeStart # qRangeStart = 0 if --dont-fix-psl-coordinates
            qEnd += qRangeStart
        else: # if "-" strand convert qStart and qEnd from relative to the end to relative to the beginning
            qStart, qEnd = qRangeEnd - qEnd, qRangeEnd - qStart
        
        tName = h["s1"]["name"]
        tRangeStart = s["s1"]["start"] if fix else 0
        tRangeEnd = s["s1"]["end"]
        tSize = tChrSize if fix else tRangeEnd
        tStart = al.s1start
        tEnd = al.s1end
        if not fix: tChrSize = tSize # to simplify, fixing steps will change no value in this case

        tStart += tRangeStart # tRangeStart = 0 if --dont-fix-psl-coordinates
        tEnd += tRangeStart

        # blockCount - Number of blocks in the alignment
        # blockSizes - Comma-separated list of sizes of each block
        # qStarts - Comma-separated list of start position of each block in query
        # tStarts - Comma-separated list of start position of each block in target

        blockSizes = ",".join(str(s) for s in al.blockSizes()) + ","
        qStarts = ",".join(str(s) for s in al.qStarts(strand, qRangeStart, qRangeEnd, qChrSize)) + ","
        tStarts = ",".join(str(s) for s in al.tStarts(tRangeStart)) + ","

        print("%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%s\t%s\t%d\t%d\t%d\t%s\t%d\t%d\t%d\t%d\t%s\t%s\t%s" % (
            al.matches(),
            al.misMatches(),
            al.repMatches(),
            al.nCount(),
            al.qNumInsert(),
            al.qBaseInsert(),
            al.tNumInsert(),
            al.tBaseInsert(),
            strand,
            qName,
            qSize,
            qStart,
            qEnd,
            tName,
            tSize,
            tStart,
            tEnd,
            al.blockCount(),
            blockSizes,
            qStarts,
            tStarts
        ))

    # X Stanza
    elif line.startswith("x {"):
        x = parse_x()

    # M Stanza
    elif line.startswith("m {"):
        m = parse_m()

    # Census Stanza
    elif line.startswith("Census {"):
        census = parse_Census()

    else:
        raise Exception('Malformed file, unexpected value at line %d.' % lnum)

    line = readline_or_eof()
    
    
#stdin = sys.stdin; sys.stdin = open('/dev/tty'); import pdb; pdb.set_trace();
#sys.stdin = stdin;
    
exit(0)
