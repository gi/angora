#!/usr/bin/env python3

from sys import stdin, argv
import os

# GLOBAL VARIABLES -----------------------------------------------------------
HELP = """\
This scripts converts local to global coordinates/positions/ranges taking into
account inconsistencies arisen in psl files converted by lavToPsl from lav
files given by lastz. Inconsistencies happen when lastz computes alignments for
subranges of original sequences, because lavToPsl doesn't know about this
subrange calculation of lastz. When lastz computes alignments for, say, the
subrange [1001,3000], lavToPsl doesn't take into account the whole size of
original sequence and the range start, then it thinks the range is [0,3000)
instead of [1000,3000). Thus, when the second subsequence (query) is in the
positive strand the aligment start position is counted from 0, but when it is
in the negative strand the start is counted back from the end (3000 for the
example). Also, for the negative strand case, the positions in the list
"qStarts" are the distances relative to the end.

Note 1: ranges/positions in lav files are 1-based [start,end] and in psl files
are 0-based [start,end).

Note 2: lastz takes 2 sequences, the first one is called target in psl and its
corresponding columns appear latter in psl columns order, and the second one is
called query and its corresponding columns appear first in psl columns order.

List of psl columns:

    matches - Number of bases that match that aren't repeats
    misMatches - Number of bases that don't match
    repMatches - Number of bases that match but are part of repeats
    nCount - Number of "N" bases
    qNumInsert - Number of inserts in query
    qBaseInsert - Number of bases inserted in query
    tNumInsert - Number of inserts in target
    tBaseInsert - Number of bases inserted in target
    strand - "+" or "-" for query strand. For translated alignments, second
        "+" or "-" is for target genomic strand.
    qName - Query sequence name
    qSize - Query sequence size.
    qStart - Alignment start position in query
    qEnd - Alignment end position in query
    tName - Target sequence name
    tSize - Target sequence size
    tStart - Alignment start position in target
    tEnd - Alignment end position in target
    blockCount - Number of blocks in the alignment (a block contains no gaps)
    blockSizes - Comma-separated list of sizes of each block. If the query is a
        protein and the target the genome, blockSizes are in amino acids.
    qStarts - Comma-separated list of starting positions of each block in query
    tStarts - Comma-separated list of starting positions of each block in target

Example of how coordinates work in psl files:

Here is a 61-mer containing 2 blocks that align on the minus strand and 2 blocks
that align on the plus strand (this sometimes happens due to assembly errors):
"""
EXAMPLE = """\
0         1         2         3         4         5         6 tens position in query
0123456789012345678901234567890123456789012345678901234567890 ones position in query
                      ++++++++++++++                    +++++ plus strand alignment on query
    ------------------              --------------------      minus strand alignment on query
0987654321098765432109876543210987654321098765432109876543210 ones position in query negative strand coordinates
6         5         4         3         2         1         0 tens position in query negative strand coordinates

Plus strand:
     qStart=22
     qEnd=61
     blockSizes=14,5
     qStarts=22,56

Minus strand:
     qStart=4
     qEnd=56
     blockSizes=20,18
     qStarts=5,39
"""
USAGE = """\
Usage:

\t%s [start_1] [chromosome_size_1] [start_2] [chromosome_size_2]

Where start_1 is the start position of the target sequence range (given first
to lastz, appears latter in the columns of psl format), start_2 is the start
position of the query sequence range (given last to lastz, appears first in the
columns of psl format), and chromosome_size_1 and chromosome_size_2 are the
sizes of the whole chromosomes related to start_1 and start2, respectively.
Positions start_1 and start_2 must be in 1-based [start,end] format.

This script reads the psl file from stdin and writes to stdout.
""" % os.path.basename(argv[0])


# GLOBAL FUNCTIONS -----------------------------------------------------------

def parse_fields_q(fields):
    return parse_fields(fields, False)

def parse_fields_t(fields):
    return parse_fields(fields, True)

def parse_fields(fields, is_target):
    pos = (10, 11, 12, 19, 8)
    if is_target:
        pos = (14, 15, 16, 20)
    rangeEnd = int(fields[pos[0]])
    start = int(fields[pos[1]])
    end = int(fields[pos[2]])
    blocksStarts = [ int(s) for s in fields[pos[3]].rstrip(",").split(",") ]
    if is_target:
        return (rangeEnd, start, end, blocksStarts)
    else:
        strand = fields[pos[4]]
        return (rangeEnd, start, end, blocksStarts, strand)

def update_fields_q(fields, qChrSize, qStart, qEnd, qBlocksStarts):
    update_fields(fields, qChrSize, qStart, qEnd, qBlocksStarts, False)

def update_fields_t(fields, tChrSize, tStart, tEnd, tBlocksStarts):
    update_fields(fields, tChrSize, tStart, tEnd, tBlocksStarts, True)

def update_fields(fields, chrSize, start, end, blocksStarts, is_target):
    pos = (10, 11, 12, 19)
    if is_target:
        pos = (14, 15, 16, 20)
    fields[pos[0]] = str(chrSize)
    fields[pos[1]] = str(start)
    fields[pos[2]] = str(end)
    fields[pos[3]] = ",".join(str(b) for b in blocksStarts) + ","


# MAIN -----------------------------------------------------------------------

if __name__ == '__main__':

    if len(argv) != 5:
        print(HELP)
        print(EXAMPLE)
        print(USAGE)
        exit(1)


# Important variables:
#
# Global:
# tChrSize # original chromosome size
# tRangeStart # range considered by lastz in the chromosome
# tRangeEnd
#
# For each line/alignment:
# tStart # (whole) aligment range
# tEnd
# tBlocksStarts # starts of blocks aligned in the whole alignment
#
# And the same for query sequence, plus strand:
# qChrSize
# qRangeStart
# qRangeEnd
#
# qStart
# qEnd
# qBlocksStarts
#
# qStrand

    tRangeStart = int(argv[1]) - 1
    tChrSize = int(argv[2])
    qRangeStart = int(argv[3]) - 1
    qChrSize = int(argv[4])

    lines = stdin.readlines()

    for l in lines:
        if l.lstrip().startswith("#") or len(l.strip()) == 0: # comment or empty line
            print(l[:-1]) # l has \n
            continue

        fields = l.split()

        qRangeEnd, qStart, qEnd, qBlocksStarts, qStrand = parse_fields_q(fields)
        tRangeEnd, tStart, tEnd, tBlocksStarts = parse_fields_t(fields)

        if (qStrand == "+"):
            qStart += qRangeStart
            qEnd += qRangeStart
            for i in range(len(qBlocksStarts)):
                qBlocksStarts[i] += qRangeStart
        else:
            # strand can be "-" just for query, and in this case qStart/qEnd are already ok
            for i in range(len(qBlocksStarts)):
                qBlocksStarts[i] += qChrSize - qRangeEnd # was the distance to qRangeEnd, now is to qChrSize

        tStart += tRangeStart
        tEnd += tRangeStart
        for i in range(len(tBlocksStarts)):
            tBlocksStarts[i] += tRangeStart

        update_fields_q(fields, qChrSize, qStart, qEnd, qBlocksStarts)
        update_fields_t(fields, tChrSize, tStart, tEnd, tBlocksStarts)
        print("\t".join(fields))

