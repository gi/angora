#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Reads markers from file passed as argument (ANGES markers format)
# and prints some statistics


import argparse
from collections import Counter, defaultdict
import statistics
import math
from numpy import histogram

try:
    import matplotlib.pyplot as plt
except ImportError:
    plt = None

#DEFAULT_HISTOGRAM_BINS = "20"
DEFAULT_HISTOGRAM_BINS = "[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]"
DEFAULT_HISTOGRAM_PERC = 0.95

class MarkersStatistics:

    def __init__(self, histogram_cut, histogram_bins, plot):
        self.histogram_cut = histogram_cut
        self.histogram_bins = histogram_bins
        self.plot = plot and plt
        self.figures = 0

    # Statistics for some data (prints and generate figures)
    def statistics(self, name, data):
        data.sort()

        print("Statistics for %s:" % name)
        self._pprint("Number", len(data))
        self._pprint("Min", min(data))
        self._pprint("Max", max(data))
        self._pprint("Mean", statistics.mean(data))
        for p in [0.5, 0.75, 0.9, 0.99]:
            self._pprint("Percentile %d%%" % int(p*100), int(self._percentile(data, p)))
        hdata = data[:self._percentile_pos_low(data, self.histogram_cut)+1]

        #import pdb
        #pdb.set_trace()
        hist, bin_edges = histogram(hdata, bins=self.histogram_bins)
        self._pprint("Histogram in percentile %d%%" % int(self.histogram_cut*100))
        for i in range(len(hist)):
            self._pprint("[%s,%s%s" % (self._float2str(bin_edges[i]), self._float2str(bin_edges[i+1]),
                                 "]" if i == len(hist)-1 else ")"),
                   "%d (%s%%)" % (hist[i], self._float2str(100*hist[i]/len(hdata))),
                   indentation=2)
        if plot:
            self.figures += 1
            plt.figure(self.figures)
            #import pdb
            #pdb.set_trace()
            plt.hist(hdata, bins=self.histogram_bins)
            plt.title("Histogram for %s in percentile %d%%" % (name, int(self.histogram_cut*100)))
        print()

    # Show figures generated so far (blocking)
    def show_figures(self):
        if plot:
            plt.show()
    
    # Pretty print
    def _pprint(self, name, value = None, indentation = 1):
        if isinstance(value, float):
            value_str = "%.1f" % value
        elif isinstance(value, int):
            value_str = "%d" % value
        elif value is None:
            value_str = ""
        else:
            value_str = str(value)

        indentstr = "    "
        print("%s%-15s %s" % (indentstr*indentation, name+":", value_str))

    # Returns a formated string without trailing zeros
    def _float2str(self, value):
        return ("%.1f" % value).rstrip('0').rstrip('.')

    # Returns the position of percentile
    def _percentile_pos_low(self, data, percent):
        return math.floor((len(data)-1) * percent)

    # Calculates percentile (based on http://code.activestate.com/recipes/511478/)
    # Data must be sorted
    def _percentile(self, data, percent): 
        if not data:
            return None
        k = (len(data)-1) * percent
        f = math.floor(k)
        c = math.ceil(k)
        if f == c:
            return data[int(k)]
        d0 = data[int(f)] * (c-k)
        d1 = data[int(c)] * (k-f)
        return d0+d1


EPILOG="""
Note 1: Percentile 𝑝% shows a value 𝑣 for which 𝑝% of data is ≤ 𝑣,
e.g. Percentile 75%: 21 means 75% of data is ≤ 21.

Note 2: Histogram in percentile 𝑝% shows, for the 𝑝% smaller values in
the data, equal-width ranges and the numbers of elements in the data
that fit that ranges.

Note 3: The parameter hbins can be a number or a method among auto,
fd, doane, scott, stone, rice, sturges or sqrt. See
<https://docs.scipy.org/doc/numpy/reference/generated/
 numpy.histogram_bin_edges.html#numpy.histogram_bin_edges>
for details on methods.
"""

parser = argparse.ArgumentParser(description='Reads markers from file passed as argument (ANGES markers format)\nand prints some statistics.',
                                 epilog=EPILOG, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('markers', metavar='file', type=argparse.FileType('r'),
                    help='contains markers in ANGES marker file format.')
parser.add_argument('--hcut', metavar='float', type=float, default=DEFAULT_HISTOGRAM_PERC,
                    help='compute histogram for percentile (default: %g)' % DEFAULT_HISTOGRAM_PERC)
parser.add_argument('--hbins', metavar='value', type=str, default=DEFAULT_HISTOGRAM_BINS,
                    help='number of histogram bins/ranges, method or list of values (default: %s)' % DEFAULT_HISTOGRAM_BINS)
parser.add_argument('--ignore-singletons', '-i', action='store_true',
                    help='ignore families occurring in only one genome')
parser.add_argument('--plot', '-p', action='store_true',
                    help='show histogram plot (if matplotlib.pyplot is available)')

args = parser.parse_args()

plot = args.plot
hcut = args.hcut

if args.hbins.isdigit():
    hbins = int(args.hbins)
else:
    try: # try to check if its a list...
        hbins = eval(args.hbins)
    except:
        hbins = args.hbins

line = args.markers.readline()
markers_sp = defaultdict(Counter) # counts for each species how many times the markers appear
markers = defaultdict(set) # stores for each marker in which species it appears
marker = ""
while line:
    if not line.strip(): # empty line
        pass
    elif line.startswith(">"): # new marker
        marker = int(line.split(maxsplit=1)[0].replace(">",""))
    else: # occurrence of marker
        species = line.split(".", maxsplit=1)[0]
        markers_sp[species][marker] += 1
        markers[marker].add(species)
    line = args.markers.readline()

if args.ignore_singletons:
    for marker in list(markers.keys()):
        if len(markers[marker]) == 1:
            sp = markers[marker].pop()
            del markers[marker]
            del markers_sp[sp][marker]

stat = MarkersStatistics(hcut, hbins, plot)
for species in sorted(markers_sp.keys()):
    families = markers_sp[species]
    stat.statistics(species + " families", list(families.values()))

coverage = [ len(spset) for spset in markers.values() ]
stat.statistics("families coverage", coverage)
    
stat.show_figures()

exit(0)
