#!/bin/bash
# Script for running/testing lastz with given parameters
# and save both the plot of results AND the parameters.
# Appends a number to the end of output files for saving
# multiple runs (to compare results of different parameters).
set -e

LASTZ="/prj/gsp/tools/align2/lastz"
RPLOT="/prj/gsp/eudicot_study/analysis/scripts/rdotplot.R"
PREFIX="dotplot"
LOGEXT="log"
EXT="pdf"


if [ $# -eq 0 ]
then
    echo "Missing parameters!"
    exit 1
fi

lastn="$(ls | grep "$PREFIX[0-9]*.$EXT" | sort -V | tail -1 | sed "s/^$PREFIX\|.$EXT$//g")"
n=$((lastn + 1)) # works too if lastn is empty, then n will be 1
logfile="$PREFIX$n.$LOGEXT"
outfile="$PREFIX$n.$EXT"

echo "Parameters: $@" > "$logfile"
echo "Outputs: $logfile $outfile"
"$LASTZ" $@ 2>> "$logfile" | "$RPLOT" "$outfile" "$@" 2>> "$logfile" > "$outfile"
retcodelastz="${PIPESTATUS[0]}"
exit $retcodelastz

