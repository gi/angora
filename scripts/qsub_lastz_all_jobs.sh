#!/bin/bash
# Script that submits a job array to qsub (of lastz jobs)
# Doesn't submit jobs to recompute files that exist and are not empty.

LANG=C
MEMORY=8 # in GB
PROJECT="gcdcj"
EXCLUDE_HOSTS=-l\ h='!cuda*'

# this script should be in scripts subdir of the workflow somewhere in the system
scriptdir="$(dirname "$0")"
workflowdir="$(realpath "$scriptdir/..")"

# Path to script that runs one job
JOB_CMD="$workflowdir/scripts/qsub_lastz_one_job.sh"

# Path to script that generates the list of commands to be submited as jobs
LIST_CMD="$workflowdir/scripts/snakemake_cmds.py"
LIST_PARAMS=("--run_lastz")

# Where list of commands (one for each job) will be stored
cmdfile="$workflowdir/logs/$(basename "${0%.sh}")_$(date +%F_%H_%M_%S).cmdlist"

# Log
logfile="$workflowdir/logs/$(basename "${0%.sh}")_$(date +%F_%H_%M_%S).log"

# transform to full paths
JOB_CMD="$(realpath "$JOB_CMD")"
LIST_CMD="$(realpath "$LIST_CMD")"
cmdfile="$(realpath -m "$cmdfile")"
logfile="$(realpath -m "$logfile")"


# Exit the script if any command returns error
set -e

usage() {
  echo "Submits the lastz job array to qsub."
  echo "Doesn't submit jobs to recompute files that exist and are not empty"
  echo "(snakemake tells which ones must be computed)."
  echo "Usage:"
  echo -e "\t$(basename "$0") [snakefile]"
}


if [ $# -ne 1 ]
then
  >&2 echo -e "Missing argument!\n"
  usage
  exit 1
fi


error() {
  local sourcefile=$1
  local lineno=$2
  >&2 echo "ERROR code returned by command at line $lineno"
}
trap 'error "${BASH_SOURCE}" "${LINENO}"' ERR



# Empty log file
> "$logfile"

snakefile="$(realpath "$1")" # transform to full path
if [ ! -f "$snakefile" ]
then
  >&2 echo -e "Invalid snakefile: \"$snakefile\"\n"
  usage
  exit 1
fi

workdir="$(dirname "$snakefile")"

echo "Generating list of commands to file \"$cmdfile\"."
echo "Generating list of commands to file \"$cmdfile\"." >> "$logfile"
"$LIST_CMD" ${LIST_PARAMS[@]} "$snakefile" > "$cmdfile"
n="$(wc -l < "$cmdfile")"

echo "Submitting job array with $n jobs from file \"$cmdfile\", workdir \"$workdir\"."
echo "Submitting job array with $n jobs from file \"$cmdfile\", workdir \"$workdir\"." >> "$logfile"
qsub -t 1-$n -o "$logfile" -cwd -V -j y -l mem_free=${MEMORY}G,h_vmem=${MEMORY}G -l arch=lx-amd64 -l idle=1 $EXCLUDE_HOSTS -P "$PROJECT" -b y "$JOB_CMD" "$cmdfile" "$logfile" "$workdir"
ret=$?

sleep 2
qstat

exit $ret
