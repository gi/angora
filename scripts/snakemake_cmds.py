#!/usr/bin/env python3
# Lists all shell commands that must be executed for one of the rules in the given snakefile.

import sys
from SnakemakeUtils import SnakemakeShellCmds
import argparse

parser = argparse.ArgumentParser(description='Lists all shell commands that must be executed for one of the rules in the given snakefile.',
                                 epilog='Works only for rules supported by this script (rules that we know how to list the targets).',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('--force', '-f', action='store_true',
                    help='list commands of existing non-empty files')
parser.add_argument('--no-dirs', '-n', action='store_true',
                    help='do not create the directory tree needed for storing the target files')

group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--run_lastz', action='store_true', help="For run_lastz rule")
group.add_argument('--atomizer', action='store_true', help="For atomizer rule")

parser.add_argument('Snakefile', type=str, help="Path to the snakefile containing the rules")

args = parser.parse_args()

snakefile = args.Snakefile
if SnakemakeShellCmds.is_empty_file(snakefile):
    print("Invalid snakefile: \"%s\"" % snakefile)
    exit(1)

# Have to "hide" args from snakemake modules
sys.argv = sys.argv[:1]

snake = SnakemakeShellCmds(snakefile, force=args.force, create_dirs=not args.no_dirs)
atoms_dir = snake.get("ATOMS_DIR")
lastz_cmd = snake.get("LASTZ_CMD")
lastz_str = snake.get("LASTZ_PARSTR")
lastz_dir = "lastz_%s" % lastz_str
atomizer_cmd = snake.get("ATOMIZER_CMD")
atomizer_str = snake.get("ATOMIZER_PARSTR")

targets = ""
if args.run_lastz:
    targets = [ atoms_dir + "/" + lastz_dir + "/" + f + ".psl" for f in snake.manager.pwAlignmentFiles() ]
elif args.atomizer:
    targets = [ '%s/lastz_%s_%s.atoms' %( atoms_dir, lastz_str, atomizer_str) ]

cmds = snake.shellcmds(targets)

if args.atomizer and len(cmds) > 1:
    print("ERROR: snakemake generated more than one command, probably there are missing files required by atomizer rule.", file = sys.stderr)
    exit(1)

for cmd in cmds:
    print(cmd)
    if args.run_lastz and cmd.split()[0] != lastz_cmd:
        print("ERROR: snakemake generated a cmd other than lastz, probably there are missing files required by run_lastz rule.", file = sys.stderr)
        exit(1)
    elif args.atomizer and cmd.split()[0] != atomizer_cmd:
        print("ERROR: snakemake generated a cmd other than atomizer, probably there are missing files required by atomizer rule.", file = sys.stderr)
        exit(1)

exit(0)

