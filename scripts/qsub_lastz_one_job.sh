#!/bin/bash
# Script that qsub uses to run one job (one cplex run) with index
# SGE_TASK_ID. The number of .lp files under the directory in the
# first argument cannot be changed while the jobs are running.

LANG=C

# wait a random number of seconds before starting to avoid many threads writing
# to the same log file across different hosts (NFS can mess up the log)
RANDOM_WAIT=180 # in seconds


# Exit the script if any command returns error
set -e

usage() {
  echo "Runs one lastz job."
  echo "Usage:"
  echo -e "\t$(basename "$0") [cmdfile] [logfile] [workdir]"
  echo "where cmdfile is the file containing one command (that corresponds to one job) per line."
  echo "The line which will be executed by this script is given by the SGE_TASK_ID environment variable."
  echo "The command will be run from the workdir directory (usually where the snakefile is)."
}

if [ $# -ne 3 ]
then
  >&2 echo "Missing or too many parameters"
  usage
  exit 1
fi

error() {
   local sourcefile=$1
   local lineno=$2
   >&2 echo "ERROR code returned by command at line $lineno"
}
trap 'error "${BASH_SOURCE}" "${LINENO}"' ERR


cmdfile="$(realpath "$1")" # transform to full path
logfile="$(realpath -m "$2")" # transform to full path
workdir="$(realpath "$3")" # transform to full path

if [ -z "$SGE_TASK_ID" ]
then
  >&2 echo "Error: SGE_TASK_ID variable not set"
  exit 1
fi

cmd="$(sed "${SGE_TASK_ID}q;d" "$cmdfile")"

# wait a random number of seconds to avoid many threads writing to the same log file across different hosts (NFS can mess up the log)
sleep $((RANDOM % RANDOM_WAIT))

# We don't want to abort the whole script if the ilp or logging fail
set +e 

echo "[$(date +"%F %T")] Starting job for command \"$cmd\" (job $SGE_TASK_ID/$SGE_TASK_LAST)" >> "$logfile"

prevdir="$(pwd)"
cd "$workdir"

eval "$cmd"
ret=$?

cd "$prevdir"

if [ $ret -eq 0 ]
then
  echo "[$(date +"%F %T")] Ended job for command \"$cmd\" (job $SGE_TASK_ID/$SGE_TASK_LAST)" >> "$logfile"
else
  echo "[$(date +"%F %T")] ERROR code $ret in job for command \"$cmd\" (job $SGE_TASK_ID/$SGE_TASK_LAST)" >> "$logfile"
fi

exit 0
