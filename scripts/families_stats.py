#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Prints some info about families in a .cog file

 
import argparse
from collections import defaultdict, Counter
from statistics import mean

parser = argparse.ArgumentParser(description='Prints some info about families in a .cog file',
                                 formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('cog', metavar='cog_file', type=argparse.FileType('r'),
                     help='file in COG format given as input to Gecko-DCJ, contains all chromosomes data.')

args = parser.parse_args()


# read cog/markers
genomes = defaultdict(lambda:defaultdict(list)) # chromosomes content only for the species we are interested (key is <chromosome number>)
families = defaultdict(list) # for each family occurrence, we just store the species name
families_sp = defaultdict(Counter) # counts how many times each family appear in each species
species = chrname = comment = ""
line = args.cog.readline()
while line:
    line = line.strip()
    if not line: # end of chromosome
        species = chrname = comment = ""
    elif not chrname: # first line of new chromosome
        species, chrname = line.split(",")
        species = species.strip()
        chrname = chrname.strip().replace("chromosome", "").strip()
    elif not comment: # second line of new chromosome
        comment = line
    else: # gene line
        gene = line.split()
        family = int(gene[0])
        start,end = gene[4].split("..")
        gene_info = {"start":int(start), "end":int(end)-1, "family":family} # position defined in basepairs
        genomes[species][chrname].append(family)
        families[family].append(species)
        families_sp[species][family] += 1

    line = args.cog.readline()

for species, chromosomes in genomes.items():
    print(species)
    gcount = sum(len(chromosome) for chromosome in chromosomes.values())
    print("  genes: %d" % gcount)
    famsp = [ f for f,occ in families.items() if species in occ ]
    print("  families: %d" % len(famsp))
    avgocc = mean(families_sp[species].values())
    print("  avg. family occurrences: %.1f" % avgocc)
    spcount = Counter()
    for f in famsp:
        spcount[len(set(families[f]))] += 1
    print("  families in at least x genomes: %s" % ", ".join("%d: %d" % (n, count) for n,count in sorted(spcount.items())))

print()
print("Total families: %d" % len(families))
print("Avg. family size: %.1f genes" % mean(len(occs) for occs in families.values()))
print("Avg. family coverage: %.1f species" % mean([ len(set(occs)) for occs in families.values() ]))
    
exit(0)

