#!/bin/bash
# Shows some stats for all instances computed by anges.
# Assumes cars_stats.py in the same directory of this script.

basepath="$(dirname "$0")"
cars_stats="$basepath/cars_stats.py"
pqtree_score="$basepath/pq-tree_score.py"
count=0

for d in atoms/lastz*/ag*/rgrape*/*_eudicots_* genes/families*/rgrape*/*_eudicots_*
do
  [ ! -d "$d" ] && continue
  let count++

  parent="$(dirname "$d")"
  dname="$(basename "$d")"
  acs="$parent/${dname%_eudicots_*}.acs"
  clusters="$(dirname "$parent")/$(basename "$parent").clusters"
  
  echo "$d"
  echo "  clusters (by gecko): $(head -1 "$clusters" | cut -d ' ' -f 2)"
  echo "  acs (clusters after limit filter): $(wc -l "$acs" | awk '{ print $1 }')"
  for f in "$d/CARS/eudicots_PQTREE"*
  do
    method="${f##*_}"
    "$cars_stats" "$f" | sed "s/.*: /  CARs in ancestor ($method): /"
    #echo -n "  PQ-tree score: "
    echo "  PQ-tree score:"
    "$pqtree_score" "$f"
  done
  echo
done

if [ $count -eq 0 ]
then
  echo "This is very weird, I found no file to process..."
  echo "Maybe you have run this script from the wrong directory."
  echo "You must run it from the directory where the Snakefike file is"
  exit 1
fi

exit 0
