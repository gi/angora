#!/usr/bin/env python3
# Given a set of CARs files (PQ-trees), prints some information.

from statistics import mean
from sys import argv
from os.path import basename

if len(argv) == 1:
    print("At least one CARS (PQ-tree) file needed (ANGES ouput).")
    print("")
    print("Usage:")
    print("")
    print("%s cars_file [ cars_file ... ]" % basename(argv[0]))
    print()
    exit(1)


for fname in argv[1:]:
    f = open(fname, "r")
    lengths = []
    for l in f:
        if l.startswith("#") or l.startswith(">"):
            continue
        car = l.split()
        cargenes = [ v for v in car if v.isdigit() ]
        lengths.append(len(cargenes))
    print("%s: genes = %d, avg_len = %f" % (fname, sum(lengths), mean(lengths)))

exit(0)

