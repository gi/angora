#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Reads file containing only one sequence and prints an interval of that sequence.

import sys
import argparse

parser = argparse.ArgumentParser(description='Reads file containing only one sequence and prints an interval of that sequence.')

group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--origin-zero', '-0', action='store_true',
                    help='start:end coordinates are origin zero, closed at start and open at end, that is, [start..end) (as in psl files)')
group.add_argument('--origin-one', '-1', action='store_true',
                    help='start:end coordinates are origin one closed, that is, [start..end] (as in lav files)')
parser.add_argument('file', metavar='fasta_file', type=str,
                    help='fasta file containing one sequence')
parser.add_argument('start', type=int,
                    help='start of the interval')
parser.add_argument('end', type=int,
                    help='end of the interval')
args = parser.parse_args()

f = open(args.file)
start = args.start
end = args.end


if args.origin_one: # then we need to convert, because python interval is exactly as in --origin-zero [start..end)
    start -= 1

first = True

seq = ""
for l in f:
    if l.startswith(">"):
        if first:
            first = False
            continue
        else:
            break
    
    seq += l.strip() # remove \n
    
    if len(seq) > end+1: # we don't need to read the rest
        break
f.close()

print(seq[start:end])

exit(0)
