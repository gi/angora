#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Reads atoms from files passed as arguments and prints some statistics
# Line format:
#   name atom_nr class strand start end
# where the sequence range is 0-based [start,end)


import argparse
from collections import Counter
import statistics
import math
from numpy import histogram

try:
    import matplotlib.pyplot as plt
except ImportError:
    plt = None

DEFAULT_HISTOGRAM_BINS = "20"
DEFAULT_HISTOGRAM_PERC = 0.95

class AtomsStatistics:

    def __init__(self, histogram_cut, histogram_bins, plot):
        self.histogram_cut = histogram_cut
        self.histogram_bins = histogram_bins
        self.plot = plot and plt
        self.figures = 0

    # Statistics for some data (prints and generate figures)
    def statistics(self, name, data):
        data.sort()

        print("Statistics for %s:" % name)
        self._pprint("Number", len(data))
        self._pprint("Min", min(data))
        self._pprint("Max", max(data))
        self._pprint("Mean", statistics.mean(data))
        for p in [0.5, 0.75, 0.9, 0.99]:
            self._pprint("Percentile %d%%" % int(p*100), int(self._percentile(data, p)))
        hdata = data[:self._percentile_pos_low(data, self.histogram_cut)+1]

        hist, bin_edges = histogram(hdata, bins=self.histogram_bins)
        self._pprint("Histogram in percentile %d%%" % int(self.histogram_cut*100))
        for i in range(len(hist)):
            self._pprint("[%s,%s%s" % (self._float2str(bin_edges[i]), self._float2str(bin_edges[i+1]),
                                 "]" if i == len(hist)-1 else ")"),
                   "%d (%s%%)" % (hist[i], self._float2str(100*hist[i]/len(hdata))),
                   indentation=2)
        if plot:
            self.figures += 1
            plt.figure(self.figures)
            plt.hist(hdata, bins=self.histogram_bins)
            plt.title("Histogram for %s in percentile %d%%" % (name, int(self.histogram_cut*100)))
        print()

    # Show figures generated so far (blocking)
    def show_figures(self):
        if plot:
            plt.show()
    
    # Pretty print
    def _pprint(self, name, value = None, indentation = 1):
        if isinstance(value, float):
            value_str = "%.1f" % value
        elif isinstance(value, int):
            value_str = "%d" % value
        elif value is None:
            value_str = ""
        else:
            value_str = str(value)

        indentstr = "    "
        print("%s%-15s %s" % (indentstr*indentation, name+":", value_str))

    # Returns a formated string without trailing zeros
    def _float2str(self, value):
        return ("%.1f" % value).rstrip('0').rstrip('.')

    # Returns the position of percentile
    def _percentile_pos_low(self, data, percent):
        return math.floor((len(data)-1) * percent)

    # Calculates percentile (based on http://code.activestate.com/recipes/511478/)
    # Data must be sorted
    def _percentile(self, data, percent): 
        if not data:
            return None
        k = (len(data)-1) * percent
        f = math.floor(k)
        c = math.ceil(k)
        if f == c:
            return data[int(k)]
        d0 = data[int(f)] * (c-k)
        d1 = data[int(c)] * (k-f)
        return d0+d1


EPILOG="""
Note 1: Percentile 𝑝% shows a value 𝑣 for which 𝑝% of data is ≤ 𝑣,
e.g. Percentile 75%: 21 means 75% of data is ≤ 21.

Note 2: Histogram in percentile 𝑝% shows, for the 𝑝% smaller values in
the data, equal-width ranges and the numbers of elements in the data
that fit that ranges.

Note 3: The parameter hbins can be a number or a method among auto,
fd, doane, scott, stone, rice, sturges or sqrt. See
<https://docs.scipy.org/doc/numpy/reference/generated/
 numpy.histogram_bin_edges.html#numpy.histogram_bin_edges>
for details on methods.
"""

parser = argparse.ArgumentParser(description='Reads atoms from files passed as arguments (atomizer output format)\nand prints some statistics.',
                                 epilog=EPILOG, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('files', metavar='file', type=str, nargs='+',
                    help='contains atoms given by atomizer tool.')
parser.add_argument('--hcut', metavar='float', type=float, default=DEFAULT_HISTOGRAM_PERC,
                    help='compute histogram for percentile (default: %g)' % DEFAULT_HISTOGRAM_PERC)
parser.add_argument('--hbins', metavar='value', type=str, default=DEFAULT_HISTOGRAM_BINS,
                    help='number of histogram bins/ranges or method (default: %s)' % DEFAULT_HISTOGRAM_BINS)
parser.add_argument('--plot', '-p', action='store_true',
                    help='show histogram plot (if matplotlib.pyplot is available)')
args = parser.parse_args()

plot = args.plot
alengths = []
families = Counter()
hcut = args.hcut

if args.hbins.isdigit():
    hbins = int(args.hbins)
else:
    try: # try to check if its a list...
        hbins = eval(args.hbins)
    except:
        hbins = args.hbins

for fname in args.files:
    with open(fname) as f:
        for line in f:
            if line.startswith("#"):
                continue
            cname, gene_number, family, strand, start, end = line.split()
            alengths.append(int(end) - int(start))
            families[family] += 1

stat = AtomsStatistics(hcut, hbins, plot)
stat.statistics("atom sizes", alengths)
stat.statistics("families sizes", list(families.values()))
stat.statistics("families sizes (discarding singletons)", [ v for v in families.values() if v > 1])
stat.show_figures()

exit(0)
