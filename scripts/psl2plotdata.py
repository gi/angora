#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Reads psl from stdin and writes the plotdata to stdout

import sys
import argparse


parser = argparse.ArgumentParser(description='Reads psl data from stdin and writes plotdata to stdout.',
                                 epilog='Works for 2 sequences only. Input or pipe the output to rdotplot.R later.')
args = parser.parse_args()


first = True

for line in sys.stdin:
    if line.startswith("#"):
        continue

    fields = line.split()
    
    if first: # print reader
        first = False
        print("%s\t%s" % (fields[13], fields[9]))

    #import pdb
    #sys.stdin = open('/dev/tty')
    #pdb.set_trace()
        
    # converting to origin-one, closed
    fields[11] = str(int(fields[11]) + 1)
    fields[15] = str(int(fields[15]) + 1)

    if fields[8] == "-":
        print("%s\t%s\n%s\t%s\nNA\tNA" % (fields[15], fields[12], fields[16], fields[11]))
    else:
        print("%s\t%s\n%s\t%s\nNA\tNA" % (fields[15], fields[11], fields[16], fields[12]))

if first: # empty input
    print("no_alignment\tno_alignment\n0\t0\n0\t0\nNA\tNA")

exit(0)
