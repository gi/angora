#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Takes gene families given by blastp computation (and other auxiliary
# files) and writes files to be used with CloseUp and ANGES.

# Just a sidenote:
#
# blastp default table (format 6 column headers):
#
# qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore
#
# 1. 	 qseqid 	 query (e.g., gene) sequence id
# 2. 	 sseqid 	 subject (e.g., reference genome) sequence id
# 3. 	 pident 	 percentage of identical matches
# 4. 	 length 	 alignment length
# 5. 	 mismatch 	 number of mismatches
# 6. 	 gapopen 	 number of gap openings
# 7. 	 qstart 	 start of alignment in query
# 8. 	 qend 	 end of alignment in query
# 9. 	 sstart 	 start of alignment in subject
# 10. 	 send 	 end of alignment in subject
# 11. 	 evalue 	 expect value
# 12. 	 bitscore 	 bit score
 
import sys
import argparse
import re
from os.path import basename
from sys import stderr
from Bio import SeqIO


def extractGenesFromRecord(genes, species, chrnum, record):
    chrname = record.name
    for f in record.features:
        # XXX maybe check untranslated proteins for stop codons?
        if f.type != 'CDS':
            continue
       
        # sort out protein identifier
        protId = None
        gi = None
        geneID = None
        locus = 'locus_tag' in f.qualifiers and f.qualifiers['locus_tag'][0] or '-unknown locus -' # 1st try
        if 'locus_tag' not in f.qualifiers:
            print('Unknown locus tag for feature %s' % f, file=sys.stderr)
            if 'gene' in f.qualifiers:
                locus = f.qualifiers['gene'][0] # 2nd try
            else:
                locus = f.qualifiers['name'][0] # 3rd try
        locus = locus.replace(' ', '')
        if 'protein_id' in f.qualifiers:
            protId = f.qualifiers['protein_id'][0]
        if 'db_xref' in f.qualifiers:
            gis = [x[3:] for x in f.qualifiers['db_xref'] if x.startswith('GI')]
            ged = [x[7:] for x in f.qualifiers['db_xref'] if
                    x.startswith('GeneID')]
            gi = gis and gis[0]
            geneID = ged and ged[0]

        start = f.location.start.position
        end = f.location.end.position
        strand = '+' if f.strand == 1 else '-'
        header = '|'.join(filter(None, (gi and 'gi|%s' %gi or 'gi|-unknown-',
            geneID and ('ge|%s' %geneID) or 'ge|-unknown-', protId and 'gb|%s' %protId, locus, 
            'strand|%s' %strand, record.name and 'chromosome|%s' %record.name)))

        gene = dict()
        gene["sp"] = species
        gene["chrname"] = chrname
        gene["chrnum"] = chrnum
        gene["start"] = start
        gene["end"] = end
        gene["strand"] = strand
        
        genes[header] = gene
        
    return chrname
            

EPILOG="""\
Note that this script does not generate all data needed by ANGES, it
also needs some data given by CloseUp (post-processed).

The species names for chromosome map and markers file are taken from
gbk filenames removing the ".gbk" suffix.
 
 
##Marker map (input for CloseUp)##

Each line is in the format:

marker (pos chr, pos chr, pos chr)

where chr must be an unique number (we convert names to unique numbers).

 
##Chromosome map (post-processing of CloseUp output to generate ANGES input)##

Each line in this file is in the following format:

chromosome_number species_name chromosome_name

where chromosome_number is the chromosome number for CloseUp, and
species_name and chromosome_name are the original names. The values
cannot contain spaces.

 
##Markers file (input for ANGES)##

The format is:

>marker_name
species.chromosome:start-end [+/-]
species.chromosome:start-end [+/-]

>marker_name...

where marker_name must be and integer. Notice the empty line after marker data.
 
"""

parser = argparse.ArgumentParser(description='Takes gene families given by blastp computation (and other auxiliary files)\n' +
                                             'and writes files to be used with CloseUp and ANGES.',
                                 epilog=EPILOG,
                                 formatter_class=argparse.RawDescriptionHelpFormatter)

ingroup = parser.add_argument_group(title="Input files", description="Files containing data we need to compute outputs.")

ingroup.add_argument('--families', metavar='file', type=str, required=True,
                     help='contains families given by processing of blastp output (see blast2families.py)')

ingroup.add_argument('gbk', type=str, nargs='+',
                     help='gbk file(s) containing all genes (needed to fetch their start-end in chromosomes)')

outcloseup = parser.add_argument_group(title="Output files for CloseUp", description="Files that will be generated for CloseUp (or post-processing of its output).")

outcloseup.add_argument('--markermap', metavar='file', type=str, required=True,
                        help='describes all markers/families and their occurrences')

outcloseup.add_argument('--chrmap', metavar='file', type=str, required=True,
                     help='chromosome map file, which describes which chromosome number in ' +
                          'CloseUp output corresponds to each original chromosome (because ' +
                          'CloseUp uses unique integers as chromosome names), used to post-' +
                          'process CloseUp output')

outanges = parser.add_argument_group(title="Output files for ANGES", description="Files that will be generated for ANGES.")

outanges.add_argument('--markers', metavar='file', type=str, required=True,
                     help='describes all marker/families and their occurrences')

args = parser.parse_args()

genes = dict()
chrnum = 1
chrmap = dict()
for gbk in args.gbk:
    species = re.sub('\.gbk$', '', basename(gbk))
    seqRecords = SeqIO.parse(gbk, 'genbank')
    for record in seqRecords:
        chrname = extractGenesFromRecord(genes, species, chrnum, record)
        chrmap[chrnum] = (species, chrname)
        chrnum += 1

with open(args.chrmap, 'w') as chrmap_f:
    for i, values in chrmap.items():
        chrmap_f.write("%d %s %s\n" % (i, values[0], values[1]))

family = 1
with open(args.families, 'r') as families_f, \
     open(args.markermap, 'w') as markermap_f, \
     open(args.markers, 'w') as markers_f:
    for line in families_f:
        headers = line.strip().split("\t")
        
        # filter different proteins for the same locus
        #locus_set = set()
        familygenes = []
        for header in headers:
            gene = genes[header]
            s = "%d %d" % (gene["chrnum"], gene["start"])
            #if s not in locus_set:
            #    locus_set.add(s)
            familygenes.append(gene)
                
        #out format: marker (pos chr, pos chr, pos chr)
        markermap_f.write("%d (%s)\n" %
                          (family,
                           ", ".join(["%d %d" % (gene["start"], gene["chrnum"]) for gene in familygenes])))
        
        #out format: >marker\nspecies.chromosome:start-end [+/-]\n...
        markers_f.write(">%d\n" % family)
        for gene in familygenes:
            markers_f.write("%s.%s:%d-%d %s\n" %
                            (gene["sp"],
                             gene["chrname"],
                             gene["start"],
                             gene["end"],
                             gene["strand"]))
        markers_f.write("\n")
        family += 1
    
exit(0)
