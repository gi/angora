#!/usr/bin/env python
# -*- coding: utf-8 -*-

from textwrap import wrap

from Bio import SeqIO
from Bio import GenBank

gbk_filename = "lettuce.gbk"
faa_filename = "lettuce-new.faa"

input_handle  = open(gbk_filename, "r")
output_handle = open(faa_filename, "w")

for seq_record in SeqIO.parse(input_handle, "genbank") :
    print "Dealing with GenBank record %s" % seq_record.id
    for seq_feature in seq_record.features :
        if seq_feature.type=="CDS" :
            assert len(seq_feature.qualifiers['translation'])==1
            output_handle.write(">%s from %s\n%s\n" % (
                   seq_feature.qualifiers['locus_tag'][0],
                   seq_record.name,
                   "\n".join(wrap(seq_feature.qualifiers['translation'][0],60))))

output_handle.close()
input_handle.close()
print "Done"
