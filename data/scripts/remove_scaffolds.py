#!/usr/bin/env python
# -*- coding: utf-8 -*-

# removes unplaced/unlocalized scaffolds and chloroplast/mitochondrion chromosomes
# by NCBI standard, chromosomes names start with NC and unplaced scaffolds with NW

from sys import argv

from Bio import SeqIO

# remove chromosomes with these strings in description
REMOVE_STR = ("mitochondrion", "chloroplast")

if len(argv) != 3:
    print("This script removes unplaced/unlocalized scaffolds and chloroplast/mitochondrion chromosomes.")
    print("Usage: remove_scaffolds.py <input.gbk> <output.gbk>")
    exit(1)

infile = open(argv[1], "r")

records = []
for record in SeqIO.parse(infile, "genbank"):
    if not record.id.startswith("NC_"):
        print("Removing scaffold %s (%s)" % (record.id, record.description))
        continue
    
    if any( [s in record.description for s in REMOVE_STR] ):
        print("Removing chloroplast/mitochondrion chromosome %s (%s)" % (record.id, record.description))
        continue
        
    print("Keeping chromosome %s (%s)" % (record.id, record.description))
    records.append(record)

outfile = open(argv[2], "w")
count = SeqIO.write(records, outfile, "genbank")

infile.close()
outfile.close()

exit(0)