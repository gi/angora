#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sys import argv
from collections import OrderedDict

from Bio import SeqIO


def add_locus_tag(feature):
    newdict = OrderedDict() # we try to keep the same insertion order, but with /locus_tag right after /gene
    for k,v in feature.qualifiers.items():
        newdict[k] = v 
        if k == "gene":
            newdict["locus_tag"] = v 
    feature.qualifiers = newdict
        

if len(argv) != 3:
    print("This script adds a /locus_tag with same identifier as the /gene entry for all CDS features.")
    print("Usage: add_locus_tag.py <input.gbk> <output.gbk>")
    exit(1)

infile = open(argv[1], "r")
records = [ record for record in SeqIO.parse(infile, "genbank") ]

for record in records:
    for feature in record.features:
        if feature.type != "CDS":
            continue
        add_locus_tag(feature)

outfile = open(argv[2], "w")
count = SeqIO.write(records, outfile, "genbank")

infile.close()
outfile.close()

exit(0)