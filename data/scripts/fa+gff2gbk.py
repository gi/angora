#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sys import argv
from collections import OrderedDict
from re import compile

from Bio import SeqIO
from Bio.Alphabet import IUPAC
from Bio.Seq import Seq, UnknownSeq
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature
from Bio.SeqFeature import FeatureLocation, CompoundLocation
from Bio.Data.IUPACData import ambiguous_dna_letters

# .faa .fa .gff .gff3 and the last one is the output
FA_EXT = ( ".fa", ".faa", ".fna", ".fasta" )
GFF_EXT = ( ".gff", ".gff3" ) # we expect files in gff3 format
GBK_EXT = ( ".gbk", ".gbff" )

GFF_IGNORE = ( "#", )

CHR_IGNORE = ( "chr0", )

LETTUCE_CHR_PREFIX = "Dovetail_09Sept_Map_inspected_12-07-2015_1_v8_"
LETTUCE_LOCUS_SUFFIX = ".mRNA1"

#DNA_ALPHABET = r'ACGTNXacgtnx'
DNA_ALPHABET = r'' + ambiguous_dna_letters + ambiguous_dna_letters.lower()

def check_empty_file_list(name, files):
    if len(files) == 0:
        print ("Error: no " + name + " files provided!")
        exit(1)

def is_DNA_alphabet(string):
    charRe = compile(r'[^' + DNA_ALPHABET + r']')
    string = charRe.search(string.rstrip())
    return not bool(string)

def guess_alphabet(handle):
    alph = IUPAC.ambiguous_dna #Alphabet.DNAAlphabet()#Alphabet.NucleotideAlphabet()
    
    for i in range(10000): # heuristic: read 10000 first lines to try to guess if protein or DNA alphabet
        l = handle.readline()
        if len(l) == 0 or l.startswith(">"):
            continue
        if not is_DNA_alphabet(l):
            alph = IUPAC.extended_protein
            break
    handle.seek(0)
    return alph

def parse_GFF_line(l):
    entries = l.split("\t")
    attrs = OrderedDict()
    for a in entries[8].rstrip().split(";"):
        a = a.strip()
        fields = a.split("=") # try to split by = first
        if len(fields) == 1:
            fields = a.split(" ") # if not possible, try to split by space
        key, value = fields
        attrs[key] = value.strip('"')
    entries[8] = attrs
    entries[3] = int(entries[3])
    entries[4] = int(entries[4])
    return entries # seqname, source, feature, start, end, score, strand, frame, attribute

def strand_type(strand):
    if strand == "+":
        return 1
    elif strand == "-":
        return -1
    elif strand == "?":
        return 0
    else:
        return None
    
def add_feat_chr(features_by_chr, chr_name, feat):
    feat_list = features_by_chr.setdefault(chr_name, [])
    feat_list.append(feat)
    
def append_children(chromosome, parent_id, parent_name, subfeatures):
    features = chromosome.features
    subfeat_parent = subfeatures.get(parent_id)
    
    if subfeat_parent is None:
        return
    
    for ftype, lst in subfeat_parent.items():
        if len(lst) == 1:
            loc = lst[0][0]
            attrs = lst[0][1]
            fid = lst[0][2]
        else: # compound location
            locs = []
            attrs = dict()
            for sub in lst:
                locs.append(sub[0])
                attrs.update(sub[1])
            locs.sort(key=lambda x: x.start)
            loc = CompoundLocation(locs)
            fid = attrs.get("ID", "<unknown id>")

        if ftype == "CDS": # we try to translate
            translate_loc = loc
            if len(lst) > 1:
                translate_loc = CompoundLocation(locs[::-1]) if loc.strand == -1 else loc
            locus_tag = attrs.get("locus_tag") or parent_name
            if locus_tag.endswith(LETTUCE_LOCUS_SUFFIX):
                locus_tag = locus_tag[:-len(LETTUCE_LOCUS_SUFFIX)]
            attrs["locus_tag"] = locus_tag
            attrs["translation"] = translate_loc.extract(chromosome).translate().seq

        feature = SeqFeature(location=loc, type=ftype, strand=loc.strand, id=fid, qualifiers=attrs)
        features.append(feature)
    
        # we may have children of subfeats (solve recursively)
        for sub in lst:
            fid = sub[2]
            attrs = sub[1]
            name = attrs.get("Name") or attrs.get("locus_tag")
            if fid != "<unknown id>" and subfeatures.get(fid) is not None:
                append_children(chromosome, fid, name, subfeatures)
                
def fasta_identifier(record):
    # Here we try to guess almost blindly
    descr = record.description
    parts = descr.split("||")
    if len(parts) == 1: # probably coffe format: >Cc01_g00020 Putative Elongation factor P
        return descr.split()[0]
    else: # probably lettuce format: >Lactuca sativa (lettuce)||Dovetail_09Sept_Map_inspected_12-07-2015_1_v8_lg_1||79663||92776||Lsat_1_v5_gn_1_1441.1||1||CDS||774166801||31470||frame0
        if parts[6] == "chromosome":
            cid = parts[1]
            return cid[len(LETTUCE_CHR_PREFIX):] if cid.startswith(LETTUCE_CHR_PREFIX) else cid
        else:
            return parts[4][:-2] # removes ".1" from end of identifier


for f in argv[1:]:
    if not f.endswith(FA_EXT + GFF_EXT + GBK_EXT):
        print ('WARNING: Unknown extension for "%s", ignoring.' % (f))


fa_files = [ f for f in argv[1:-1] if f.endswith(FA_EXT) ]
check_empty_file_list("FASTA", fa_files)
seqs = dict()
for fname in fa_files:
    handle = open(fname, "r")
    alph = guess_alphabet(handle)
    seqs.update(SeqIO.to_dict(SeqIO.parse(handle, "fasta", alphabet=alph), key_function=fasta_identifier))
    handle.close()
for ignore in CHR_IGNORE:
    if ignore in seqs:
        del seqs[ignore]

gff_files = [ f for f in argv[1:-1] if f.endswith(GFF_EXT) ]
check_empty_file_list("GFF", gff_files)

chromosomes = OrderedDict()
features = OrderedDict() # main features, not subfeatures
features_by_chr = OrderedDict() # list of main features by chromosome
subfeatures =  OrderedDict() # subfeatures, to be resolved later, we first have to store all of them to make parent references later
for fname in gff_files:
    handle = open(fname, "r")

    for l in handle:
        if l.startswith(GFF_IGNORE): # comment
            continue
        
        seqname, source, ftype, start, end, score, strand, frame, attrs = parse_GFF_line(l)
        
        if seqname in CHR_IGNORE:
            continue
        
        name = attrs.get("Name") or attrs.get("name")
        parent = attrs.get("Parent") or attrs.get("parent")
        fid = attrs.get("ID") or attrs.get("Id") or attrs.get("id", "<unknown id>")
        stype = strand_type(strand)
        loc = FeatureLocation(start-1, end, strand=stype)
                    
        if seqname.startswith(LETTUCE_CHR_PREFIX): # too long, raises exception when writing genbank
            seqname = seqname[len(LETTUCE_CHR_PREFIX):]
            if fid.startswith("chromosome "): # need to transform this to a good ID, not the original ridiculous ID
                fid = fid[len("chromosome " + LETTUCE_CHR_PREFIX):]
        
        seq = seqs.get(seqname) if ftype == "chromosome" else seqs.get(name)
        seq_descr = seq.description if seq is not None else None
        seq = UnknownSeq(end-start+1) if seq is None else seq.seq
        
        if ftype == "chromosome":
            if isinstance(seq, UnknownSeq): # in this case, we can't allow a unknown sequence type
                seq = Seq("N" * len(seq), IUPAC.ambiguous_dna)
            source = SeqFeature(location=loc, type="source", qualifiers={ k:v for k,v in attrs.items() if k not in ("ID", "Name")})
            descr = "Chromosome " + seqname
            if seq_descr is not None and seq_descr != '<unknown description>':
                descr += " (" + seq_descr.replace("||", " || ") + ")"
            chromosomes[seqname] = SeqRecord(seq, id=fid, name=seqname, description=descr, features=[source])
            continue
        
        if not parent: # should have and ID
            if not isinstance(seq, UnknownSeq):
                if seq_descr is not None and seq_descr != '<unknown description>':
                    attrs["note"] = seq_descr
                attrs["translation"] = seq
            feature = SeqFeature(location=loc, type=ftype, strand=stype, id=fid, qualifiers=attrs)
            features[fid] = feature
            add_feat_chr(features_by_chr, seqname, feature) 
        else: # subfeature
            #if fid != "<unknown id>":
            #    features[fid] = feature
            subfeat_parent = subfeatures.setdefault(parent, dict())
            subfeat_list = subfeat_parent.setdefault(ftype, list())
            subfeat_list.append( (loc, attrs, fid) )
    handle.close()

#import pdb
#pdb.set_trace()

for chr_name, feat_list in features_by_chr.items():
#for chr_name, feat_list in [ i for i in features_by_chr.items()][:1]:
    for feature in feat_list:
    #for feature in feat_list[:1000]:
        chromosomes[chr_name].features.append(feature)
        name = feature.qualifiers.get("Name") or feature.qualifiers.get("locus_tag")
        append_children(chromosomes[chr_name], feature.id, name, subfeatures) 


gbk_fname = argv[-1]
if not gbk_fname.endswith(GBK_EXT):
    print("Error: no GenBank output file provided! (must be the last argument)")
handle = open(gbk_fname, "w")

count = SeqIO.write(chromosomes.values(), handle, "genbank")
handle.close()
