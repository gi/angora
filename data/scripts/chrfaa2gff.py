#!/usr/bin/env python
# -*- coding: utf-8 -*-

# given (1) a species name and (2) a fasta file with chromosomes
# content, prints a gff table

from sys import argv
from Bio import SeqIO

if len(argv) != 3:
    print("This script prints a gff table given (1) a species name,")
    print("and (2) a fasta file with chromosomes content.")
    print()
    print("Usage: chrfaa2gff.py <species name> <input.fasta>")
    exit(1)

species = argv[1]
infile = open(argv[2], "r")
records = []

for rec in SeqIO.parse(infile, "fasta"):
    print("%s\t%s\tchromosome\t1\t%d\t.\t.\t.\tID=%s;Name=%s" %
          (rec.id, species, len(rec), rec.id, rec.name,))

infile.close()

exit(0)
