#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sys import argv
from collections import OrderedDict

from Bio import SeqIO

THRESHOLD=5

        

if len(argv) != 3:
    print("This script removes ambiguous (>" + str(THRESHOLD) + "%% X amino acid codes) genes and related features.")
    print("Usage: remove_ambiguous_genes.py <input.gbk> <output.gbk>")
    exit(1)

print("#######################")
print("# NOT YET IMPLEMENTED #")
print("#######################")

infile = open(argv[1], "r")
records = [ record for record in SeqIO.parse(infile, "genbank") ]

for record in records:
    for feature in record.features:
        import pdb
        pdb.set_trace()
        
        #######################
        # NOT YET IMPLEMENTED #
        #######################
        # print removed (locus_tag or gene) and protein_id for CDSs

outfile = open(argv[2], "w")
count = SeqIO.write(records, outfile, "genbank")

infile.close()
outfile.close()

exit(0)
