Simulated .fna files obtained by ALF - A Simulation Framework for Genome Evolution
http://alfsim.org

.gff files obtained by running for each species (SE001, SE002 and SE003):
  data/scripts/chrfaa2gff.py species_name species_name.fna

.gbk files obtained by running for each species:
  data/scripts/fa+gff2gbk.py species_name.fna species_name.gff species_name.gbk
  

######################################################################
# !!!!!IMPORTANT!!!!!                                                #
# COPY OR LINK THE config.yaml TO THE ROOT FOLDER OF ANGORA.         #
######################################################################

