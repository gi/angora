configfile: 'config.yaml'

from glob import glob
from importlib import util as imputil
from os.path import basename, join, abspath, dirname
from os import getcwd
import re

# where is the software directory?
SCRIPT_DIR      = config.get('script_dir', 'scripts')
# load experiment manager
spec = imputil.spec_from_file_location('experiment', '%s/experiment.py' % \
        SCRIPT_DIR)
exp = imputil.module_from_spec(spec)
spec.loader.exec_module(exp)

DATASET         = config.get('dataset', 'dataset.cfg')
MANAGER         = exp.Manager(DATASET)

ANCESTOR        = config.get('ancestor', '')
EVOL_TREE       = config.get('evol_tree', '')
REFERENCE       = config.get('reference', '')

MARKER_MINLEN   = config.get('marker_min_length', 100)
ALN_IDENT       = config.get('sgmtn_alignment_ident', 70)
ALN_MAXGAP      = config.get('sgmtn_alignment_maxgap', 20)
ALN_MINLEN      = config.get('sgmtn_alignment_minlen', 13)
ATOMIZER_CMD    = config.get('segmentation_cmd', 'atomizer')
ATOMS_DIR       = config.get('atoms_out', 'atoms')
ATOMIZER_PARSTR = 'ag%s_ai%s_al%s_m%s' %(ALN_MAXGAP, ALN_IDENT, ALN_MINLEN,
        MARKER_MINLEN)
ATOMIZER_CPUS   = config.get('sgmtn_cpus', 1)

BLAST_DIR       = config.get('blast_dir', '')
BLAST_PARAMS    = config.get('blast_params', '')
BLAST_PARSTR    = BLAST_PARAMS.replace('-', '').replace('=', '').replace(' ',
        '_')
BLAST_THREADS   = config.get('sgmtn_cpus', 1)
CIRCOS_CMD      = config.get('circos_cmd', 'circos')
CORES           = snakemake.get_argument_parser().parse_args().cores or 1
GENES_DIR       = config.get('genes_out', 'genes')


GENOMES_DIR     = config.get('genome_data_dir', 'genomes')
LASTZ_CMD       = config.get('lastz_cmd', 'lastz')
LASTZ_PARAMS    = config.get('lastz_params', '')
LASTZ_PARSTR    = LASTZ_PARAMS.replace('-', '').replace('=', '').replace(' ',
        '_')
LASTZ_DIR = '%s/lastz_%s' % (ATOMS_DIR, LASTZ_PARSTR)

LAV2PSL_CMD     = config.get('lavToPsl_cmd', 'lavToPsl') # deprecated, not used anymore
PLOT_DIR        = config.get('plot_dir', 'plots')
COG_PARAMS      = config.get('cog_params', '')

GECKO_CMD       = config.get('gecko_cmd', '')
GECKO_PARAMS    = "-r " + REFERENCE + " " + config.get('gecko_params','')
GECKO_PARSTR    = ("r" + REFERENCE + " " + config.get('gecko_params','')).replace(
        '"',"").replace(" --","\n").replace(" -","\n").replace(" ","").replace(
        "\n","_").replace("],[","__").replace("[","").replace("]","").replace(
        ",","_")
GECKO_DIR       = '%s/%s' % (LASTZ_DIR, ATOMIZER_PARSTR)

G2A_PARAMS      = config.get('gecko2anges_params', '--noref') or '--noref'
G2A_PARSTR      = G2A_PARAMS.replace('--', '_').replace(
        ' ', '').replace('-','').strip(" _")
G2A_DIR = '%s/%s' % (GECKO_DIR, GECKO_PARSTR)
REFINE          = "--ref-overlap" in G2A_PARAMS

ANGES_CMD       = config.get('anges_cmd', '')
ANGES_USE_SIM_WEIGHT = config.get('anges_use_sim_weight', True)
ANGES_RUN_BAB   = config.get('anges_run_bab', False)
ANGES_PARSTR    = "%s_%s_%s" % (ANCESTOR, "sim" if ANGES_USE_SIM_WEIGHT else "nosim",
        "bab" if ANGES_RUN_BAB else "heur")
ANGES_DIR = '%s/%s_%s' % (G2A_DIR, G2A_PARSTR, ANGES_PARSTR)


# Some separate variable for the "alternative" pipeline using gene families (not in "all" rule)
CIP_THRESHOLD   = config.get('cip_threshold', 0.6)
CALP_THRESHOLD  = config.get('calp_threshold', 0.7)
GENEFAMILIES_DIR = '%s/families_cip%s_calp%s' %(GENES_DIR, CIP_THRESHOLD, CALP_THRESHOLD)
GENEFAMILIES_PARSTR = 'cip%s_calp%s' %(CIP_THRESHOLD, CALP_THRESHOLD)

GF_GECKO_DIR = GENEFAMILIES_DIR
GF_G2A_DIR = '%s/%s' % (GF_GECKO_DIR, GECKO_PARSTR)
GF_ANGES_DIR = '%s/%s_%s' % (GF_G2A_DIR, G2A_PARSTR, ANGES_PARSTR)

# Variables for Salse's pipeline
CLOSEUP_CMD     = config.get('closeup_cmd', '')
CLOSEUP_PARAMS  = config.get('closeup_params', '')
CLOSEUP_PARSTR  = CLOSEUP_PARAMS.replace('\\n','_')

C2A_DIR = '%s/%s' % (GENEFAMILIES_DIR, CLOSEUP_PARSTR)
CL_ANGES_DIR = '%s/%s' % (C2A_DIR, ANGES_PARSTR)

#
# MAIN RULE
#

rule all:
    input:
        pqtree = [ '%s/CARS/%s_PQTREE_HEUR' %(ANGES_DIR, ANCESTOR) ] +
        ( ['%s_refined/CARS/%s_PQTREE_HEUR' %(ANGES_DIR, ANCESTOR) ]
                if REFINE else [] ),

        # for annotated genes pipeline:
        #pqtreeG = [ '%s_refined/CARS/%s_PQTREE_HEUR' %(GF_ANGES_DIR, ANCESTOR) ] +
        #( [ '%s_refined/CARS/%s_PQTREE_BAB' %(GF_ANGES_DIR, ANCESTOR) ]
        #        if ANGES_RUN_BAB else [] ),


#rule link_gbks:
#    input:
#        list(MANAGER.get(genome, 'gbk') for genome in MANAGER.genomes)
#    output:
#        expand('%s/{genome}.gbk' %GENOMES_DIR, genome=MANAGER.genomes)
#    run:
#        from os import symlink, path
#        for g in MANAGER.genomes:
#            src = MANAGER.get(g, 'gbk')
#            dest = '%s/%s.gbk' %(GENOMES_DIR, g)
#            symlink(path.relpath(src, GENOMES_DIR), dest)

rule extract_fna:
    input:
        lambda wildcards: MANAGER.get(wildcards.genome, 'gbk')
    output:
        '%s/{genome,[^/]+}.fna' %GENOMES_DIR
    shell:
        '%s/gbk2fasta.py "{input}" > "{output}"' %SCRIPT_DIR


rule extract_to_chr_fasta:
    input:
        list(map(lambda x: MANAGER.get(x, 'gbk'), MANAGER.genomes))
    output:
        list(map(lambda x: join(GENOMES_DIR, '%s.fna' %x),
                MANAGER.chromosomeFiles()))
    run:
        from Bio import SeqIO
        for f in input:
            out_dir = join(GENOMES_DIR, basename(f)[:-4])
            for record in SeqIO.parse(open(f), 'gb'):
                out = open(join(out_dir, '%s.fna' %record.name), 'w')
                SeqIO.write(record, out, 'fasta')
                out.close()


rule run_lastz:
    input:
        f1 = '%s/{genome1}/{chrom_file1}.fna' %GENOMES_DIR,
        f2 = '%s/{genome2}/{chrom_file2}.fna' %GENOMES_DIR
    params:
        lastz = LASTZ_PARAMS,
        coord1 = lambda wildcards: '..'.join((wildcards.coord1_start,
                wildcards.coord1_end)),
        coord2 = lambda wildcards: '..'.join((wildcards.coord2_start,
                wildcards.coord2_end)),
        chrlen1 = lambda wildcards: MANAGER.getChromosomeLength(
                wildcards.chrom_file1),
        chrlen2 = lambda wildcards: MANAGER.getChromosomeLength(
                wildcards.chrom_file2),
    output:
        '%s/lastz_%s/{genome1,[^/]+}:{genome2,[^/]+}/' %(ATOMS_DIR,LASTZ_PARSTR) +
        '{chrom_file1,[^/]+}:{chrom_file2,[^/]+}/'
        '{coord1_start,[0-9]+}-{coord1_end,[0-9]+}:'
        '{coord2_start,[0-9]+}-{coord2_end,[0-9]+}.psl'
    shell:
        '%s {params.lastz} "{input.f1}[{params.coord1}]" ' % LASTZ_CMD +
        '"{input.f2}[{params.coord2}]" | %s/lav2psl.py ' % SCRIPT_DIR +
        '--fix-psl-coordinates {params.chrlen1} {params.chrlen2} > "{output}"'
        ## old way using UCSC Genome Browser's lavToPsl
        #'%s {params.lastz} {input.f1}[{params.coord1}] ' %LASTZ_CMD +
        #'{input.f2}[{params.coord2}] | %s /dev/stdin /dev/stdout' %LAV2PSL_CMD +
        #' | %s/fix_psl.py {wildcards.coord1_start} ' % SCRIPT_DIR +
        #'{params.chrlen1} {wildcards.coord2_start} {params.chrlen2} > {output}' 


rule combine_all_chr_pairs:
    input:
        lambda wildcards: list(map(lambda x: join(ATOMS_DIR, 'lastz_%s' %
            LASTZ_PARSTR, '%s.psl' %x),
                MANAGER.pwAlignmentFiles(genome_pair = (wildcards.g1,
                wildcards.g2), chr_pair = (wildcards.chr1, wildcards.chr2))))
    output:
        '%s/lastz_%s/{g1,[^:]+}:{g2,[^:]+}/{chr1,[^:]+}:{chr2,[^:]+}.psl' %(
                ATOMS_DIR, LASTZ_PARSTR)
    shell:
        'cat {input} > "{output}"'


rule psl_to_list:
    input:
        expand('%s/lastz_%s/{genome_chrom_pair}.psl' %(ATOMS_DIR,
            LASTZ_PARSTR), genome_chrom_pair=MANAGER.genomeChromsomePairs())
    output:
        '%s/lastz_%s.list' %(ATOMS_DIR, LASTZ_PARSTR)
    run:
        out = open(output[0], 'w')
        for f in input:
            out.write('%s\n' %f)
        out.close()


rule atomizer:
    input:
        '%s/lastz_%s.list' %(ATOMS_DIR, LASTZ_PARSTR)
    params:
        al_ident = ALN_IDENT,
        al_max_gap = ALN_MAXGAP,
        al_min_len = ALN_MINLEN,
        min_len = MARKER_MINLEN,
    threads:
        ATOMIZER_CPUS
    output:
        '%s/lastz_%s/%s.atoms' %(ATOMS_DIR, LASTZ_PARSTR, ATOMIZER_PARSTR)
    log:
        'logs/lastz_%s_%s.log' %(LASTZ_PARSTR, ATOMIZER_PARSTR)
    shell:
        '%s "{input}" --inputNotPsl --minLength {params.min_len} ' %ATOMIZER_CMD +
        '--minAlnLength {params.al_min_len} --minIdent {params.al_ident} --maxGap '
        '{params.al_max_gap} --numThreads {threads} > "{output}" 2> "{log}"'


rule dotplot:
    input:
        '%s/{genome_chrom_pair}.psl' % LASTZ_DIR
    params:
        #title = lambda wildcards: wildcards.genome_chrom_pair.replace(':',
        #        ' vs ')
        title = lambda wildcards: \
                MANAGER.getChromosomePairPlotTitle(wildcards.genome_chrom_pair),
        # do not use quoted in shell
        chrlen = lambda wildcards: \
                MANAGER.getChromosomePairLengths(wildcards.genome_chrom_pair)
    output:
        '%s/{genome_chrom_pair}.pdf' % LASTZ_DIR
    log:
        'logs/dotplot_{genome_chrom_pair}_%s.log' %LASTZ_PARSTR
    shell:
        '%s/psl2plotdata.py < "{input}" | ' % SCRIPT_DIR +
        '%s/rdotplot.R "{params.title}" maxxy {params.chrlen} ' % SCRIPT_DIR +
        '> "{output}"'


rule alldotplots:
    input:
        dotplots = expand('%s/lastz_%s/{genome_chrom_pair}.pdf' %(ATOMS_DIR,
            LASTZ_PARSTR), genome_chrom_pair=MANAGER.genomeChromsomePairs()),


rule cog:
    input:
        '%s/%s.atoms' %(LASTZ_DIR, ATOMIZER_PARSTR)
    params:
        lastz = LASTZ_PARAMS,
        cog = COG_PARAMS,
        genomes_chr = MANAGER.getGenomesChromosomesList(),
    output:
        '%s/%s.cog' %(LASTZ_DIR, ATOMIZER_PARSTR),
    log:
        'logs/%s/cog_%s.log' %(LASTZ_PARSTR, ATOMIZER_PARSTR)
    shell:
        '%s/atoms2cog.py {params.cog} ' % SCRIPT_DIR +
        '--note "lastz parameters: {params.lastz}" {params.genomes_chr} ' +
        '< "{input}" > "{output}"' 
        

rule gecko:
    input:
        '%s/%s.cog' %(LASTZ_DIR, ATOMIZER_PARSTR),
    params:
        GECKO_PARAMS
    output:
        gckz = '%s/%s.gckz' %(GECKO_DIR, GECKO_PARSTR),
        clusters = '%s/%s.clusters' %(GECKO_DIR, GECKO_PARSTR),
    log:
        'logs/%s/%s/gecko_%s.log' %(LASTZ_PARSTR, ATOMIZER_PARSTR,
                GECKO_PARSTR)
    shell:
        '%s -c --Infile "{input}" --Outfile "{output.gckz}" ' %GECKO_CMD +
        '--resultOutput clusterData showAll "{output.clusters}" {params}'


rule gecko_to_anges:
    input:
        cog = '%s/%s.cog' %(LASTZ_DIR, ATOMIZER_PARSTR),
        clusters = '%s/%s.clusters' %(GECKO_DIR, GECKO_PARSTR),
    params:
        G2A_PARAMS
    output:
        ['%s/%s_refined.cog' %(G2A_DIR, G2A_PARSTR)] if REFINE else [],
        markers = '%s/%s.markers' %(G2A_DIR, G2A_PARSTR),
        acs = '%s/%s.acs' %(G2A_DIR, G2A_PARSTR),
        acsdoubled = '%s/%s_doubled.acs' %(G2A_DIR, G2A_PARSTR),
        chrmap = '%s/%s.chrmap' %(G2A_DIR, G2A_PARSTR),
        familiesmap = '%s/%s.familiesmap' %(G2A_DIR, G2A_PARSTR),
        clustersmap = '%s/%s.clustersmap' %(G2A_DIR, G2A_PARSTR),
    log:
        'logs/%s/%s/gecko2anges_%s_%s.log' %(LASTZ_PARSTR, ATOMIZER_PARSTR,
                GECKO_PARSTR, G2A_PARSTR)
    shell:
        '%s/gecko2anges.py --cog "{input.cog}" --clusters ' % SCRIPT_DIR +
        '"{input.clusters}" --markers "{output.markers}" --acs ' +
        '"{output.acs}" --acsdoubled "{output.acsdoubled}" --chrmap ' +
        '"{output.chrmap}" --familiesmap "{output.familiesmap}" ' +
        '--clustersmap "{output.clustersmap}" {params} ' +
        ('--ref-cog {output[0]}' if REFINE else '')


rule create_anges_conf:
    input:
        markers = '%s/%s.markers' %(G2A_DIR, G2A_PARSTR),
        acs = '%s/%s.acs' %(G2A_DIR, G2A_PARSTR),
        tree = EVOL_TREE
    params:
        markers = '%s.markers' % G2A_PARSTR,
        acs = '%s.acs' % G2A_PARSTR,
        tree = basename(EVOL_TREE) 
    output:
        '%s/anges_%s_%s.params' %(G2A_DIR, G2A_PARSTR, ANGES_PARSTR)
    log:
        'logs/%s/%s/%s_%s/angesconf_%s.log' %(LASTZ_PARSTR, ATOMIZER_PARSTR,
                GECKO_PARSTR, G2A_PARSTR, ANGES_PARSTR)
    shell:
        '%s/create_anges_conf.sh "%s" ' % (SCRIPT_DIR, ANCESTOR) +
        '"{params.markers}" "{params.tree}" "{params.acs}" ' +
        '"%s_%s" ' %(G2A_PARSTR, ANGES_PARSTR) +
        '%d %d > "{output}" ' %(int(ANGES_USE_SIM_WEIGHT), int(ANGES_RUN_BAB)) +
        ' && cp "{input.tree}" "%s/"' % G2A_DIR


rule anges:
    input:
        params = '%s/anges_%s_%s.params' %(G2A_DIR, G2A_PARSTR, ANGES_PARSTR),
        markers = '%s/%s.markers' %(G2A_DIR, G2A_PARSTR),
        acs = '%s/%s.acs' %(G2A_DIR, G2A_PARSTR),
        tree = EVOL_TREE
    output:
        [ '%s/CARS/%s_PQTREE_HEUR' %(ANGES_DIR, ANCESTOR) ] +
        ( [ '%s/CARS/%s_PQTREE_BAB' %(ANGES_DIR, ANCESTOR) ] if ANGES_RUN_BAB
                else [] )
    log:
        'logs/%s/%s/%s_%s/anges_%s.log' %(LASTZ_PARSTR, ATOMIZER_PARSTR,
                GECKO_PARSTR, G2A_PARSTR, ANGES_PARSTR)
    shell:
        'cd "%s" && "%s" "anges_%s_%s.params" && cd -' % (G2A_DIR,
                abspath(ANGES_CMD), G2A_PARSTR, ANGES_PARSTR)


if REFINE:

    rule geckoREF:
        input:
            '%s/%s_refined.cog' %(G2A_DIR, G2A_PARSTR)
        params:
            GECKO_PARAMS
        output:
            gckz = '%s/%s_refined.gckz' %(G2A_DIR, G2A_PARSTR),
            clusters = '%s/%s_refined.clusters' %(G2A_DIR, G2A_PARSTR)
        log:
            'logs/%s/%s/gecko_%s_%s.log' %(LASTZ_PARSTR, ATOMIZER_PARSTR,
                    GECKO_PARSTR, G2A_PARSTR)
        shell:
            '%s -c --Infile "{input}" --Outfile "{output.gckz}" ' %GECKO_CMD +
            '--resultOutput clusterData showAll "{output.clusters}" {params}'


    rule gecko_to_angesREF:
        input:
            cog = '%s/%s_refined.cog' %(G2A_DIR, G2A_PARSTR),
            clusters = '%s/%s_refined.clusters' %(G2A_DIR, G2A_PARSTR)
        params:
            re.sub(" ?--ref-[a-z]* ?"," ", G2A_PARAMS)
        output:
            markers = '%s/%s_refined.markers' %(G2A_DIR, G2A_PARSTR),
            acs = '%s/%s_refined.acs' %(G2A_DIR, G2A_PARSTR),
            acsdoubled = '%s/%s_doubled_refined.acs' %(G2A_DIR, G2A_PARSTR),
            chrmap = '%s/%s_refined.chrmap' %(G2A_DIR, G2A_PARSTR),
            clustersmap = '%s/%s_refined.clustersmap' %(G2A_DIR, G2A_PARSTR),
        log:
            'logs/%s/%s/gecko2anges_%s_%s_refined.log' %(LASTZ_PARSTR, ATOMIZER_PARSTR,
                    GECKO_PARSTR, G2A_PARSTR)
        shell:
            '%s/gecko2anges.py --cog "{input.cog}" --clusters ' % SCRIPT_DIR +
            '"{input.clusters}" --markers "{output.markers}" --acs ' +
            '"{output.acs}" --acsdoubled "{output.acsdoubled}" --chrmap ' +
            '"{output.chrmap}" --clustersmap "{output.clustersmap}" {params}'


    rule create_anges_confREF:
        input:
            markers = '%s/%s_refined.markers' %(G2A_DIR, G2A_PARSTR),
            acs = '%s/%s_refined.acs' %(G2A_DIR, G2A_PARSTR),
            tree = EVOL_TREE
        params:
            markers = '%s_refined.markers' % G2A_PARSTR,
            acs = '%s_refined.acs' % G2A_PARSTR,
            tree = basename(EVOL_TREE) 
        output:
            '%s/anges_%s_%s_refined.params' %(G2A_DIR, G2A_PARSTR, ANGES_PARSTR)
        log:
            'logs/%s/%s/%s_%s/angesconf_%s_refined.log' %(LASTZ_PARSTR,
                    ATOMIZER_PARSTR, GECKO_PARSTR, G2A_PARSTR, ANGES_PARSTR)
        shell:
            '%s/create_anges_conf.sh "%s" ' % (SCRIPT_DIR, ANCESTOR) +
            '"{params.markers}" "{params.tree}" "{params.acs}" ' +
            '"%s_%s_refined" ' %(G2A_PARSTR, ANGES_PARSTR) +
            '%d %d > "{output}" ' %(int(ANGES_USE_SIM_WEIGHT), int(ANGES_RUN_BAB)) +
            ' && cp "{input.tree}" "%s/"' % G2A_DIR


    rule angesREF:
        input:
            params = '%s/anges_%s_%s_refined.params' %(G2A_DIR, G2A_PARSTR, ANGES_PARSTR),
            markers = '%s/%s_refined.markers' %(G2A_DIR, G2A_PARSTR),
            acs = '%s/%s_refined.acs' %(G2A_DIR, G2A_PARSTR),
            tree = EVOL_TREE
        output:
            [ '%s_refined/CARS/%s_PQTREE_HEUR' %(ANGES_DIR, ANCESTOR) ] +
            ( [ '%s_refined/CARS/%s_PQTREE_BAB' %(ANGES_DIR, ANCESTOR) ]
                    if ANGES_RUN_BAB else [] )
        log:
            'logs/%s/%s/%s_%s/anges_%s_refined.log' %(LASTZ_PARSTR,
                    ATOMIZER_PARSTR, GECKO_PARSTR, G2A_PARSTR, ANGES_PARSTR)
        shell:
            'cd "%s" && "%s" "anges_%s_%s_refined.params" && cd -' % (G2A_DIR,
                    abspath(ANGES_CMD), G2A_PARSTR, ANGES_PARSTR)



rule do_circos_conf:
    input:
        '%s/{atom_out}.atoms' %ATOMS_DIR
    params:
        outdir = '%s/{atom_out}' %PLOT_DIR
    output:
        circos_conf = '%s/{atom_out}/circos.conf' %PLOT_DIR,
        links = '%s/{atom_out}/links.txt' %PLOT_DIR,
        karyotype  = '%s/{atom_out}/karyotype.txt' %PLOT_DIR,
        chromosomes = '%s/{atom_out}/chromosomes.txt' %PLOT_DIR,
    shell:
        '%s/atoms2circos.py -o {params.outdir} "{input}"' %SCRIPT_DIR


rule run_circos:
    input:
        conf = '%s/{plot_dir}/circos.conf' %PLOT_DIR,
        links= '%s/{plot_dir}/links.txt' %PLOT_DIR,
        karyo= '%s/{plot_dir}/karyotype.txt' %PLOT_DIR,
        chrom= '%s/{plot_dir}/chromosomes.txt' %PLOT_DIR,
    output:
        '%s/{plot_dir}/circos.png' %PLOT_DIR,
    shell:
        '%s -conf "{input.conf}"' %CIRCOS_CMD




## RULES FOR ALTERNATIVE PIPELINE (ANNOTATED GENES) ##

rule extract_all_proteins:
    input:
        lambda wildcards: MANAGER.get(wildcards.genome, 'gbk')
    output:
        '%s/{genome,[^/]+}.faa' %GENOMES_DIR
    shell:
        '%s/extract_all_proteins.py "{input}" > "{output}"' %SCRIPT_DIR


rule create_blast_db:
    input:
        expand('%s/{genome}.faa' %GENOMES_DIR, genome =
                MANAGER.genomes)
    params:
        title = 'BLAST database of protein sequences ' + ' '.join(
                MANAGER.genomes),
        dbname = '%s/%s' %(config['genes_out'], config['blast_db_name']),
        dbtype = config['blast_db_type'],
        dbversion = config['blast_db_version']
    output:
        expand('%s/%s.{dbfile}' %(config['genes_out'],
            config['blast_db_name']), dbfile=config['blast_db_endings'])
    log:
        'logs/mkblastdb_%s_%s.log' %(config['blast_db_type'],
                config['blast_db_name'])
    shell:
        '%s -in "{input}" ' %join(BLAST_DIR, config['mkblastdb_cmd']) +
        '-hash_index -out {params.dbname} -dbtype {params.dbtype} -title '
        '"{params.title}" -logfile {log} -blastdb_version {params.dbversion}'


rule run_blast:
    input:
        faa = '%s/{genome}.faa' %GENOMES_DIR,
        blast_db = expand(join(config['genes_out'], '%s.{dbfile}' %(
            config['blast_db_name'])), dbfile=config['blast_db_endings'])
    params:
        dbname = join(config['genes_out'], config['blast_db_name']),
        blast_params = BLAST_PARAMS
    output:
        '%s/%s_%s/{genome,[^/]+}.%s' %(config['genes_out'], config['blast_cmd'],
                BLAST_PARSTR, config['blast_cmd'])
    log:
        'logs/%s_%s_{genome}.log' %(config['blast_cmd'], BLAST_PARSTR)
    threads:
        BLAST_THREADS
    shell:
        '%s -db {params.dbname} ' %join(BLAST_DIR, config['blast_cmd']) +
        '-num_threads {threads} {params.blast_params} -outfmt 6 '
        '< "{input.faa}" > "{output}" 2> "{log}"'


rule blast_to_families:
    input:
        faa = expand('%s/{genome}.faa' %GENOMES_DIR, genome=MANAGER.genomes),
        blastp = expand('%s/%s_%s/{genome}.%s' %(config['genes_out'],
                config['blast_cmd'], BLAST_PARSTR, config['blast_cmd']),
                genome=MANAGER.genomes)
    params:
        cip = '{cip}',
        calp = '{calp}' 
    output:
        '%s/families_cip{cip}_calp{calp}/families' %GENES_DIR
    log:
        'logs/blast2families_cip{cip}_calp{calp}.log'
    shell:
        '%s/blast2families.py -i {params.cip} -a {params.calp} ' %SCRIPT_DIR +
        '{input.faa} -- {input.blastp} > "{output}"'


rule postprocess_gene_families:
    input:
        families = '%s/families' %GENEFAMILIES_DIR,
        gbk = list(map(lambda x: MANAGER.get(x, 'gbk'), MANAGER.genomes))
    output:
        markermap = '%s/markermap' %GENEFAMILIES_DIR,
        chrmap = '%s/chrmap' %GENEFAMILIES_DIR,
        markers = '%s/markers' %GENEFAMILIES_DIR
    log:
        'logs/postprocess_families_%s.log' %(GENEFAMILIES_PARSTR)
    shell:
        '%s/postprocess_blastpfamilies.py ' %SCRIPT_DIR +
        '--families "{input.families}" --markermap "{output.markermap}" ' +
        '--chrmap "{output.chrmap}" --markers "{output.markers}" {input.gbk}'


rule cogG:
    input:
        '%s/markers' %GENEFAMILIES_DIR
    params:
        cip = CIP_THRESHOLD,
        calp = CALP_THRESHOLD,
        cog = COG_PARAMS,
    output:
        cog = '%s/genes.cog' %GENEFAMILIES_DIR,
    log:
        'logs/%s/cog_%s.log' %(GENEFAMILIES_DIR, GENEFAMILIES_PARSTR)
    shell:
        '%s/gfamilies2cog.py {params.cog} ' % SCRIPT_DIR +
        '--note "cip: {params.cip}, calp: {params.calp}" ' +
        '< "{input}" > "{output.cog}"' 
        

rule geckoG:
    input:
        cog = '%s/genes.cog' %GENEFAMILIES_DIR,
    params:
        GECKO_PARAMS
    output:
        gckz = '%s/%s.gckz' %(GF_GECKO_DIR, GECKO_PARSTR),
        clusters = '%s/%s.clusters' %(GF_GECKO_DIR, GECKO_PARSTR),
    log:
        'logs/%s/%s/gecko_%s.log' %(GENEFAMILIES_DIR, GENEFAMILIES_PARSTR,
                GECKO_PARSTR)
    shell:
        '%s -c --Infile "{input}" --Outfile "{output.gckz}" ' %GECKO_CMD +
        '--resultOutput clusterData showAll "{output.clusters}" {params}'


rule gecko_to_angesG:
    input:
        cog = '%s/genes.cog' %GENEFAMILIES_DIR,
        clusters = '%s/%s.clusters' %(GF_GECKO_DIR, GECKO_PARSTR)
    params:
        G2A_PARAMS
    output:
        ['%s/genes_refined.cog' %GENEFAMILIES_DIR] if REFINE else [],
        markers = '%s/%s.markers' %(GF_G2A_DIR, G2A_PARSTR),
        acs = '%s/%s.acs' %(GF_G2A_DIR, G2A_PARSTR),
        acsdoubled = '%s/%s_doubled.acs' %(GF_G2A_DIR, G2A_PARSTR),
        chrmap = '%s/%s.chrmap' %(GF_G2A_DIR, G2A_PARSTR),
        familiesmap = '%s/%s.familiesmap' %(GF_G2A_DIR, G2A_PARSTR),
        clustersmap = '%s/%s.clustersmap' %(GF_G2A_DIR, G2A_PARSTR),
    log:
        'logs/%s/%s/gecko2anges_%s_%s.log' %(GENEFAMILIES_DIR,
                GENEFAMILIES_PARSTR, GECKO_PARSTR, G2A_PARSTR)
    shell:
        '%s/gecko2anges.py --cog "{input.cog}" --clusters ' % SCRIPT_DIR +
        '"{input.clusters}" --markers "{output.markers}" --acs ' +
        '"{output.acs}" --acsdoubled "{output.acsdoubled}" --chrmap ' +
        '"{output.chrmap}" --familiesmap "{output.familiesmap}" ' +
        '--clustersmap "{output.clustersmap}" {params} ' +
        ('--ref-cog {output[0]}' if REFINE else '')


rule create_anges_confG:
    input:
        markers = '%s/%s.markers' %(GF_G2A_DIR, G2A_PARSTR),
        acs = '%s/%s.acs' %(GF_G2A_DIR, G2A_PARSTR),
        tree = EVOL_TREE
    params:
        markers = '%s.markers' % G2A_PARSTR,
        acs = '%s.acs' % G2A_PARSTR,
        tree = basename(EVOL_TREE) 
    output:
        '%s/anges_%s_%s.params' %(GF_G2A_DIR, G2A_PARSTR, ANGES_PARSTR),
    log:
        'logs/%s/%s/%s_%s/angesconf_%s.log' %(GENEFAMILIES_DIR,
                GENEFAMILIES_PARSTR, GECKO_PARSTR, G2A_PARSTR,
                ANGES_PARSTR)
    shell:
        '%s/create_anges_conf.sh "%s" ' % (SCRIPT_DIR, ANCESTOR) +
        '"{params.markers}" "{params.tree}" "{params.acs}" ' +
        '"%s_%s" ' %(G2A_PARSTR, ANGES_PARSTR) +
        '%d %d > "{output}" ' %(int(ANGES_USE_SIM_WEIGHT), int(ANGES_RUN_BAB)) +
        ' && cp "{input.tree}" "%s/"' % GF_G2A_DIR


rule angesG:
    input:
        params = '%s/anges_%s_%s.params' %(GF_G2A_DIR, G2A_PARSTR, ANGES_PARSTR),
        markers = '%s/%s.markers' %(GF_G2A_DIR, G2A_PARSTR),
        acs = '%s/%s.acs' %(GF_G2A_DIR, G2A_PARSTR),
        tree = EVOL_TREE
    output:
        [ '%s/CARS/%s_PQTREE_HEUR' %(GF_ANGES_DIR, ANCESTOR) ] +
        ( [ '%s/CARS/%s_PQTREE_BAB' %(GF_ANGES_DIR, ANCESTOR) ] if ANGES_RUN_BAB
                else [] )
    log:
        'logs/%s/%s/%s_%s/anges_%s.log' %(GENEFAMILIES_DIR,
                GENEFAMILIES_PARSTR, GECKO_PARSTR, G2A_PARSTR,
                ANGES_PARSTR)
    shell:
        'cd "%s" && "%s" "anges_%s_%s.params" && cd -' % (GF_G2A_DIR,
                abspath(ANGES_CMD), G2A_PARSTR, ANGES_PARSTR)

if REFINE:

    rule geckoGREF:
        input:
            cog = '%s/genes_refined.cog' %GENEFAMILIES_DIR,
        params:
            GECKO_PARAMS
        output:
            gckz = '%s/%s_refined.gckz' %(GF_GECKO_DIR, GECKO_PARSTR),
            clusters = '%s/%s_refined.clusters' %(GF_GECKO_DIR, GECKO_PARSTR),
        log:
            'logs/%s/%s/gecko_%s_refined.log' %(GENEFAMILIES_DIR,
                    GENEFAMILIES_PARSTR, GECKO_PARSTR)
        shell:
            '%s -c --Infile "{input}" --Outfile "{output.gckz}" ' %GECKO_CMD +
            '--resultOutput clusterData showAll "{output.clusters}" {params}'


    rule gecko_to_angesGREF:
        input:
            cog = '%s/genes_refined.cog' %GENEFAMILIES_DIR,
            clusters = '%s/%s_refined.clusters' %(GF_GECKO_DIR, GECKO_PARSTR)
        params:
            re.sub(" ?--ref-[a-z]* ?"," ", G2A_PARAMS)
        output:
            markers = '%s/%s_refined.markers' %(GF_G2A_DIR, G2A_PARSTR),
            acs = '%s/%s_refined.acs' %(GF_G2A_DIR, G2A_PARSTR),
            acsdoubled = '%s/%s_doubled_refined.acs' %(GF_G2A_DIR, G2A_PARSTR),
            chrmap = '%s/%s_refined.chrmap' %(GF_G2A_DIR, G2A_PARSTR),
            clustersmap = '%s/%s_refined.clustersmap' %(GF_G2A_DIR, G2A_PARSTR),
        log:
            'logs/%s/%s/gecko2anges_%s_%s_refined.log' %(GENEFAMILIES_DIR,
                    GENEFAMILIES_PARSTR, GECKO_PARSTR, G2A_PARSTR)
        shell:
            '%s/gecko2anges.py --cog "{input.cog}" --clusters ' % SCRIPT_DIR +
            '"{input.clusters}" --markers "{output.markers}" --acs ' +
            '"{output.acs}" --acsdoubled "{output.acsdoubled}" --chrmap ' +
            '"{output.chrmap}" --clustersmap "{output.clustersmap}" {params}'


    rule create_anges_confGREF:
        input:
            markers = '%s/%s_refined.markers' %(GF_G2A_DIR, G2A_PARSTR),
            acs = '%s/%s_refined.acs' %(GF_G2A_DIR, G2A_PARSTR),
            tree = EVOL_TREE
        params:
            markers = '%s_refined.markers' % G2A_PARSTR,
            acs = '%s_refined.acs' % G2A_PARSTR,
            tree = basename(EVOL_TREE) 
        output:
            '%s/anges_%s_%s_refined.params' %(GF_G2A_DIR, G2A_PARSTR, ANGES_PARSTR),
        log:
            'logs/%s/%s/%s_%s/angesconf_%s_refined.log' %(GENEFAMILIES_DIR,
                    GENEFAMILIES_PARSTR, GECKO_PARSTR, G2A_PARSTR,
                    ANGES_PARSTR)
        shell:
            '%s/create_anges_conf.sh "%s" ' % (SCRIPT_DIR, ANCESTOR) +
            '"{params.markers}" "{params.tree}" "{params.acs}" ' +
            '"%s_%s_refined" ' %(G2A_PARSTR, ANGES_PARSTR) +
            '%d %d > "{output}" ' %(int(ANGES_USE_SIM_WEIGHT), int(ANGES_RUN_BAB)) +
            ' && cp "{input.tree}" "%s/"' % GF_G2A_DIR


    rule angesGREF:
        input:
            params = '%s/anges_%s_%s_refined.params' %(GF_G2A_DIR, G2A_PARSTR, ANGES_PARSTR),
            markers = '%s/%s_refined.markers' %(GF_G2A_DIR, G2A_PARSTR),
            acs = '%s/%s_refined.acs' %(GF_G2A_DIR, G2A_PARSTR),
            tree = EVOL_TREE
        output:
            [ '%s_refined/CARS/%s_PQTREE_HEUR' %(GF_ANGES_DIR, ANCESTOR) ] +
            ( [ '%s_refined/CARS/%s_PQTREE_BAB' %(GF_ANGES_DIR, ANCESTOR) ]
                    if ANGES_RUN_BAB else [] )
        log:
            'logs/%s/%s/%s_%s/anges_%s_refined.log' %(GENEFAMILIES_DIR,
                    GENEFAMILIES_PARSTR, GECKO_PARSTR, G2A_PARSTR,
                    ANGES_PARSTR)
        shell:
            'cd "%s" && "%s" "anges_%s_%s_refined.params" && cd -' % (GF_G2A_DIR,
                    abspath(ANGES_CMD), G2A_PARSTR, ANGES_PARSTR)




## RULES FOR SALSE'S (2016) PIPELINE ##

rule closeup: # must run in its directory, that's why we change the workdir.
              # params in absolute path, in and out needs relative path
    input:
        '%s/markermap' %GENEFAMILIES_DIR
    params:
        inf = abspath('%s/markermap' %GENEFAMILIES_DIR),
        outf = abspath('%s/clusters_%s.closeup' %(GENEFAMILIES_DIR,CLOSEUP_PARSTR)),
    output:
        out = '%s/clusters_%s.closeup' %(GENEFAMILIES_DIR,CLOSEUP_PARSTR),
        overview = '%s/clusters_%s.closeup_overview.txt' %
                (GENEFAMILIES_DIR, CLOSEUP_PARSTR),
        detail = '%s/clusters_%s.closeup_detail.txt' %(GENEFAMILIES_DIR,
            CLOSEUP_PARSTR)
    log:
        'logs/%s/closeup_%s.log' %(GENEFAMILIES_PARSTR,CLOSEUP_PARSTR)
    shell:
        'cd "%s"; ' %dirname(abspath(CLOSEUP_CMD)) + # change workdir
        '%s <<< "$(echo -e "intra\\n{params.inf}\\n' %abspath(CLOSEUP_CMD) +
        '%s\\n{params.outf}\\n{params.outf}")"; ' %CLOSEUP_PARAMS +
        'rm -f delete.me pierre_ps; ' +
        'cd "%s"' %getcwd() # back to original dir, just in case   


rule closeup_to_anges:
    input:
        overview = '%s/clusters_%s.closeup_overview.txt' % (GENEFAMILIES_DIR,
                CLOSEUP_PARSTR),
        detail = '%s/clusters_%s.closeup_detail.txt' %(GENEFAMILIES_DIR,
            CLOSEUP_PARSTR),
        chrmap = '%s/chrmap' %GENEFAMILIES_DIR,
        markers = '%s/markers' %GENEFAMILIES_DIR,
    output:
        acs = '%s/acs' %C2A_DIR,
        chrmap = '%s/chrmap' %C2A_DIR,
        markers = '%s/markers' %C2A_DIR

    log:
        'logs/%s/closeup2anges_%s.log' %(GENEFAMILIES_PARSTR, CLOSEUP_PARSTR)
    shell:
        '%s/closeup2anges.py --overview "{input.overview}" ' %SCRIPT_DIR +
        '--detail "{input.detail}" --closeup-chrmap "{input.chrmap}" ' +
        '--markers-in "{input.markers}" --acs "{output.acs}" ' +
        '--chrmap "{output.chrmap}" --markers "{output.markers}"'


rule create_anges_confC:
    input:
        markers = '%s/markers' %C2A_DIR,
        acs = '%s/acs' %C2A_DIR,
        tree = EVOL_TREE
    params:
        markers = 'markers',
        acs = 'acs',
        tree = basename(EVOL_TREE)
    output:
        '%s/anges_%s.params' %(C2A_DIR, ANGES_PARSTR)
    log:
        'logs/genes_closeup/%s/%s/angesconf_%s.log' %(GENEFAMILIES_PARSTR,
                CLOSEUP_PARSTR, ANGES_PARSTR)
    shell:
        '%s/create_anges_conf.sh "%s" ' % (SCRIPT_DIR, ANCESTOR) +
        '"{params.markers}" "{params.tree}" "{params.acs}" ' +
        '"%s" ' %ANGES_PARSTR +
        '%d %d > "{output}" ' %(int(ANGES_USE_SIM_WEIGHT), int(ANGES_RUN_BAB)) +
        ' && cp "{input.tree}" "%s/"' % C2A_DIR


rule angesC:
    input:
        params = '%s/anges_%s.params' %(C2A_DIR, ANGES_PARSTR),
        markers = '%s/markers' %C2A_DIR,
        acs = '%s/acs' %C2A_DIR,
        tree = EVOL_TREE
    output:
        [ '%s/CARS/%s_PQTREE_HEUR' %(CL_ANGES_DIR, ANCESTOR) ] +
        ( [ '%s/CARS/%s_PQTREE_BAB' %(CL_ANGES_DIR, ANCESTOR) ] if ANGES_RUN_BAB
                else [] )
    log:
        'logs/genes_closeup/%s/%s/anges_%s.log' %(GENEFAMILIES_PARSTR,
                CLOSEUP_PARSTR, ANGES_PARSTR)
    shell:
        'cd "%s" && "%s" "anges_%s.params" && cd -' % (C2A_DIR,
                abspath(ANGES_CMD), ANGES_PARSTR)



